var gulp = require('gulp');
var sass = require('gulp-sass');
module.exports = function() {
    return gulp.src('./frontend/css/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./frontend/css'));
}
