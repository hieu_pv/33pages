var gulp = require('gulp');
var ngHtml2Js = require("gulp-ng-html2js");
var concat = require("gulp-concat");
module.exports = function() {
    return gulp.src("frontend/js/**/*.tpl.html")
        .pipe(ngHtml2Js({
            moduleName: "app.template",
            prefix: "/frontend/js/"
        }))
        .pipe(concat("templates.js"))
        .pipe(gulp.dest("./frontend/js"));
}
