(function(app) {

    app.controller('RouteCardCtrl', RouteCardCtrl);
    RouteCardCtrl.$inject = ['$rootScope', '$scope', '$localStorage'];

    function RouteCardCtrl($rootScope, $scope, $localStorage) {
        if (_.isNil($localStorage.routeReload)) {
            $localStorage.routeReload = 1;
            location.reload();
        }
    	$rootScope.isBirtPage = true;
        $scope.birtRouteCard = $localStorage.routeCardData;
    }

})(angular.module('app.components.route-card', []));
