require('./department/');
require('./login/');
require('./landing-page/');
require('./treatment-admin/');
require('./route-card/');

(function(app) {

})(angular.module('app.components', [
    'app.components.department',
    'app.components.login.index',
    'app.components.landing-page',
    'app.components.treatment-admin',
    'app.components.route-card',
]));
