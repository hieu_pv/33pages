(function(app) {

    app.controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$rootScope', '$scope', '$state', '$cookieStore', '$http', 'API', 'Notification'];

    function LoginCtrl($rootScope, $scope, $state, $cookieStore, $http, API, Notification) {
        $scope.login = login;

        function login(user) {
            API.user.login(user)
                .then(function(token) {
                    Notification.show('success', 'Login success');
                    $cookieStore.put('token', token);
                    $http.defaults.headers.common.Authorization = 'Bearer ' + token;
                    API.user.profile()
                        .then(function(result) {
                            $rootScope.user = result;
                            $state.go('select-department');
                        })
                        .catch(function(error) {
                            $state.go('login');
                        });
                })
                .catch(function(error) {
                    Notification.show('warning', 'Wrong User or Password');
                    throw (error);
                });
        }
    }

})(angular.module('app.components.login.index', []));
