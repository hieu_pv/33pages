(function(app) {
    app.controller('AdminReferenceCtrl', AdminReferenceCtrl);
    AdminReferenceCtrl.$inject = ['$rootScope', '$scope', '$state', 'Notification', 'API', '$window'];

    function AdminReferenceCtrl($rootScope, $scope, $state, Notification, API, $window) {

        /*
         * get Our Approval levels
         */
        API.procedure.get('ProcGetOurApprovalList')
            .then(function(result) {
                $scope.OurApprovalList = result;
            })
            .catch(function(error) {
                throw (error);
            });

        /*
         * add new our approval
         */
        $scope.addOurApproval = function(OurApproval) {
            if (OurApproval === undefined || OurApproval === '') {
                Notification.show('warning', 'Please enter OurApproval');
                return false;
            }

            if (OurApproval.length > 15) {
                Notification.show('warning', 'The our approval may not be greater than 15 characters.');
                return false;
            }

            API.procedure.post('ProcAddOurApproval', {
                    UserID: $rootScope.user.getId(),
                    OurApproval: OurApproval,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /*
         * delete an approval;
         */
        $scope.deleteOurApproval = function(OurApprovalList) {
            OurApprovalList.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteOurApproval', {
                            UserID: $rootScope.user.getId(),
                            OurApprovalID: value.pkApprovalID
                        })
                        .then(function(result) {
                            _.remove(OurApprovalList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /*
         * get Deviation Reasons
         */
        API.procedure.get('ProcGetDeviationList')
            .then(function(result) {
                $scope.DeviationList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /*
         * add deviation reason
         */
        $scope.addDeviation = function(Deviation) {
            if (Deviation === undefined || Deviation === '') {
                Notification.show('warning', 'Please enter Deviation Reasons');
                return false;
            }

            if (Deviation.length > 20) {
                Notification.show('warning', 'The deviation reason may not be greater than 20 characters.');
                return false;
            }

            API.procedure.post('ProcAddDeviationReason', {
                    UserID: $rootScope.user.getId(),
                    DeviationType: Deviation,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.post('ProcAddOurApproval', {
                    UserID: $rootScope.user.getId(),
                    OurApproval: OurApproval,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /*
         * delete deviation reason
         */
        $scope.deleteDeviationReasons = function(DeviationList) {
            DeviationList.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteDeviationReason', {
                            UserID: $rootScope.user.getId(),
                            DeviationID: value.DeviationID,
                        })
                        .then(function(result) {
                            _.remove(DeviationList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /*
         * get shapes list
         */
        API.procedure.get('ProcGetMaterialShapes')
            .then(function(result) {
                $scope.ShapesList = result;
            })
            .catch(function(error) {
                throw (error);
            });

        /*
         * add Material Shape
         */
        $scope.addMaterialShape = function(MaterialShape, MaterialShapeRequire) {
            if (MaterialShape === undefined || MaterialShape === '') {
                Notification.show('warning', 'Please enter Material Shape');
                return false;
            }

            if (MaterialShape.length > 15) {
                Notification.show('warning', 'The material shape may not be greater than 15 characters.');
                return false;
            }

            var ReqdDesc;
            if (MaterialShapeRequire) {
                ReqdDesc = 1;
            } else {
                ReqdDesc = null;
            }
            API.procedure.post('ProcAddMaterialShape', {
                    UserID: $rootScope.user.getId(),
                    Description: MaterialShape,
                    ReqdDesc: ReqdDesc,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /*
         * del material shape
         */
        $scope.delMaterialShape = function(ShapesList) {
            ShapesList.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteMaterialShape', {
                            UserID: $rootScope.user.getId(),
                            pkShapeID: value.pkShapeID
                        })
                        .then(function(result) {
                            _.remove(ShapesList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /**
         * get all approval
         */
        API.procedure.get('ProcGetAllApprovalList')
            .then(function(result) {
                $scope.AllApproval = result;
            })
            .catch(function(error) {
                throw error;
            });

        /*
         * add new approval level
         */
        $scope.addApprovalLevel = function(ApprovalLevel) {
            if (ApprovalLevel === undefined || ApprovalLevel === '') {
                Notification.show('warning', 'Please enter approval level');
                return false;
            }

            if (ApprovalLevel.length > 15) {
                Notification.show('warning', 'The all approval may not be greater than 15 characters.');
                return false;
            }

            API.procedure.post('ProcAddApproval', {
                    UserID: $rootScope.user.getId(),
                    Approval: ApprovalLevel
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * delete AllApproval
         */
        $scope.deleteAllApproval = function(AllApproval) {
            AllApproval.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteApproval', {
                            UserID: $rootScope.user.getId(),
                            ApprovalID: value.pkApprovalID
                        })
                        .then(function(result) {
                            _.remove(AllApproval, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /**
         * get unit of measure
         */
        API.procedure.get('ProcGetUOMList')
            .then(function(result) {
                $scope.ListUOM = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * add UOM
         */
        $scope.addUOM = function(UOM) {
            if (UOM === undefined | UOM === '') {
                Notification.show('warning', 'Please enter Units of Measure');
                return false;
            }

            if (UOM.length > 11) {
                Notification.show('warning', 'The unit of measure may not be greater than 11 characters.');
                return false;
            }

            API.procedure.post('ProcAddUOM', {
                    UserID: $rootScope.user.getId(),
                    UOM: UOM,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * delete UOM
         */
        $scope.deleteUOM = function(ListUOM) {
            ListUOM.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteUOM', {
                            UserID: $rootScope.user.getId(),
                            UOMID: value.pkUOMID,
                        })
                        .then(function(result) {
                            _.remove(ListUOM, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /**
         * get all drop document
         */
        API.procedure.get('ProcGetDocumentDropTypes')
            .then(function(result) {
                $scope.ListDropTypes = result;
            })
            .catch(function(error) {
                throw error;
            });

        /*
         * add document drop type
         */
        $scope.AddDocumentDropType = function(DocumentDropType) {
            if (DocumentDropType === undefined || DocumentDropType === '') {
                Notification.show('warning', 'Please enter document drop type');
                return false;
            }

            if (DocumentDropType.length > 17) {
                Notification.show('warning', 'The document drop types may not be greater than 17 characters.');
                return false;
            }

            API.procedure.post('ProcAddDocumentDropType', {
                    UserID: $rootScope.user.getId(),
                    DropType: DocumentDropType,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };
        /*
         * delete document drop type
         */
        $scope.DeleteDocumentDropType = function(ListDropTypes) {
            ListDropTypes.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteDocumentDropType', {
                            UserID: $rootScope.user.getId(),
                            DropTypeID: value.pkDocumentDropTypeID,
                        })
                        .then(function(result) {
                            _.remove(ListDropTypes, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /**
         * get Business Shutdown
         */
        API.procedure.get('ProcGetBusinessShutdowns')
            .then(function(result) {
                $scope.listBusiness = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * add Business Shutdown
         */
        $scope.addBusinessShutdown = function(ShutDate, ShutReason) {
            var regexShutDate = /^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])$/;
            if (ShutDate === undefined | ShutDate === '' | ShutReason === undefined | ShutReason === '') {
                Notification.show('warning', 'Please enter Business shutdowns');
                return false;
            }
            if (!regexShutDate.test(ShutDate)) {
                Notification.show('warning', 'Business Date has format DD/MM');
                return false;
            }
            ShutDate = ShutDate.split('/');
            ShutDate = ShutDate[1] + '/' + ShutDate[0];
            var year = new Date().getFullYear();
            ShutDate = year + '/' + ShutDate;

            API.procedure.post('ProcAddBusinessShutdown', {
                    UserID: $rootScope.user.getId(),
                    ShutDate: ShutDate,
                    ShutReason: ShutReason,
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * delete Business Shutdown
         */
        $scope.deleteBusinessShutdown = function(ListBS) {
            ListBS.forEach(function(value) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcDeleteBusinessShutdown', {
                            UserID: $rootScope.user.getId(),
                            BusinessShutdownID: value.ShutdownID,
                        })
                        .then(function(result) {
                            _.remove(ListBS, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        $scope.exportInvoices = function() {
            API.global.post('exportExcel', {
                    userName: $rootScope.user.getUsername(),
                    procedureName: 'ProcSageExport',
                    title: 'Export Invoices'
                })
                .then(function(result) {
                    var form = $('<form method="GET" action="/' + result + '">');
                    $('body').append(form);
                    form.submit();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }
})(angular.module('app.components.department.admin-reference', []));
