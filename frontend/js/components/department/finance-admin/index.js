var Branch = require('../../../models/Branch');
(function(app) {

    app.controller('FinanceAdminCtrl', FinanceAdminCtrl);
    FinanceAdminCtrl.$inject = ['$rootScope', '$scope', '$state', 'API', 'Notification', '$window'];

    function FinanceAdminCtrl($rootScope, $scope, $state, API, Notification, $window) {

        /**
         * get Finance Admin
         */
        API.procedure.get('ProcGetFinanceAdmin')
            .then(function(result) {
                $scope.FinanceAdmin = result[0];
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Websites
         */
        API.procedure.get('ProcGetWebsites')
            .then(function(result) {
                $scope.Websites = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddWebsite = function(website) {
            if (website === undefined) {
                Notification.show('warning', 'Please enter full information');
                return false;
            }
            if (website.Website === undefined) {
                Notification.show('warning', 'Please enter Full site address');
                return false;
            }
            var regexWebsite = /^www\.[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
            if (!regexWebsite.test(website.Website)) {
                Notification.show('warning', 'Website not invalid');
                return false;
            }
            if (website.UserName === undefined) {
                Notification.show('warning', 'Please enter Username');
                return false;
            }
            if (website.SitePassword === undefined) {
                Notification.show('warning', 'Please enter Password');
                return false;
            }

            API.procedure.post('ProcAddWebsite', {
                    UserID: $rootScope.user.getId(),
                    Website: website.Website,
                    UserName: website.UserName,
                    SitePassword: website.SitePassword
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.DeleteWebsite = function(Websites) {
            var selectedWebsite = _.filter(Websites, _.matches({ 'selected': true }));
            if (selectedWebsite.length > 0) {
                selectedWebsite.forEach(function(value) {

                    API.procedure.post('ProcDeleteWebsite', {
                            UserID: $rootScope.user.getId(),
                            WebsiteID: value.WebsiteID
                        })
                        .then(function(result) {
                            _.remove(Websites, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                });
            } else {
                Notification.show('Please select Website');
                return false;
            }
        };

        /**
         * get Branches
         */
        API.procedure.get('ProcGetBranches')
            .then(function(result) {
                $scope.Branches = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.DeleteBranch = function(Branches) {
            if (Branches.length > 0) {
                var selectedBranch = _.filter(Branches, _.matches({ 'selected': true }));
                if (selectedBranch.length > 0) {
                    selectedBranch.forEach(function(value) {
                        API.procedure.post('ProcDeleteBranch', {
                                UserID: $rootScope.user.getId(),
                                BranchID: value.BranchID
                            })
                            .then(function(result) {
                                _.remove(Branches, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                }
            }
        };

        $scope.AddAddBranch = function(addBranch) {
            var AddBranch = new Branch(addBranch);
            API.procedure.post('ProcAddBranch', {
                    UserID: $rootScope.user.getId(),
                    BranchName: AddBranch.getBranchName(),
                    Address1: AddBranch.getAddress1(),
                    Address2: AddBranch.getAddress2(),
                    Address3: AddBranch.getAddress3(),
                    Address4: AddBranch.getAddress4(),
                    Postcode: AddBranch.getPostcode(),
                    Phone: AddBranch.getPhone(),
                    Fax: AddBranch.getFax(),
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.UpdateFinanceAdmin = function(FinanceAdmin) {
            var regexNumber = /^\d+$/;
            var regexCurrency = /(^\d{1,5}$)|(^\d{1,4}\.\d{1}$)|(^\d{1,3}\.\d{1,2}$)|(^\d{1,2}\.\d{1,3}$)|(^\d{1}\.\d{1,4}$)/;
            if (FinanceAdmin.VATRate !== undefined && FinanceAdmin.VATRate !== '') {
                if (!regexNumber.test(FinanceAdmin.VATRate)) {
                    Notification.show('warning', 'VAT rate must be number');
                    return false;
                }
            } 
            if (FinanceAdmin.FAIRCharge === undefined | FinanceAdmin.FAIRCharge === '') {
                Notification.show('warning', 'Please enter FAIR charge');
                return false;
            }
            if (!regexNumber.test(FinanceAdmin.FAIRCharge)) {
                Notification.show('warning', 'FAIR charge must be number');
                return false;
            }
            if (FinanceAdmin.MaterialMarkup === undefined | FinanceAdmin.MaterialMarkup === '') {
                Notification.show('warning', 'Please enter Material markup');
                return false;
            }
            if (!regexNumber.test(FinanceAdmin.MaterialMarkup)) {
                Notification.show('warning', 'Material markup must be number');
                return false;
            }
            if (FinanceAdmin.USD === undefined | FinanceAdmin.USD === '') {
                Notification.show('warning', 'Please enter USD');
                return false;
            }
            if (!regexCurrency.test(FinanceAdmin.USD)) {
                Notification.show('warning', 'USD must be decimal(5,4)');
                return false;
            }
            if (FinanceAdmin.Euros === undefined | FinanceAdmin.Euros === '') {
                Notification.show('warning', 'Please enter Euros');
                return false;
            }
            if (!regexCurrency.test(FinanceAdmin.Euros)) {
                Notification.show('warning', 'Euros must be decimal(5,4)');
                return false;
            }
            if (FinanceAdmin.IrishPounds === undefined | FinanceAdmin.IrishPounds === '') {
                Notification.show('warning', 'Please enter Irish pounds');
                return false;
            }
            if (!regexCurrency.test(FinanceAdmin.IrishPounds)) {
                Notification.show('warning', 'Irish pounds must be decimal(5,4)');
                return false;
            }

            API.procedure.post('ProcUpdateFinanceAdmin', {
                    VATRate: FinanceAdmin.VATRate,
                    FAIRCharge: FinanceAdmin.FAIRCharge,
                    MaterialMarkup: FinanceAdmin.MaterialMarkup,
                    USD: FinanceAdmin.USD,
                    Euros: FinanceAdmin.Euros,
                    IrishPounds: FinanceAdmin.IrishPounds,
                })
                .then(function(result) {
                    Notification.show('success', 'Update success');
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.getRates = function() {
            var url = 'http://www.currenciesdirect.com/en/uk/currency-tools/todays-rate';
            window.open(url, '_blank');
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }

})(angular.module('app.components.department.finance-admin', []));
