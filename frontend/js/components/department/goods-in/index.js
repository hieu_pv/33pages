var moment = require('moment');
var GoodsIn = require('../../../models/GoodsIn');
(function(app) {

    app.controller('GoodsInCtrl', GoodsInCtrl);

    app.filter('contain', function() {
        return function(array, key, data) {
            if (!_.isUndefined(array) && !_.isUndefined(_(array).head())) {
                if (!_.has(_(array).head(), key) || data === undefined || data === '') {
                    return array;
                } else {
                    return _.filter(array, function(item) {
                        var pattern = new RegExp(data, "i");
                        return pattern.test(_.toString(item[key]));
                    });
                }
            }
        };
    });
    GoodsInCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function GoodsInCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;

        /**
         * Goods in have 2 sub screen is Receive & Material Issue
         * if WordOrderID not exists then execute Receive Screen
         * else execute Material Screen
         */

        /**
         * Sort Table
         */
        $scope.sortTable = [{
            title: 'GRB No.',
            sortBy: 'GRBNo'
        }, {
            title: 'Spec',
            sortBy: 'Spec'
        }, {
            title: 'Size',
            sortBy: 'Size'
        }, {
            title: 'units',
            sortBy: 'Units'
        }, {
            title: 'Shape',
            sortBy: 'Shape'
        }, {
            title: 'Release',
            sortBy: 'ReleaseLevel'
        }, {
            title: 'Purch. Lgth',
            sortBy: 'PurchaseLength'
        }, {
            title: 'Date in',
            sortBy: 'DateIn'
        }, {
            title: 'Ord. Qty',
            sortBy: 'OrderQty'
        }, {
            title: 'In Stk',
            sortBy: 'InStock'
        }, {
            title: 'Locn',
            sortBy: 'Location'
        }, ];

        $scope.sortBy = $scope.sortTable[0].sortBy;

        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        $scope.Units = [{
            id: 1,
            name: 'inches'
        }, {
            id: 2,
            name: 'mm'
        }];

        $scope.DocType = {};

        /**
         * get In stock table
         */
        $scope.getMaterialInStock = function() {
            API.procedure.get('ProcMaterialInStock')
                .then(function(result) {
                    if (result.length > 0) {
                        result.forEach(function(value) {
                            if (value.GRBNo !== null && value.GRBNo !== "") {
                                value.GRBNo = parseFloat(value.GRBNo);
                            }
                            if (value.OrderQty !== null && value.OrderQty !== "") {
                                value.OrderQty = parseFloat(value.OrderQty);
                            }
                            if (value.InStock !== null && value.InStock !== "") {
                                value.InStock = parseFloat(value.InStock);
                            }
                        });
                        if ($stateParams.GRBNo !== undefined) {
                            var selectMaterial = _.find(result, function(value) {
                                return parseFloat(value.GRBNo) === parseFloat($stateParams.GRBNo);
                            });
                            if (selectMaterial !== undefined) {
                                selectMaterial.selected = true;
                            }
                        }
                    }
                    $scope.MaterialInStock = result;

                    /**
                     * Params pass to WorkOrder - POs tab
                     */
                    if ($stateParams.WOGRBNo !== undefined) {
                        $scope.MaterialInStock = _.filter(result, _.matches({
                            'GRBNo': parseFloat($stateParams.WOGRBNo)
                        }));
                        $scope.MaterialInStock[0].selected = true;
                        $scope.selectedLocation();
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };
        $scope.getMaterialInStock();

        $scope.getMaterialIssues = function(GRBNo) {
            API.procedure.post('ProcGetMaterialIssues', {
                    GRBNo: GRBNo
                })
                .then(function(result) {
                    $scope.MaterialIssues = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.getSelectedStockDetail = function(GRBNo) {
            API.procedure.post('ProcShowSelectedStockDetail', {
                    GRBNo: GRBNo
                })
                .then(function(response) {
                    if (response.length) {

                        $scope.GRB = $scope.GRB.setOptions(response[0]);
                        $scope.GRB.Customer = response[0].CustomerName;
                        $scope.GRB.XLocation = response[0].YLocation;
                        $scope.GRB.YLocation = response[0].XLocation;

                        $scope.$watch('MaterialTypeList', function(MaterialTypeList) {
                            if (!_.isNil(MaterialTypeList)) {
                                $scope.GRType = _.find(MaterialTypeList, function(item) {
                                    return parseInt(item.MaterialType) === parseInt(response[0].MaterialTypeID);
                                });
                            }
                        });

                        $scope.$watch('POList', function(POList) {
                            if (!_.isNil(POList)) {
                                $scope.PONumber = _.find(POList, function(item) {
                                    return parseInt(item.POnID) === parseInt(response[0].POnID);
                                });
                                if (!_.isNil($scope.PONumber)) {
                                    $scope.getPOItemsList($scope.PONumber.POnID);
                                }
                            }
                        });

                        $scope.$watch('POItemsList', function(POItemsList) {
                            if (!_.isNil(POItemsList)) {
                                $scope.POItem = _.find(POItemsList, function(item) {
                                    return parseInt(item.ItemID) === parseInt(response[0].ItemID);
                                });
                            }
                        });

                        $scope.$watch('ApprovalList', function(ApprovalList) {
                            if (!_.isNil(ApprovalList)) {
                                $scope.Approval = _.find(ApprovalList, function(item) {
                                    return parseInt(item.pkApprovalID) === parseInt(response[0].ApprovalTypeID);
                                });
                            }
                        });

                        $scope.$watch('UOMList', function(UOMList) {
                            if (!_.isNil(UOMList)) {
                                $scope.UOM = _.find(UOMList, function(item) {
                                    return parseInt(item.pkUOMID) === parseInt(response[0].UOMID);
                                });

                                $scope.UOM2 = _.find(UOMList, function(item) {
                                    return parseInt(item.pkUOMID) === parseInt(response[0].UOMID2);
                                });
                            }
                        });

                        $scope.$watch('MaterialShapes', function(MaterialShapes) {
                            if (!_.isNil(MaterialShapes)) {
                                $scope.Shape = _.find(MaterialShapes, function(item) {
                                    return parseInt(item.pkShapeID) === parseInt(response[0].ShapeID);
                                });
                            }
                        });

                        $scope.$watch('StoreAreaList', function(StoreAreaList) {
                            if (!_.isNil(StoreAreaList)) {
                                $scope.GRB.Location = _.find(StoreAreaList, function(item) {
                                    return parseInt(item.StoreAreaID) === parseInt(response[0].StoreAreaID);
                                });

                                if ($scope.LocationInfo === undefined) {
                                    $scope.LocationInfo = {};
                                }
                                $scope.LocationInfo.Location = _.find(StoreAreaList, function(item) {
                                    return parseInt(item.StoreAreaID) === parseInt(response[0].StoreAreaID);
                                });

                                if (!_.isNil($scope.GRB) && !_.isNil($scope.GRB.Location)) {
                                    $scope.getXLocationProc($scope.GRB.Location.StoreAreaID);
                                    $scope.getYLocationProc($scope.GRB.Location.StoreAreaID);
                                }
                            }
                        });

                        $scope.$watch('XStoreLocations', function(XStoreLocations) {
                            if (!_.isNil(XStoreLocations)) {
                                if (!_.isNil($scope.GRBNo)) {
                                    $scope.XLocation = _.find(XStoreLocations, function(item) {
                                        return parseInt(item.XLocation) === parseInt(response[0].YLocation);
                                    });

                                    $scope.LocationInfo.XLocation = _.find(XStoreLocations, function(item) {
                                        return parseInt(item.XLocation) === parseInt(response[0].YLocation);
                                    });
                                }
                            }
                        });

                        $scope.$watch('YStoreLocations', function(YStoreLocations) {
                            if (!_.isNil(YStoreLocations)) {
                                if (!_.isNil($scope.GRBNo)) {
                                    $scope.YLocation = _.find(YStoreLocations, function(item) {
                                        return item.YLocation === response[0].XLocation;
                                    });

                                    $scope.LocationInfo.YLocation = _.find(YStoreLocations, function(item) {
                                        return item.YLocation === response[0].XLocation;
                                    });
                                }
                            }
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        if (!_.isNil($stateParams.GRBNo)) {
            $scope.GRBNo = $stateParams.GRBNo;
            $scope.getMaterialIssues($stateParams.GRBNo);
            $scope.getSelectedStockDetail($stateParams.GRBNo);
        }

        /**
         * the function working when click a row of In stock table
         */
        $scope.selectMaterialInStock = function(item) {
            if (_.isNil(WorkOrderID)) {
                var state = 'department.goods-in';
                var params = {
                    WorkOrderID: undefined,
                    GRBNo: item.GRBNo
                };
                $rootScope.redirectDept(state, params);
            } else {
                API.procedure.post('ProcGetMaterialIssueDetail', {
                        WorkOrderID: WorkOrderID,
                        GRBID: item.GRBNo
                    })
                    .then(function(result) {
                        $scope.MaterialIssueDetail = result[0];
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * get Location drop-down
         */
        API.procedure.get('ProcGetStoreAreaList')
            .then(function(result) {
                $scope.StoreAreaList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * the function working when user select Location drop-down
         */
        $scope.changeLocation = function(Location) {

            /**
             * populate X - Y Location
             */
            $scope.getXLocationProc(Location.StoreAreaID);
            $scope.getYLocationProc(Location.StoreAreaID);
        };

        $scope.getXLocationProc = function(StoreAreaID) {
            API.procedure.post('ProcGetXStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.XStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.getYLocationProc = function(StoreAreaID) {
            API.procedure.post('ProcGetYStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.YStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click Update Loc'n button
         */
        $scope.updateLocation = function(LocationInfo) {
            if (_.isNil(LocationInfo)) {
                Notification.show('warning', 'Please select Location');
                return false;
            }
            if (_.isNil(LocationInfo.XLocation)) {
                Notification.show('warning', 'Please select XLocation');
                return false;
            }
            if (_.isNil(LocationInfo.YLocation)) {
                Notification.show('warning', 'Please select YLocation');
                return false;
            }
            $scope.selectedMaterialInstock = _.find($scope.MaterialInStock, {
                'selected': true
            });
            if (!_.isUndefined($scope.selectedMaterialInstock)) {
                API.procedure.post('ProcUpdateStockLocation', {
                        GRBNo: $scope.selectedMaterialInstock.GRBNo,
                        StoreAreaID: LocationInfo.Location.StoreAreaID,
                        Xlocation: LocationInfo.XLocation.XLocation,
                        Ylocation: LocationInfo.YLocation.YLocation,
                    })
                    .then(function(result) {
                        $state.reload();
                        Notification.show('success', 'Update Location success');
                    })
                    .catch(function(error) {
                        throw error;
                        // Notification.show('warning', 'Location updated');
                    });

            } else {
                Notification.show('warning', 'Please select Material in stock item');
                return false;
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        /**
         * Receive Screen
         */
        if (_.isUndefined(WorkOrderID)) {

            // Use to show/hide Approval mismatch
            $scope.hasApprovalMismatch = false;

            /**
             * Data to add new GRB
             */
            $scope.GRB = new GoodsIn();

            /**
             * get Approval type drop-down
             */
            API.procedure.get('ProcGetApprovalList')
                .then(function(result) {
                    $scope.ApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Supplier / Customer drop-down
             */
            API.procedure.get('ProcGetCustomerSupplierList')
                .then(function(result) {
                    $scope.CustomerSupplierList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get UOM drop-down
             */
            API.procedure.get('ProcGetUOMList')
                .then(function(result) {
                    $scope.UOMList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Shape drop-down
             */
            API.procedure.get('ProcGetMaterialShapes')
                .then(function(result) {
                    $scope.MaterialShapes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Rejects drop-down
             */
            API.procedure.get('ProcGetRejectsList')
                .then(function(result) {
                    $scope.RejectsList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get PO number drop-down
             */
            API.procedure.get('ProcGetPOList')
                .then(function(result) {
                    $scope.POList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when select PO number drop-down
             */
            $scope.changePOList = function(PO) {
                $scope.getPOItemsList(PO.POnID);
            };

            /**
             * the function working when select Item drop-down
             */
            $scope.changePOItem = function(POItem) {
                if (!_.isNil(POItem) && _.isNil($scope.GRBNo)) {

                    /**
                     * populate PODetail
                     */
                    API.procedure.post('ProcGetPOItemStoresDetail', {
                            ItemID: POItem.ItemID
                        })
                        .then(function(result) {
                            $scope.POItemStoresDetail = result[0];

                            $scope.Unit = _.find($scope.Units, function(item) {
                                return item.id === parseInt(result[0].UnitID);
                            });

                            $scope.GRB = $scope.GRB.setOptions(result[0]);
                            $scope.GRB.QtyRcvd = parseInt($scope.GRB.Quantity);

                            // Selected Approval drop-down
                            $scope.Approval = _.find($scope.ApprovalList, function(item) {
                                return item.pkApprovalID === $scope.POItemStoresDetail.ApprovalTypeID;
                            });

                            // Selected UOM drop-down
                            $scope.UOM = _.find($scope.UOMList, function(item) {
                                return item.pkUOMID === $scope.POItemStoresDetail.UOMID;
                            });

                            // Selected Shape drop-down
                            $scope.Shape = _.find($scope.MaterialShapes, function(item) {
                                return item.pkShapeID === $scope.POItemStoresDetail.ShapeID;
                            });

                            // Bind data to Work Order No.
                            $scope.GRB.WorkOrderNo = $scope.POItemStoresDetail.WONo;

                            // Bind data to Description
                            $scope.GRB.Description = $scope.POItemStoresDetail.Description;
                            $scope.GRB.Size = $scope.POItemStoresDetail.Size;
                            $scope.GRB.SupplierName = $scope.POItemStoresDetail.SupplierName;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            /**
             * the function working when click 'Generate next GR' button
             */
            $scope.generateNextGR = function() {
                API.procedure.post('ProcGenerateNewGRB', {
                        UserID: $rootScope.user.getId()
                    })
                    .then(function(result) {
                        $scope.GRBNo = undefined;
                        $scope.clearFields();
                        $scope.GRB.nextGR = result[0].GRBNo;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get GR type drop-down
             */
            API.procedure.get('ProcGetMaterialTypeList')
                .then(function(result) {
                    $scope.MaterialTypeList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get unknow drop-down when GR type drop-down = FI Material
             */
            API.procedure.get('ProcPartTemplateList')
                .then(function(result) {
                    if (result.length > 0) {
                        result.forEach(function(value, key) {
                            result[key].DisplayData = value.PartNumber + ' // ' + value.Issue + ' // ' + value.COS;
                        });
                        $scope.PartTemplateList = result;
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when select GR type drop-down
             */
            $scope.changeFIMaterial = function(FIMaterial) {
                /**
                 * populate Customer Order No. drop-down
                 */
                API.procedure.post('ProcGetMatchingCOList', {
                        PartTemplateID: FIMaterial.PTemplateID
                    })
                    .then(function(result) {
                        $scope.MatchingCOList = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * the function working when select Customer Order drop-down
             */
            $scope.changeCustomerOrder = function(CustomerOrder) {

                /**
                 * populate Work Order No. drop-down
                 */
                if (!_.isNil(CustomerOrder) && !_.isNil(CustomerOrder.OrderID) && CustomerOrder.OrderID !== '') {
                    API.procedure.post('ProcGetMatchingLineItems', {
                            OrderID: CustomerOrder.OrderID
                        })
                        .then(function(result) {
                            $scope.MatchingLineItems = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            /**
             * the function working when select Approval drop-down
             */
            $scope.changeApproval = function(Approval) {
                if (!_.isUndefined($scope.POItemStoresDetail) && !_.isNil($scope.POItemStoresDetail.ApprovalTypeID)) {
                    if (parseInt(Approval.pkApprovalID) !== parseInt($scope.POItemStoresDetail.ApprovalTypeID)) {
                        $scope.hasApprovalMismatch = true;
                    } else {
                        $scope.hasApprovalMismatch = false;
                    }
                }
            };

            $scope.changeUnit = function(Unit) {
                $scope.Unit = Unit;
            };

            /**
             * the function working when click 'Add' button
             */
            $scope.AddOrUpdateGRB = function(GRB) {
                var numberRegex = /^\d+$/;
                if (_.isNil($scope.GRType)) {
                    Notification.show('warning', 'Please select GR type');
                    return false;
                }
                if (!_.isNil(GRB.QtyRcvd)) {
                    if (!numberRegex.test(GRB.QtyRcvd)) {
                        Notification.show('warning', 'Qty Received must be number');
                        return false;
                    }
                }
                if (!_.isNil(GRB.QtyManu)) {
                    if (!numberRegex.test(GRB.QtyManu)) {
                        Notification.show('warning', 'Qty to Manufacture must be number');
                        return false;
                    }
                }
                if (_.isNil(GRB.Location) || GRB.Location === '') {
                    Notification.show('warning', 'Please select Location');
                    return false;
                }
                if (_.isNil($scope.XLocation) || $scope.XLocation === '') {
                    Notification.show('warning', 'Please select XLocation');
                    return false;
                }
                if (_.isNil($scope.YLocation) || $scope.YLocation === '') {
                    Notification.show('warning', 'Please select YLocation');
                    return false;
                }
                if (_.isUndefined(GRB.FastTrack)) {
                    GRB.FastTrack = null;
                }
                var RcvdDate;
                if (_.isNil(GRB.getRcvdDate())) {
                    RcvdDate = moment().format('YYYY-MM-DD');
                } else {
                    RcvdDate = GRB.RcvdDate.split('/');
                    RcvdDate = RcvdDate[2] + '-' + RcvdDate[1] + '-' + RcvdDate[0];
                }
                if (GRB.getReleaseNoteNo() === undefined) {
                    GRB.ReleaseNoteNo = '';
                }
                if (_.isUndefined(GRB.TheirWONo) || GRB.TheirWONo === '') {
                    GRB.TheirWONo = null;
                }
                if (!_.isNil($scope.GRBNo)) {
                    API.procedure.post('ProcUpdateSelectedStockDetail', {
                            UserID: $rootScope.user.getId(),
                            GRBNo: $scope.GRBNo,
                            CustomerName: GRB.getCustomer(),
                            Description: GRB.getDescription(),
                            QtyRcvd: GRB.getQtyRcvd(),
                            QtyManu: GRB.getQtyManu(),
                            Size: GRB.getSize(),
                            UOMID: GRB.getUOMID(),
                            UOMID2: GRB.getUOMID2(),
                            ShapeID: GRB.getShapeID(),
                            ReleaseNoteNo: GRB.getReleaseNoteNo(),
                            DeliveryNoteNo: GRB.getDeliveryNoteNo(),
                            PartNumber: GRB.getPartNumber(),
                            StoreAreaID: GRB.getStoreAreaID(),
                            XLocation: $scope.YLocation.YLocation,
                            YLocation: $scope.XLocation.XLocation,
                            ApprovalTypeID: GRB.getApprovalTypeID(),
                            TheirWONo: GRB.getTheirWONo()
                        })
                        .then(function(response) {
                            Notification.show('success', 'Update GRB success');
                            $scope.GRBForm.$setPristine();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    API.procedure.post('ProcAddGRB', {
                            UserID: $rootScope.user.getId(),
                            RcvdDate: RcvdDate,
                            MaterialTypeID: $scope.GRType.MaterialType,
                            OrderID: GRB.getOrderID(),
                            LineItemID: GRB.getLineItemID(),
                            POnID: GRB.getPOnID(),
                            ItemID: GRB.getItemID(),
                            ETemplateID: GRB.getETemplateID(),
                            WorkOrderNo: GRB.getWorkOrderNo(),
                            ReleaseNoteNo: GRB.getReleaseNoteNo(),
                            ID: GRB.getID(),
                            Type: GRB.getType(),
                            Description: GRB.getDescription(),
                            DeliveryNoteNo: GRB.getDeliveryNoteNo(),
                            PartNumber: GRB.getPartNumber(),
                            QtyRcvd: GRB.getQtyRcvd(),
                            QtyManu: GRB.getQtyManu(),
                            Size: GRB.getSize(),
                            UOMID: GRB.getUOMID(),
                            UOMID2: GRB.getUOMID2(),
                            ShapeID: GRB.getShapeID(),
                            StoreAreaID: GRB.getStoreAreaID(),
                            XLocation: $scope.YLocation.YLocation,
                            YLocation: $scope.XLocation.XLocation,
                            DirectIssue: GRB.getDirectIssue(),
                            ApprovalTypeID: GRB.getApprovalTypeID(),
                            RejectNoteID: GRB.getRejectNoteID(),
                            FastTrack: GRB.getFastTrack(),
                            TheirWONo: GRB.getTheirWONo(),
                            Customer: GRB.getCustomer()
                        })
                        .then(function(result) {
                            Notification.show('success', 'Add GRB success');
                            $scope.GRB = new GoodsIn();
                            $scope.GRType = null;
                            $scope.PONumber = null;
                            $scope.POItem = null;
                            $scope.XLocation = null;
                            $scope.YLocation = null;
                            $scope.GRBForm.$setPristine();
                            $scope.getMaterialInStock();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            $scope.beforeUpload = function(file) {
                if (!$scope.GRB || !$scope.GRB.nextGR) {
                    Notification.show('warning', 'Please click "Generate next GR" button');
                    return false;
                }
                if (_.isNil($scope.DocType) || _.isNil($scope.DocType.ID)) {
                    Notification.show('warning', 'Please select DocType');
                    return false;
                }
            };

            $scope.uploadSuccess = function(response) {
                API.procedure.post('ProcDropDeliveryDocument', {
                        UserID: $rootScope.user.getId(),
                        GRBNo: $scope.GRB.nextGR,
                        DocTypeID: $scope.DocType.ID,
                        FileName: response.data
                    })
                    .then(function(result) {
                        Notification.show('success', 'Drop Delivery Doc success');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.uploadError = function(error) {
                Notification.show('warning', error.error);
            };
        }

        $scope.clearFields = function() {
            $scope.GRB = new GoodsIn();
            $scope.GRType = null;
            $scope.PONumber = null;
            $scope.POItem = null;
            $scope.Approval = null;
            $scope.UOM = null;
            $scope.UOM2 = null;
            $scope.Shape = null;
            $scope.XLocation = null;
            $scope.YLocation = null;
        };

        /**
         * populate Item drop-down
         */
        $scope.getPOItemsList = function(POnID) {
            API.procedure.post('ProcGetPOItemsList', {
                    POnID: POnID
                })
                .then(function(result) {
                    $scope.POItemsList = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.redirectWOpage = function(WorkOrderID) {
            if (!_.isNil(WorkOrderID)) {
                var state = 'department.workorders.main';
                var params = {
                    WorkOrderID: WorkOrderID
                };
                $rootScope.redirectDept(state, params);
            }
        };

        $scope.redirectCustomerOrder = function(Qty) {
            if (!_.isNil(Qty)) {
                var state = 'department.customer-order';
                var params = {};
                $rootScope.redirectDept(state, params);
            }
        };

        $scope.redirectPartListpage = function(PartNumber) {
            if (!_.isNil(PartNumber)) {
                var state = 'department.part-control.part-list';
                var params = {
                    PartNumber: PartNumber
                };
                $rootScope.redirectDept(state, params);
            }
        };

        $scope.chooseFastTrack = function() {
            if (!_.isNil($scope.GRB)) {
                Notification.show('success', 'Fasttrack enabled');
                $scope.GRB.FastTrack = 1;
            }
        };

        $scope.allocatedReport = function() {
            var selectedMaterialInstock = _.find($scope.MaterialInStock, { 'selected': true });
            if (!_.isNil(selectedMaterialInstock) && !_.isNil(selectedMaterialInstock.GRBNo)) {
                $rootScope.openTabBirt('Allocated', {
                    GRBNo: selectedMaterialInstock.GRBNo
                });
            } else {
                Notification.show('warning', 'Please select instock item');
                return false;
            }
        };

        $scope.refreshInStock = function() {
            $scope.getMaterialInStock();
        };

        $scope.openBIRT = function(type) {
            switch (type) {
                case 'DueIn':
                    $rootScope.openTabBirt(type, params);
                    break;
                case 'PurchaseOrder_v5':
                    if (_.isNil($scope.GRB) || _.isNil($scope.GRB.POnID)) {
                        Notification.show('warning', 'Please select PO number');
                        return false;
                    }
                    var params = {
                        PONo: $scope.GRB.POnID
                    };
                    $rootScope.openTabBirt(type, params);
                    break;
            }
        };
    }

})(angular.module('app.components.department.goods-in', [
    'angular.filter',
    'app.template'
]));