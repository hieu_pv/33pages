require('./main/');
require('./cocs/');
require('./pos/');
require('./costing/');
require('./invoice/');
require('./route-card/');
require('./material-issue/');
require('./document/');
(function(app) {
    app.controller('WorkordersCtrl', WorkordersCtrl);

    WorkordersCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', '$window'];

    function WorkordersCtrl($rootScope, $scope, $state, $stateParams, API, $window) {
        $scope.isLiveOrder = 1;

        $scope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            if (from.name.indexOf('workorders') === -1) {
                var isNull = false;
                var params = ['WorkOrderID'];
                _.forEach(params, function(param) {
                    if (_.isNil(toParams[param])) {
                        isNull = true;
                    }
                });

                // Remove search element
                if (isNull) {
                    $scope.WorkOrderDetail = null;
                    $scope.WorkOrderID = null;
                    $scope.PONo = null;
                    $scope.WorkOrdersForPart = null;
                }
            }
        });

        /**
         * selected Matching Work Order Dropdown
         */
        if ($stateParams.WorkOrderID !== undefined) {
            $scope.$watch('WorkOrdersForParts', function(WorkOrdersForParts) {
                if (WorkOrdersForParts !== undefined) {
                    $scope.WorkOrdersForPart = _.find(WorkOrdersForParts, { 'pkWorkOrderID': parseInt($stateParams.WorkOrderID) });
                }
            });
        }

        /**
         * redirect to Customer Order page
         */
        $scope.redirectCOpage = function(WorkOrderDetail) {
            var state = 'department.customer-order';
            var params = {};
            if (!_.isNil(WorkOrderDetail)) {
                var hasCustomerID = false;
                var hasCustomerOrderId = false;
                var hasCustomerOrderRef = false;
                if (!_.isNil(WorkOrderDetail.CustomerID) && WorkOrderDetail.CustomerID !== '') {
                    hasCustomerID = true;
                }
                if (!_.isNil(WorkOrderDetail.OrderID) && WorkOrderDetail.OrderID !== '') {
                    hasCustomerOrderId = true;
                }
                if (!_.isNil(WorkOrderDetail.CustomerOrderRef) && WorkOrderDetail.CustomerOrderRef !== '') {
                    hasCustomerOrderRef = true;
                }
                if (hasCustomerID && hasCustomerOrderId && hasCustomerOrderRef) {
                    params = {
                        CustomerId: WorkOrderDetail.CustomerID,
                        CustomerOrderId: WorkOrderDetail.OrderID,
                        CustomerOrderRef: WorkOrderDetail.CustomerOrderRef
                    };
                    $rootScope.redirectDept(state, params);
                }
            }
        };

        $scope.WorkOrderNoSelected = function(item, model) {
            var WorkOrderID = item.pkWorkOrderID;
            $scope.getWorkOrderInfo(WorkOrderID);
            var state = 'department.workorders.main';
            var params = {
                WorkOrderID: WorkOrderID
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.getWorkOrderInfo = function(WorkOrderID) {

            /**
             * get WorkOrder Detail
             */
            API.procedure.post('ProcGetWorkOrderDetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WorkOrderDetail = result[0];
                    $rootScope.PONo = result[0];
                    if (result.length > 0) {
                        $rootScope.PONo = result[0].PartNumber;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * Change input SelectedPO
         */
        $scope.changeSelectedPO = function(PONo) {
            API.procedure.post('ProcPartNumberSearch', {
                    PartNumberSearchField: PONo
                })
                .then(function(result) {
                    $scope.PONoList = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.ChoosePONo = function(evt) {
            if (evt.keyCode === 13) {
                $rootScope.PONo = evt.target.value;

                /**
                 * Matching WorkOrder through Part Number
                 */
                API.procedure.post('ProcWorkOrdersForPart', {
                        PartNumber: $rootScope.PONo
                    })
                    .then(function(result) {
                        $scope.WorkOrdersForParts = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.$watch('PONo', function(value) {
            if (!_.isNil(value)) {

                /**
                 * Matching WorkOrder through Part Number
                 */
                API.procedure.post('ProcWorkOrdersForPart', {
                        PartNumber: value
                    })
                    .then(function(result) {
                        $scope.WorkOrdersForParts = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        });

        $scope.changeWorkOrdersForPart = function(WorkOrdersForPart) {
            var WorkOrderID = WorkOrdersForPart.pkWorkOrderID;
            $scope.getWorkOrderInfo(WorkOrderID);
            var state = 'department.workorders.main';
            var params = {
                WorkOrderID: WorkOrderID
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectPartControl = function(PTemplateID, ETemplateID) {
            var state = 'department.part-control.part-list';
            var params = {};
            if (!_.isNil(PTemplateID)) {
                params.PTemplateID = PTemplateID;
            }
            if (!_.isNil(ETemplateID)) {
                params.ETemplateID = ETemplateID;
            }
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectBack = function() { 
            $window.history.back();
        };

    }

})(angular.module('app.components.department.workorders', [
    'app.components.department.workorders.main',
    'app.components.department.workorders.cocs',
    'app.components.department.workorders.pos',
    'app.components.department.workorders.costing',
    'app.components.department.workorders.invoice',
    'app.components.department.workorders.route-card',
    'app.components.department.workorders.material-issue',
    'app.components.department.workorders.document',
]));