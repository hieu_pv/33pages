(function(app) {
    app.controller('WorkOrderRouteCardCtrl', WorkOrderRouteCardCtrl);
    WorkOrderRouteCardCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API'];

    function WorkOrderRouteCardCtrl($rootScope, $scope, $stateParams, API) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            // var params = {
            //     WorkOrderID: WorkOrderID
            // };
            // $rootScope.openTabBirt('RouteCard_2016', params);

            /**
             * get Route Card
             */
            API.birt.post('RouteCard_2016', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.birtRouteCard = result;
                })
                .catch(function(error) {
                    throw error;
                });
        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.route-card', []));
