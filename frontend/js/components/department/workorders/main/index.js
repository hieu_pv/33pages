(function(app) {
    app.controller('WorkOrderMainCtrl', WorkOrderMainCtrl);
    WorkOrderMainCtrl.$inject = ['$rootScope', '$scope', '$stateParams', '$state', 'API', 'Notification', '$localStorage'];

    function WorkOrderMainCtrl($rootScope, $scope, $stateParams, $state, API, Notification, $localStorage) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (!_.isNil(WorkOrderID) && WorkOrderID !== '') {
            if (_.isNil($scope.WorkOrderDetail)) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get Work Order Notes
             */
            API.procedure.post('ProcGetWorkOrderNotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WorkOrderNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Add Order Notes
             */
            $scope.AddOrderNotes = function(Notes) {
                if (!_.isNil(Notes)) {
                    API.procedure.post('ProcAddWorkOrderNote', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            Note: Notes
                        })
                        .then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            $scope.WOCancel = function() {
                API.procedure.post('ProcSetWOCancel', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Set WorkOrder Cancel success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.redirectEngineering = function() {
                var state = 'department.engineering-template.material';
                var params = {
                    ETemplateID: undefined,
                    WorkOrderID: $stateParams.WorkOrderID
                };
                $rootScope.redirectDept(state, params);
            };

            /**
             * redirect to Production Schedule page
             */
            $scope.redirectPSpage = function() {
                var state = 'department.production-schedule.material-issue';
                $rootScope.redirectDept(state);
            };

            $scope.redirectMachineList = function(MachineName) {
                var state = 'department.machine-list';
                var params = {
                    MachineName: MachineName
                };
                $rootScope.redirectDept(state, params);
            };

            delete $localStorage.routeCardData;
            delete $localStorage.routeReload;
            $scope.viewRouteCard = function() {
                API.birt.post('RouteCard_2016', {
                        WorkOrderID: $stateParams.WorkOrderID
                    })
                    .then(function(result) {
                        $localStorage.routeCardData = result;
                        // var url = $state.href('route-card', {}, {
                        //     reload: true
                        // });
                        var url = $state.href('route-card', {});
                        window.open(url, '_blank');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.main', []));
