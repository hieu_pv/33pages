(function(app) {
    app.controller('WorkOrderPosCtrl', WorkOrderPosCtrl);
    WorkOrderPosCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API'];

    function WorkOrderPosCtrl($rootScope, $scope, $stateParams, API) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (!_.isNil(WorkOrderID) && WorkOrderID !== '') {
            if (_.isNil($scope.WorkOrderDetail)) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get Work Order GRBs
             */
            API.procedure.post('ProcGetWorkOrderGRBs', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    result = _.map(result, function(item) {
                        var regex = /^\d+$/;
                        if (!_.isNil(item.RED) && regex.test(item.RED)) {
                            item.RED = parseInt(item.RED);
                        }
                        return item;
                    });
                    $scope.WorkOrderGRBs = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.redirectGoodsIn = function(GRB) {
                var state = 'department.goods-in';
                var params = {
                    WOGRBNo: GRB
                };
                $rootScope.redirectDept(state, params);
            };

            $scope.redirectPurchaseOrder = function(item) {
                var state = 'department.purchase-order.details';
                var params = {};
                if (!_.isNil(item.PO) && item.PO !== '') {
                    params.PONo = item.PO;
                }
                if (!_.isNil(item.RFQ) && item.RFQ !== '') {
                    params.RFQNo = item.RFQ;
                }
                $rootScope.redirectDept(state, params);
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }
    }
})(angular.module('app.components.department.workorders.pos', []));
