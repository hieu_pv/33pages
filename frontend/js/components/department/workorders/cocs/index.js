var moment = require('moment');

(function(app) {
    app.controller('WorkOrderCocsCtrl', WorkOrderCocsCtrl);
    WorkOrderCocsCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function WorkOrderCocsCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get GRB table
             */
            API.procedure.post('ProcGetWorkOrderCoCInfo', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    result.forEach(function(value, key) {
                        if (parseInt(value.Display) === 1) {
                            result[key].Display = true;
                        } else {
                            result[key].Display = false;
                        }
                        if (parseInt(value.Print) === 1) {
                            result[key].Print = true;
                        } else {
                            result[key].Print = false;
                        }
                    });
                    console.log(result);
                    $scope.OrderCoCInfo = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.PrintCoC = function(OrderCoCInfo) {
                var params = {
                    WorkOrderID: WorkOrderID
                };
                $rootScope.openTabBirt('CoC_v6b', params);
            };

            $scope.getNowDate = function() {
                $scope.nowDate = moment().format('DD/MM/YYYY');

                /**
                 * get Invoice textbox
                 */
                API.procedure.get('ProcGetNextOutvoiceNo')
                    .then(function(result) {
                        if (result.length > 0) {
                            $scope.NextOutvoiceNo = result[0].InvoiceNo;
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get Next Credit Note
             */
            API.procedure.get('ProcGetNextCreditNote')
                .then(function(result) {
                    $scope.NextCreditNote = result[0].ReturnNoteNo;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Invoice drop-down when user selected Credit
             */
            API.procedure.post('ProcGetWOOutvoiceList', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOOutvoiceList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Create Outvoice Credit
             */
            $scope.CreateOutvoiceCredit = function(OutvoiceCredit) {
                if (OutvoiceCredit === undefined) {
                    Notification.show('warning', 'Please enter full information!');
                    return false;
                }
                if (!OutvoiceCredit.InvoiceNo) {
                    Notification.show('warning', 'Please select Invoice!');
                    return false;
                }
                if (!OutvoiceCredit.Quantity) {
                    Notification.show('warning', 'Please enter Quantity!');
                    return false;
                }
                var number_regex = /^\d+$/;
                if (!number_regex.test(OutvoiceCredit.Quantity)) {
                    Notification.show('warning', 'format Quantity is number');
                    return false;
                }
                if (!OutvoiceCredit.IdentMarks) {
                    Notification.show('warning', 'Please enter IdentMarks!');
                    return false;
                }
                if (OutvoiceCredit.CoCReqd === undefined) {
                    OutvoiceCredit.CoCReqd = false;
                } else {
                    OutvoiceCredit.CoCReqd = true;
                }
                API.procedure.post('ProcCreateOutvoiceCredit', {
                        UserID: $rootScope.user.getId(),
                        InvoiceNo: OutvoiceCredit.InvoiceNo.InvoiceNo,
                        CoCReqd: OutvoiceCredit.CoCReqd,
                        Quantity: OutvoiceCredit.Quantity,
                        IdentMarks: OutvoiceCredit.IdentMarks
                    })
                    .then(function(result) {
                        var NextCreditNote = parseInt($scope.NextCreditNote) + 1;
                        Notification.show('success', 'Return Note ' + NextCreditNote + ' generated');
                        if (!_.isNil(result[0].ReturnNoteNo)) {
                            var creditNoteParams = {
                                ReturnNoteID: result[0].ReturnNoteNo
                            };
                            $rootScope.openTabBirt('CreditNote', creditNoteParams);
                        }
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.updateWOCoCInfo = function(PaperworkComplete) {
                API.procedure.post('ProcUpdateWorkOrderCoCInfo', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID,
                        PaperworkComplete: PaperworkComplete
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get Material Notes table
             */
            API.procedure.post('ProcGetWOMaterialNotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOMaterialNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Conformity Notes table
             */
            API.procedure.post('ProcGetWOConformityNotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOConformityNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.updateIRList = function(item) {
                if (!item.GRB) {
                    Notification.show('warning', 'Please enter GRB!');
                    return false;
                }
                var number_regex = /^\d+$/;
                if (!number_regex.test(item.GRB)) {
                    Notification.show('warning', 'format GRB is number!');
                    return false;
                }

                API.procedure.post('ProcUpdateIRList', {
                        UserID: $rootScope.user.getId(),
                        GRBNo: item.GRB,
                        Display: item.Display,
                        Print: item.Print
                    })
                    .then(function(result) {
                        Notification.show('success', 'updateIRList success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.CreateWOOutvoice = function(InvoiceNo) {
                if (InvoiceNo === undefined) {
                    Notification.show('warning', 'Please select Invoice and double click date input to get InvoiceNo');
                    return false;
                }
                API.procedure.post('ProcCreateWOOutvoice', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: $rootScope.WorkOrderID,
                        InvoiceNo: InvoiceNo
                    })
                    .then(function(result) {
                        Notification.show('success', 'Create WorkOrder Outvoice success!');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * show Credit
             */
            $scope.isShown = function(showCredit) {
                return showCredit === $scope.showCredit;
            };

            $scope.generateImage = function(OrderCoCInfo) {
                var fileName = OrderCoCInfo.FileName;
                var file = $rootScope.PathImageUpload + fileName;
                $window.open(file, 'blank_');
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.cocs', []));
