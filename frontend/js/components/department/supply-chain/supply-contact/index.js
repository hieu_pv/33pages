var SupplyChainHours = require('../../../../models/SupplyChainHours');

(function(app) {

    app.controller('SupplyContactCtrl', SupplyContactCtrl);
    SupplyContactCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function SupplyContactCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {
        var SupplierID = $scope.SupplierID = parseInt($stateParams.SupplierID);
        if (SupplierID !== undefined && SupplierID !== null) {
            $scope.getSupplyInfo(SupplierID);

            /**
             * get OpenHours
             */
            var days = [{
                key: '1',
                name: 'Monday'
            }, {
                key: '2',
                name: 'Tuesday'
            }, {
                key: '3',
                name: 'Wednesday'
            }, {
                key: '4',
                name: 'Thursday'
            }, {
                key: '5',
                name: 'Friday'
            }, {
                key: '6',
                name: 'Staturday'
            }, {
                key: '7',
                name: 'Sunday'
            }, ];
            $scope.days = days;
            API.procedure.post('ProcGetOpenHours', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    for (var value in result[0]) {
                        if (result[0][value] === null || result[0][value] === '00:00:00') {
                            result[0][value] = 'close';
                        } else {
                            result[0][value] = result[0][value].slice(0, -3).replace(':', '');
                        }
                    }
                    $scope.OpenHours = new SupplyChainHours(result[0]);
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Update OpenHours
             */
            $scope.UpdateOpenHours = function(OpenHours) {
                var data = {};
                for (var key in OpenHours) {
                    if (OpenHours[key] === 'close') {
                        data[key] = '';
                    }
                    var regex = /^\d{4}$/;
                    if (!regex.test(OpenHours[key])) {
                        data[key] = '000000';
                    } else {
                        data[key] = OpenHours[key] + "00";
                    }
                }

                API.procedure.post('ProcUpdateOpenHours', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        Day1Open: data.Day1Open,
                        Day1Close: data.Day1Close,
                        Day2Open: data.Day2Open,
                        Day2Close: data.Day2Close,
                        Day3Open: data.Day3Open,
                        Day3Close: data.Day3Close,
                        Day4Open: data.Day4Open,
                        Day4Close: data.Day4Close,
                        Day5Open: data.Day5Open,
                        Day5Close: data.Day5Close,
                        Day6Open: data.Day6Open,
                        Day6Close: data.Day6Close,
                        Day7Open: data.Day7Open,
                        Day7Close: data.Day7Close,
                    })
                    .then(function(result) {
                        Notification.show('success', 'Update success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get Supplier Additional
             */
            API.procedure.post('ProcGetSupplierAdditional', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierAdditional = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.RmvAdditional = function(SupplierAdditional) {
                var selectedAdditional = _.filter(SupplierAdditional, _.matches({
                    'selected': true
                }));
                if (selectedAdditional.length > 0) {
                    selectedAdditional.forEach(function(value) {

                        API.procedure.post('ProcRemoveSupplierAdditional', {
                                UserID: $rootScope.user.getId(),
                                AdditionalID: value.AdditionalID
                            })
                            .then(function(result) {
                                _.remove(SupplierAdditional, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                }
            };

            /**
             * get Supplier Personnel
             */
            API.procedure.post('ProcGetSupplierPersonnel', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.CountTotal(result[0]);
                    $scope.SupplierPersonnel = result[0];
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.CountTotal = function(SupplierPersonnel) {
                $scope.Total = 0;
                var RegexNumber = /^\d+$/;
                if (RegexNumber.test(SupplierPersonnel.Type1)) {
                    $scope.Total += parseInt(SupplierPersonnel.Type1);
                }

                if (RegexNumber.test(SupplierPersonnel.Type2)) {
                    $scope.Total += parseInt(SupplierPersonnel.Type2);
                }

                if (RegexNumber.test(SupplierPersonnel.Type3)) {
                    $scope.Total += parseInt(SupplierPersonnel.Type3);
                }

                if (RegexNumber.test(SupplierPersonnel.Type4)) {
                    $scope.Total += parseInt(SupplierPersonnel.Type4);
                }

                if (RegexNumber.test(SupplierPersonnel.Type5)) {
                    $scope.Total += parseInt(SupplierPersonnel.Type5);
                }
            };

            $scope.UpdatePersonnel = function(SupplierPersonnel) {
                $scope.CountTotal(SupplierPersonnel);
                API.procedure.post('ProcAmendSupplierPersonnel', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        Type1: parseInt(SupplierPersonnel.Type1),
                        Type2: parseInt(SupplierPersonnel.Type2),
                        Type3: parseInt(SupplierPersonnel.Type3),
                        Type4: parseInt(SupplierPersonnel.Type4),
                        Type5: parseInt(SupplierPersonnel.Type5),
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get Supplier Notes
             */
            API.procedure.post('ProcGetSupplierNotes', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Supplier Contacts
             */
            API.procedure.post('ProcGetSupplierContacts', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierContacts = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Add Notes
             */
            $scope.AddNote = function(Note) {
                API.procedure.post('ProcAddSupplierNote', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        Note: Note
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.AddSupplierContact = function(Contact) {
                if (Contact === undefined) {
                    Notification.show('warning', 'Please enter full information');
                    return false;
                }
                if (Contact.ContactName === undefined) {
                    Notification.show('warning', 'Please enter Name');
                    return false;
                }
                if (Contact.Phone === undefined) {
                    Notification.show('warning', 'Please enter Phone');
                    return false;
                }
                var regexPhone = /^\d+[ ]?\d*$/;
                if (!regexPhone.test(Contact.Phone)) {
                    Notification.show('warning', 'Phone must be number');
                    return false;
                }
                if (Contact.Email === undefined) {
                    Notification.show('warning', 'Please enter Email');
                    return false;
                }
                var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!regexEmail.test(Contact.Email)) {
                    Notification.show('warning', 'Email is not valid');
                    return false;
                }
                if (Contact.Prime === undefined) {
                    Contact.Prime = false;
                } else {
                    Contact.Prime = true;
                }

                API.procedure.post('ProcAddSupplierContact', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        ContactName: Contact.ContactName,
                        Phone: Contact.Phone,
                        Email: Contact.Email,
                        Prime: Contact.Prime
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.RmvContact = function(SupplierContacts) {
                if (SupplierContacts.length > 0) {
                    var selectedContact = _.filter(SupplierContacts, _.matches({
                        'selected': true
                    }));
                    if (selectedContact.length > 0) {
                        selectedContact.forEach(function(value) {
                            API.procedure.post('ProcRemoveSupplierContact', {
                                    UserID: $rootScope.user.getId(),
                                    ContactID: value.ContactID
                                })
                                .then(function(result) {
                                    // _.remove(SupplierContacts, value);
                                    $state.reload();
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        });
                    } else {
                        Notification.show('warning', 'Please select contact');
                        return false;
                    }
                }
            };

            /**
             * Add Supplier Additional
             */
            $scope.AddSupplierAdditional = function(Additional) {
                if (Additional === undefined) {
                    Notification.show('warning', 'Please enter full information');
                    return false;
                }
                if (Additional.AdditionalTypeID === undefined) {
                    Notification.show('warning', 'Please select Additional');
                    return false;
                }
                if (Additional.StartPoint === undefined) {
                    Notification.show('warning', 'Please enter StartPoint');
                    return false;
                }
                if (Additional.EndPoint === undefined) {
                    Notification.show('warning', 'Please enter EndPoint');
                    return false;
                }

                API.procedure.post('ProcAddSupplierAdditional', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        AdditionalTypeID: Additional.AdditionalTypeID.value,
                        StartPoint: Additional.StartPoint,
                        EndPoint: Additional.EndPoint
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            $state.go('department.supply-chain.main');
        }
    }

})(angular.module('app.components.department.supply-chain.supply-contact', []));
