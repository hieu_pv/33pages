(function(app) {

    app.controller('SupplyOrderHistoryCtrl', SupplyOrderHistoryCtrl);
    SupplyOrderHistoryCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function SupplyOrderHistoryCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var SupplierID = $scope.SupplierID = parseInt($stateParams.SupplierID);
        $scope.SupplierName = $stateParams.SupplierName ? $stateParams.SupplierName : '';
        if (!_.isNil(SupplierID)) {

            /**
             * get Supplier OrderHistory
             */

            API.procedure.post('ProcGetSupplierOrderHistory', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierOrderHistory = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.selectOrder = function(item) {

                /**
                 * get Supplier Order HistoryWO
                 */
                API.procedure.post('ProcGetSupplierOrderHistoryWO', {
                        PONo: item.PONo
                    })
                    .then(function(result) {
                        $scope.SupplierOrderHistoryWO = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.redirectBack = function() {
                $window.history.back();
            };

        } else {
            $state.go('department.supply-chain.main');
        }
    }


})(angular.module('app.components.department.supply-chain.supply-order-history', []));
