(function(app) {

    app.controller('SupplyPerformanceCtrl', SupplyPerformanceCtrl);
    SupplyPerformanceCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function SupplyPerformanceCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {

        // Sort Header
        $scope.sortHeader = [
            { title: 'Material', class: 'header-box Material', sort: 'material' },
            { title: 'Treatment', class: 'header-box Treatment', sort: 'treatment' },
            { title: 'Misc', class: 'header-box Misc', sort: 'miscellaneous' },
            { title: 'Vendor 100', class: 'header-box Vendor', sort: 'vrating' },
            { title: 'All', class: 'header-box All', sort: 'all' }
        ];

        /**
         * The function use to binding table header with Supplier & Performance table
         */
        $scope.chooseSortHeader = function(item) {
            item.selected = true;
            if ($scope.SupplierPerformance !== undefined) {
                if (item.sort === 'material') {
                    $scope.SupplierPerformance = _.filter($scope.filterSupplier, function(item) {
                        return parseInt(item.Material) === 1;
                    });
                }
                if (item.sort === 'treatment') {
                    $scope.SupplierPerformance = _.filter($scope.filterSupplier, function(item) {
                        return parseInt(item.Treatment) === 1;
                    });
                }
                if (item.sort === 'miscellaneous') {
                    $scope.SupplierPerformance = _.filter($scope.filterSupplier, function(item) {
                        return parseInt(item.Miscellaneous) === 1;
                    });
                }
                if (item.sort === 'vrating') {
                    $scope.SupplierPerformance = _.filter($scope.filterSupplier, function(item) {
                        return parseFloat(item.VRating) === 100;
                    });
                }
                if (item.sort === 'all') {
                    $scope.SupplierPerformance = angular.copy($scope.filterSupplier);
                }
            }
        };

        // Sort Supplier
        $scope.tableSupplier = [
            { title: 'Supplier Alias', sort: 'Alias' },
            { title: 'Total Orders', sort: 'TotalOrders' },
            { title: 'Out-standing', sort: 'Outstanding' },
            { title: 'Delivered', sort: 'Delivered' },
            { title: 'Cancelled', sort: 'Cancelled' },
            { title: 'Av.lead days', sort: 'AvLeadDays' },
            { title: 'Actaul days', sort: 'ActualDays' },
            { title: 'Early / Late', sort: 'EarlyLate' },
            { title: 'Early < 5 days', sort: 'Early' },
            { title: 'On Time', sort: 'OnTime' },
            { title: 'Late > 5 days', sort: 'Late' },
            { title: 'Rejects', sort: 'Rejects' },
            { title: 'Concessions', sort: 'concessions' },
            { title: 'Quality rating', sort: 'QRating' },
            { title: 'Vendor rating', sort: 'VRating' },
        ];
        $scope.sortSupplier = $scope.tableSupplier[0].sort;
        $scope.changeSortBy = function(item) {
            $scope.sortSupplier = item.sort;
        };

        /**
         * get Supplier Performance
         */
        API.procedure.get('ProcGetSupplierPerformance')
            .then(function(result) {
                result.forEach(function(value, key) {
                    result[key].TotalOrders = result[key]['Total Orders'];
                    if (value.ActualDays !== null && value.ActualDays !== "") {
                        value.ActualDays = parseFloat(value.ActualDays);
                    }
                    if (value.AvLeadDays !== null && value.AvLeadDays !== "") {
                        value.AvLeadDays = parseFloat(value.AvLeadDays);
                    }
                    if (value.Cancelled !== null && value.Cancelled !== "") {
                        value.Cancelled = parseFloat(value.Cancelled);
                    }
                    if (value.Delivered !== null && value.Delivered !== "") {
                        value.Delivered = parseFloat(value.Delivered);
                    }
                    if (value.Early !== null && value.Early !== "") {
                        value.Early = parseFloat(value.Early);
                    }
                    if (value.EarlyLate !== null && value.EarlyLate !== "") {
                        value.EarlyLate = parseFloat(value.EarlyLate);
                    }
                    if (value.Late !== null && value.Late !== "") {
                        value.Late = parseFloat(value.Late);
                    }
                    if (value.OnTime !== null && value.OnTime !== "") {
                        value.OnTime = parseFloat(value.OnTime);
                    }
                    if (value.Outstanding !== null && value.Outstanding !== "") {
                        value.Outstanding = parseFloat(value.Outstanding);
                    }
                    if (value.QRating !== null && value.QRating !== "") {
                        value.QRating = parseFloat(value.QRating);
                    }
                    if (value.Rejects !== null && value.Rejects !== "") {
                        value.Rejects = parseFloat(value.Rejects);
                    }
                    if (value.TotalOrders !== null && value.TotalOrders !== "") {
                        value.TotalOrders = parseFloat(value.TotalOrders);
                    }
                    if (value.VRating !== null && value.VRating !== "") {
                        value.VRating = parseFloat(value.VRating);
                    }
                    if (value.concessions !== null && value.concessions !== "") {
                        value.concessions = parseFloat(value.concessions);
                    }
                });
                $scope.SupplierPerformance = result;
                $scope.filterSupplier = angular.copy($scope.SupplierPerformance);
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Supplier PerformanceTotals
         */
        API.procedure.get('ProcGetSupplierPerformanceTotals')
            .then(function(result) {
                $scope.SupplierPerformanceTotals = result;
                var CompanyWide = _.find(result, function(item) {
                    return !_.isNil(item.CompanyWide);
                });
                if (!_.isNil(CompanyWide)) {
                    $scope.CompanyWide = CompanyWide.CompanyWide;
                } else {
                    $scope.CompanyWide = 0;
                }
            })
            .catch(function(error) {
                throw error;
            });

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }


})(angular.module('app.components.department.supply-chain.supply-performance', []));
