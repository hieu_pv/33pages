(function(app) {

    app.controller('OperationsTreatmentAdminCtrl', OperationsTreatmentAdminCtrl);
    OperationsTreatmentAdminCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function OperationsTreatmentAdminCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {

        /**
         * Table Treatements list
         */
        API.procedure.get('ProcGetTreatmentsList')
            .then(function(result) {
                $scope.TreatmentsList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddTreatment = function(Treatment) {
            if (Treatment === undefined | Treatment === '') {
                Notification.show('warning', 'Please enter treatment');
                return false;
            }

            API.procedure.post('ProcAddNewTreatment', {
                    Description: Treatment
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.DeteleTreatment = function(TreatmentsList) {
            var selectedTrements = _.filter(TreatmentsList, _.matches({ 'selected': true }));
            if (selectedTrements.length > 0) {
                selectedTrements.forEach(function(value) {
                    API.procedure.post('ProcDeleteTreatment', {
                            UserID: $rootScope.user.getId(),
                            TreatmentID: value.TreatmentID
                        })
                        .then(function(result) {
                            _.remove(TreatmentsList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                });
            } else {
                Notification.show('warning', 'Please select treatment');
                return false;
            }
        };

        /**
         * Table Operations list
         */
        API.procedure.get('ProcGetMachiningTypeList')
            .then(function(result) {
                $scope.MachiningTypeList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddOperation = function(Operation) {
            if (Operation === undefined | Operation === '') {
                Notification.show('warning', 'Please enter operation');
                return false;
            }

            API.procedure.post('ProcAddNewMachiningType', {
                    Description: Operation
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.DeleteOperation = function(MachiningTypeList) {
            var selectedOperation = _.filter(MachiningTypeList, _.matches({ 'selected': true }));
            if (selectedOperation.length > 0) {
                selectedOperation.forEach(function(value) {
                    API.procedure.post('ProcDeleteMachiningType', {
                            UserID: $rootScope.user.getId(),
                            MachiningTypeID: value.MachiningTypeID
                        })
                        .then(function(result) {
                            _.remove(MachiningTypeList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                });
            } else {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
        };

        /**
         * Table Sundries list
         */
        API.procedure.get('ProcGetSundriesList')
            .then(function(result) {
                $scope.SundriesList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddSundry = function(Sundry) {
            if (Sundry === undefined | Sundry === '') {
                Notification.show('warning', 'Please enter sundry');
                return false;
            }
            API.procedure.post('ProcAddNewSundry', {
                    Description: Sundry
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.DeleteSundries = function(SundriesList) {
            var selectedSundries = _.filter(SundriesList, _.matches({ 'selected': true }));
            if (selectedSundries.length > 0) {
                selectedSundries.forEach(function(value) {
                    API.procedure.post('ProcDeleteSundry', {
                            UserID: $rootScope.user.getId(),
                            SundryID: value.SundryID
                        })
                        .then(function(result) {
                            _.remove(SundriesList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                });
            } else {
                Notification.show('warning', 'Please select Sundry');
                return false;
            }
        };

        /**
         * Table Condition of Supply
         */
        API.procedure.get('ProcGetSupplyConditionList')
            .then(function(result) {
                $scope.SupplyConditionList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddCondition = function(Condition) {
            if (Condition === undefined | Condition === '') {
                Notification.show('warning', 'Please enter condition');
                return false;
            }
            API.procedure.post('ProcAddNewSupplyCondition', {
                    Description: Condition
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * Table Scheduling contingencies (Fat)
         */

        API.procedure.get('ProcGetFat')
            .then(function(result) {
                $scope.GetFat = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.UpdateFat = function(Fat) {
            API.procedure.post('ProcUpdateFat', {
                    UserID: $rootScope.user.getId(),
                    FATTypeID: Fat.FatTypeID,
                    FATDays: Fat.DelDays
                })
                .then(function(result) {})
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * Table Ope. pause reasons
         */
        API.procedure.get('ProcGetPauseReasons')
            .then(function(result) {
                $scope.PauseReasons = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddReason = function(Reason) {
            if (Reason === undefined | Reason === '') {
                Notification.show('warning', 'Please enter condition');
                return false;
            }
            API.procedure.post('ProcAddNewPauseReason', {
                    Reason: Reason
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.operations-treatment-admin', []));
