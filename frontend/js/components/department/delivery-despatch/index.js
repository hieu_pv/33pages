(function(app) {

    app.controller('DeliveryDespatchCtrl', DeliveryDespatchCtrl);
    DeliveryDespatchCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function DeliveryDespatchCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {

        /**
         * get Carriage drop-down
         */
        $scope.CarriageList = [{
            name: 'Next day',
            CarriageTypeID: 1
        }, {
            name: '9am',
            CarriageTypeID: 2
        }, {
            name: '12noon',
            CarriageTypeID: 3
        }, {
            name: '5.30pm',
            CarriageTypeID: 4
        }];

        /**
         * get Courier drop-down
         */
        API.procedure.get('ProcGetCourierList')
            .then(function(result) {
                $scope.CourierList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get complete WO table
         */
        API.procedure.get('ProcGetDispatchList')
            .then(function(result) {
                $scope.DispatchList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Treatment table
         */
        API.procedure.get('ProcGetTreatmentDispatchList')
            .then(function(result) {
                $scope.TreatmentDispatchList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.runDispatchDetail = function(WorkOrderID, POnID, GRBNo) {
            API.procedure.post('ProcGetDispatchDetail', {
                    WorkOrderID: WorkOrderID,
                    POnID: POnID,
                    GRBNo: GRBNo
                })
                .then(function(result) {
                    $scope.DispatchDetail = result[0];

                    // Selected Carriage type drop-down
                    $scope.Carriage = _.find($scope.CarriageList, function(value) {
                        return parseInt(value.CarriageTypeID) === parseInt($scope.DispatchDetail.CarriageTypeID);
                    });

                    // Selected Courier drop-down
                    $scope.Courier = _.find($scope.CourierList, function(value) {
                        return parseInt(value.CourierID) === parseInt($scope.DispatchDetail.CourierID);
                    });
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.GetDispatchDetail = function(DispatchList, TreatmentDispatchList) {
            var selectedTreatment, selectedDispatch;
            var WorkOrderID, POnID;
            if (DispatchList !== undefined) {
                POnID = null;
                selectedTreatment = _.find($scope.TreatmentDispatchList, {
                    'selected': true
                });
                if (selectedTreatment !== undefined) {
                    delete selectedTreatment.selected;
                }
                selectedDispatch = _.find($scope.DispatchList, {
                    'selected': true
                });
                if (selectedDispatch !== undefined) {
                    $scope.runDispatchDetail(selectedDispatch.WorkOrderID, POnID, selectedDispatch.GRBNo);
                }
            }
            if (TreatmentDispatchList !== undefined) {
                WorkOrderID = null;
                selectedDispatch = _.find($scope.DispatchList, {
                    'selected': true
                });
                if (selectedDispatch !== undefined) {
                    delete selectedDispatch.selected;
                }
                selectedTreatment = _.find($scope.TreatmentDispatchList, {
                    'selected': true
                });
                if (selectedTreatment !== undefined) {
                    $scope.runDispatchDetail(WorkOrderID, selectedTreatment.POnID, selectedTreatment.GRBNo);
                }
            }
        };

        $scope.checkSelectDispatchOrTreatment = function() {
            var selectedDispatch = _.find($scope.DispatchList, {
                'selected': true
            });
            var selectedTreatment = _.find($scope.TreatmentDispatchList, {
                'selected': true
            });
            if (selectedDispatch === undefined && selectedTreatment === undefined) {
                Notification.show('warning', 'Please select Completed Work Orders or Parts for Treatment table');
                return false;
            }
            if (selectedDispatch !== undefined) {
                return selectedDispatch;
            }
            if (selectedTreatment !== undefined) {
                return selectedTreatment;
            }
        };

        $scope.checkSelectDispatch = function() {
            var selectedDispatch = _.find($scope.DispatchList, {
                'selected': true
            });
            if (_.isUndefined(selectedDispatch)) {
                Notification.show('warning', 'Please select Completed Work Orders table');
                return false;
            }
            if (!_.isUndefined(selectedDispatch)) {
                return selectedDispatch;
            }
        };

        $scope.checkSelectTreatment = function() {
            var selectedTreatment = _.find($scope.TreatmentDispatchList, {
                'selected': true
            });
            if (_.isUndefined(selectedTreatment)) {
                Notification.show('warning', 'Please select Parts for Treatment table');
                return false;
            }
            if (!_.isUndefined(selectedTreatment)) {
                return selectedTreatment;
            }
        };

        /**
         * @param {[integer]} Courier
         */
        $scope.CourierBooked = function(Carriage, Courier) {
            var Operation = $scope.checkSelectDispatchOrTreatment();
            if (Operation) {
                if (_.isNil(Carriage)) {
                    Notification.show('warning', 'Please select Carriage type');
                    return false;
                }
                if (_.isNil(Courier)) {
                    Notification.show('warning', 'Please select Courier');
                    return false;
                }
                var InvoiceID;
                if (_.isNil(Operation.pkInvoiceID)) {
                    InvoiceID = null;
                } else {
                    InvoiceID = parseInt(Operation.pkInvoiceID);
                }
                var paramsPackingSlip = {};
                if (!_.isNil(Operation.GRBNo)) {
                    paramsPackingSlip.GRBNo = Operation.GRBNo;
                }

                API.procedure.post('ProcCourierBooked', {
                        UserID: $rootScope.user.getId(),
                        OperationID: Operation.OperationID,
                        CarriageTypeID: Carriage.CarriageTypeID,
                        CourierID: Courier.CourierID,
                        InvoiceID: InvoiceID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Booked success');
                        $rootScope.openTabBirt('PackingSlip', paramsPackingSlip);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.Dispatched = function() {
            var Operation = $scope.checkSelectDispatchOrTreatment();
            if (Operation) {
                var InvoiceID;
                if (_.isNil(Operation.pkInvoiceID)) {
                    InvoiceID = null;
                } else {
                    InvoiceID = parseInt(Operation.pkInvoiceID);
                }

                API.procedure.post('ProcDispatched', {
                        UserID: $rootScope.user.getId(),
                        OperationID: Operation.OperationID,
                        InvoiceID: InvoiceID
                    })
                    .then(function(result) {
                        if (result.length > 0 && parseInt(result[0].Result) === -1) {
                            Notification.show('warning', result[0].MsgResult);
                        } else {
                            Notification.show('success', 'Despatched success');
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.printDocs = function() {
            var selectedDispatch = _.find($scope.DispatchList, {
                'selected': true
            });
            var selectedTreatment = _.find($scope.TreatmentDispatchList, {
                'selected': true
            });
            if (_.isNil(selectedDispatch) && _.isNil(selectedTreatment)) {
                Notification.show('warning', 'Please select Completed Work Orders or Parts for Treatment table');
                return false;
            }
            var params = {};
            var paramsPackingSlip = {};
            var paramsCOC = {};
            if (!_.isNil(selectedDispatch)) {
                if (!_.isNil(selectedDispatch.WorkOrderID)) {
                    params.pkWorkOrderID = selectedDispatch.WorkOrderID;
                }
                if (!_.isNil(selectedDispatch.GRBNo)) {
                    params.fkGRBID = selectedDispatch.GRBNo;
                    paramsPackingSlip.GRBNo = selectedDispatch.GRBNo;
                    $rootScope.openTabBirt('PackingSlip', paramsPackingSlip);
                }
                if (!_.isNil(selectedDispatch.pkInvoiceID)) {
                    params.InvoiceNo = selectedDispatch.pkInvoiceID;
                    paramsCOC.InvoiceID = selectedDispatch.pkInvoiceID;
                }
            }
            if (!_.isNil(selectedTreatment)) {
                if (_.isNil(paramsCOC.InvoiceID)) {
                    paramsCOC.InvoiceID = 0;
                }
                if (!_.isNil(selectedTreatment.POnID)) {
                    params.pkPOnID = selectedTreatment.POnID;
                    paramsCOC.POnID = selectedTreatment.POnID;
                } else {
                    paramsCOC.POnID = 0;
                }
            }
            $rootScope.openTabBirt('DeliveryNote', params);
            if (_.isNil(paramsCOC.POnID)) {
                paramsCOC.POnID = 0;
            }
            console.log('paramsCOC ', paramsCOC);
            $rootScope.openTabBirt('CoC_v6', paramsCOC);
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.delivery-despatch', []));
