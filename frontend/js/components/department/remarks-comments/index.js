(function(app) {

    app.controller('RemarkCommentCtrl', RemarkCommentCtrl);
    RemarkCommentCtrl.$inject = ['$rootScope', '$scope', '$state', 'Notification', 'API', '$window'];

    function RemarkCommentCtrl($rootScope, $scope, $state, Notification, API, $window) {
        API.procedure.get('ProcGetRemarks')
            .then(function(result) {
                $scope.Remarks = result;
            })
            .catch(function(error) {
                throw error;
            });

        API.procedure.get('ProcGetComments')
            .then(function(result) {
                $scope.Comments = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.AddRemark = function(Remark) {
            if (Remark === undefined || Remark === '') {
                Notification.show('warning', 'Please enter Remark');
                return false;
            }
            API.procedure.post('ProcAddRemark', {
                    UserID: $rootScope.user.getId(),
                    Remark: Remark
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.RemoveRemark = function(Remarks) {
            if (Remarks.length > 0) {
                var selectedRemarks = _.filter(Remarks, _.matches({ 'selected': true }));
                if (selectedRemarks.length > 0) {
                    selectedRemarks.forEach(function(value) {
                        API.procedure.post('ProcRemoveRemark', {
                                RemarkID: value.RemarkID
                            })
                            .then(function(result) {
                                _.remove(Remarks, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                }
            }
        };

        $scope.AddComment = function(Comment) {
            if (Comment === undefined || Comment === '') {
                Notification.show('warning', 'Please enter Comment');
                return false;
            }
            API.procedure.post('ProcAddComment', {
                    UserID: $rootScope.user.getId(),
                    Comment: Comment
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.RemoveComment = function(Comments) {
            if (Comments.length > 0) {
                var selectedComments = _.filter(Comments, _.matches({ 'selected': true }));
                if (selectedComments.length > 0) {
                    selectedComments.forEach(function(value) {
                        API.procedure.post('ProcRemoveComment', {
                                CommentID: value.CommentID
                            })
                            .then(function(result) {
                                _.remove(Comments, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                }
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.remarks-comments', []));
