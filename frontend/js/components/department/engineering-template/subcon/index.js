(function(app) {

    app.controller('EngineeringSubconCtrl', EngineeringSubconCtrl);
    EngineeringSubconCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringSubconCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;
        var numberRegex = /^\d+$/;

        if (!_.isNil(OperationID) && OperationID !== '') {
            hasOperationID = true;

            API.procedure.post('ProcGetWOOpTmpltSubconDetail', {
                    OperationID: OperationID
                })
                .then(function(result) {
                    $scope.OpTmpltSubconDetail = result[0];
                    if (!_.isNil($scope.OpTmpltSubconDetail) && !_.isNil($scope.OpTmpltSubconDetail.SupplierName)) {
                        $scope.subconSearchInputModel = $scope.OpTmpltSubconDetail.SupplierName;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }
        if (!_.isNil(OperationTemplateID) && OperationTemplateID !== '') {
            hasOperationID = true;
            API.procedure.post('ProcGetOpTmpltSubconDetail', {
                    OperationTemplateID: OperationTemplateID
                })
                .then(function(result) {
                    $scope.OpTmpltSubconDetail = result[0];
                    if (!_.isNil($scope.OpTmpltSubconDetail) && !_.isNil($scope.OpTmpltSubconDetail.SupplierName)) {
                        $scope.subconSearchInputModel = $scope.OpTmpltSubconDetail.SupplierName;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * the function when user select Supplier
         */
        $scope.SupplierSelected = function(item) {
            $scope.selectedSupplier = item;
        };

        $scope.PurchaseReview = function() {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcPurchToReviewSC', {
                            UserID: $rootScope.user.getId(),
                            PartTemplateID: $scope.TemplateDetail.PartTemplateID,
                            ETemplateID: EngineeringTemplateID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Purchasing Review Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcWOPurchToReviewSC', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Purchasing Review Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        /**
         * the function working when user typing approval comment
         */
        API.procedure.post('ProcSCApprovalComments', {
                CommentSearch: ''
            })
            .then(function(result) {
                $scope.SCApprovalComments = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.addSCApprovalComment = function(Comment) {
            if (_.isNil($rootScope.SCApprovalCommentItems)) {
                $rootScope.SCApprovalCommentItems = [];
            }
            var data;
            if (!_.isNil(Comment) && Comment !== "") {
                data = {
                    Comment: Comment
                };
                $rootScope.SCApprovalCommentItems.push(data);
                $scope.ApprovalComment = undefined;

                if (!_.isNil(OperationID) && OperationID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationID, 'subcon');
                }
                if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationTemplateID, 'subcon');
                }
            }
        };

        if (_.isNil($scope.SearchPOComments)) {
            // parent scope
            $scope.getSearchPOComments();
        }

        $scope.$on('clearPOComment', function() {
            $scope.POComment = undefined;
        });

        /**
         * the function working when user press enter key input of Sub-con Notes table
         */
        $scope.AddTreatNote = function(Note) {
            if (_.isNil($rootScope.OpTmpltIntTreatNotes)) {
                $rootScope.OpTmpltIntTreatNotes = [];
            }
            var data;
            if (!_.isNil(Note) && Note !== "") {
                data = {
                    Note: Note
                };
            }
            $rootScope.OpTmpltIntTreatNotes.push(data);
            $scope.TreatNote = undefined;
            if (!_.isNil(OperationID) && OperationID !== "") {
                $rootScope.addRootTreatNote(data, OperationID);
            }
            if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                $rootScope.addRootTreatNote(data, OperationTemplateID);
            }
        };

        /**
         * the function working when user click Update button
         */
        $scope.UpdateSubconDetail = function(OpTmpltSubconDetail) {
            if (OpTmpltSubconDetail !== undefined) {
                if (!hasOperationID) {
                    Notification.show('warning', 'Please select Operation');
                    return false;
                }
                if (OpTmpltSubconDetail === undefined || OpTmpltSubconDetail.LeadTime === undefined || OpTmpltSubconDetail.LeadTime === '' || OpTmpltSubconDetail.LeadTime === null) {
                    OpTmpltSubconDetail.LeadTime = 0;
                }
                if (!numberRegex.test(OpTmpltSubconDetail.LeadTime)) {
                    Notification.show('warning', 'Std. lead time must be number');
                    return false;
                }
                if ($scope.selectedSupplier === undefined) {
                    $scope.selectedSupplier = {
                        SupplierID: null
                    };
                }
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcUpdateOpTmpltSubconDetail', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationTemplateID,
                            SupplierID: $scope.selectedSupplier.SupplierID,
                            LeadTime: OpTmpltSubconDetail.LeadTime
                        })
                        .then(function(result) {
                            Notification.show('success', 'Update Operation Template Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcUpdateWOOpTmpltSubconDetail', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            SupplierID: $scope.selectedSupplier.SupplierID,
                            LeadTime: OpTmpltSubconDetail.LeadTime
                        })
                        .then(function(result) {
                            Notification.show('success', 'Update Operation Template Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $scope.sendMail = function(mailData, typeOperation, OperationID) {
            if (mailData.StdSupplierEmail === '') {
                Notification.show('warning', 'StdSupplierEmail is null. Please add StdSupplierEmail');
                return false;
            }

            if (typeOperation === 'OperationTemplateID') {
                API.procedure.post('ProcGetOpTmpltSubconDetail', {
                        OperationTemplateID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpTmpltSubconDetail = result[0];
                        $scope.hasOpTmpltSubconDetail = true;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
            if (typeOperation === 'OperationID') {
                API.procedure.post('ProcGetWOOpTmpltSubconDetail', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpTmpltSubconDetail = result[0];
                        $scope.hasOpTmpltSubconDetail = true;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
            $scope.$watch('hasOpTmpltSubconDetail', function(hasOpTmpltSubconDetail) {
                var mailContent = "Please provide a quotation for the following:\n\n";
                if (!_.isNil(hasOpTmpltSubconDetail)) {
                    if (!_.isNil($scope.OpTmpltSubconDetail) && $scope.OpTmpltSubconDetail.Treatment !== '') {
                        mailContent += $scope.OpTmpltSubconDetail.Treatment + " of ";
                    }

                    if (mailData.PartLength !== "") {
                        mailContent += mailData.PartLength + "mm ";
                    }

                    mailContent += mailData.Description;
                    if (mailData.Size !== "") {
                        mailContent += " @ " + mailData.Size;
                    }
                    var Unit = _.find($scope.UnitList, function(item) {
                        return parseInt(item.value) === parseInt(mailData.UnitID);
                    });
                    if (!_.isNil(Unit)) {
                        mailContent += Unit.name;
                    }

                    var Shape = _.find($scope.MaterialShapes, function(item) {
                        return parseInt(item.pkShapeID) === parseInt(mailData.ShapeID);
                    });
                    if (!_.isNil(Shape)) {
                        mailContent += " " + Shape.Description;
                    }

                    var SPU = _.find($scope.UOMList, function(item) {
                        return parseInt(item.pkUOMID) === parseInt(mailData.SPUID);
                    });
                    if (!_.isNil(SPU)) {
                        mailContent += " " + SPU.Description;
                    }

                    var Approval = _.find($scope.MaterialLists, function(item) {
                        return parseInt(item.ApprovalID) === parseInt(mailData.ApprovalID);
                    });
                    if (!_.isNil(Approval)) {
                        mailContent += ", to " + Approval.Description + " Approval";
                    }
                    var mailToLink = "mailto:" + mailData.StdSupplierEmail + "?body=" + encodeURIComponent(mailContent);
                    $window.location.href = mailToLink;
                }
            });
        };

        $scope.mailTo = function() {
            if (!_.isNil($scope.ListTemplateOps)) {
                var selectedOperation = _.find($scope.ListTemplateOps, {
                    'selected': true
                });
                if (_.isNil(selectedOperation)) {
                    Notification.show('warning', 'Please select Operation');
                    return false;
                } else {
                    if (parseInt(selectedOperation.OpTypeID) !== 3) {
                        Notification.show('warning', 'Operation must be Sub-con type');
                        return false;
                    }
                }
                // get first material of ListTemplateOps
                var Operation = _.find($scope.ListTemplateOps, function(item) {
                    return parseInt(item.OpTypeID) === 1;
                });
                if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                    API.procedure.post('ProcGetTemplateMaterial', {
                            OperationTemplateID: Operation.OpTemplateID
                        })
                        .then(function(response) {
                            $scope.mailData = response[0];
                            $scope.sendMail($scope.mailData, 'OperationTemplateID', selectedOperation.OpTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    API.procedure.post('ProcGetWOTemplateMaterial', {
                            OperationID: Operation.OpTemplateID
                        })
                        .then(function(response) {
                            $scope.mailData = response[0];
                            $scope.sendMail($scope.mailData, 'OperationID', Operation.OpTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };
    }

})(angular.module('app.components.department.engineering-template.subcon', []));
