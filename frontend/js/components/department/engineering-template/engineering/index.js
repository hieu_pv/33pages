(function(app) {

    app.controller('EngineeringEngineeringCtrl', EngineeringEngineeringCtrl);
    EngineeringEngineeringCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringEngineeringCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        $scope.DeptID = $stateParams.DeptID;
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;

        if (!_.isNil(OperationID) && OperationID !== '') {
            hasOperationID = true;
        }
        if (!_.isNil(OperationTemplateID) && OperationTemplateID !== '') {
            hasOperationID = true;
        }

        $scope.AddTemplateProcess = function(Note) {
            if (_.isNil($rootScope.TemplateProcesses)) {
                $rootScope.TemplateProcesses = [];
            }
            var data;
            if (!_.isNil(Note) && Note !== "") {
                data = {
                    ProcessNote: Note
                };
                $rootScope.TemplateProcesses.push(data);
                $scope.ProcessNote = undefined;

                if (!_.isNil(OperationID) && OperationID !== "") {
                    $rootScope.addRootTemplateProcess(data, OperationID);
                }
                if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                    $rootScope.addRootTemplateProcess(data, OperationTemplateID);
                }
            }
        };

        $scope.AddOpTmpltIntEngNotes = function(Note) {
            if (_.isNil($rootScope.OpTmpltIntEngNotes)) {
                $rootScope.OpTmpltIntEngNotes = [];
            }
            var data;
            if (!_.isNil(Note) && Note !== "") {
                data = {
                    Note: Note
                };
            }
            $rootScope.OpTmpltIntEngNotes.push(data);
            $scope.EngNote = undefined;
            if (!_.isNil(OperationID) && OperationID !== "") {
                $rootScope.addRootEngNote(data, OperationID);
            }
            if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                $rootScope.addRootEngNote(data, OperationTemplateID);
            }
        };

        /**
         * the function working when click Engineering Review button 
         * in P/E Engineering Tab
         */
        $scope.EngineeringToReview = function() {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcEngineeringToReview', {
                            UserID: $rootScope.user.getId(),
                            ETemplateID: EngineeringTemplateID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Engineering Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcWOEngineeringToReview', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Engineering Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $scope.$on('AddTmpltProgram', function(event, args) {
            $scope.OpProgramNumber = '';
        });

        // $scope.ViewADD = function() {
        //     API.procedure.post('ProcViewAllTmpltADD', {
        //             PartTemplateID: $scope.TemplateDetail.PartTemplateID
        //         })
        //         .then(function(result) {
        //             if (result.length > 0) {
        //                 var fileName = result[0].FileName;
        //                 var file = $rootScope.PathImageUpload + fileName;
        //                 $window.open(file, 'blank_');
        //             }
        //         })
        //         .catch(function(error) {
        //             throw error;
        //         });
        // };
    }

})(angular.module('app.components.department.engineering-template.engineering', []));
