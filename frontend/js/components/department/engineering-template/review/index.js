(function(app) {

    app.controller('EngineeringReviewCtrl', EngineeringReviewCtrl);
    EngineeringReviewCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams'];

    function EngineeringReviewCtrl($rootScope, $scope, API, Notification, $state, $stateParams) {
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;

        if (!_.isNil(OperationID) && OperationID !== '') {
            hasOperationID = true;
        }
        if (!_.isNil(OperationTemplateID) && OperationTemplateID !== '') {
            hasOperationID = true;
        }

        $scope.listReview = [{
            name: 'Failed',
            value: 1,
            bgclass: 'bg-red',
            colorclass: 'col-grey'
        }, {
            name: 'in Process',
            value: 2,
            bgclass: 'bg-yellow',
            colorclass: 'col-grey'
        }, {
            name: 'Passed',
            value: 3,
            bgclass: 'bg-green',
            colorclass: 'col-grey'
        }];

        $scope.listRisk = [{
            name: 'High',
            value: 1,
            bgclass: 'bg-red',
            colorclass: 'col-light-yellow'
        }, {
            name: 'Medium',
            value: 2,
            bgclass: 'bg-orange',
            colorclass: 'col-light-yellow'
        }, {
            name: 'Low',
            value: 3,
            bgclass: 'bg-green',
            colorclass: 'col-grey'
        }];

        $scope.getDataReviewTab = function(type) {
            $scope.$watch('ListTemplateOps', function(ListTemplateOps) {
                if (ListTemplateOps !== undefined && ListTemplateOps.length > 0) {
                    $scope.minTemplateOps = _.minBy(ListTemplateOps, 'OpTemplateID');

                    if (type === 'EngineeringTemplateID') {

                        // get Overview info of Tab
                        API.procedure.post('ProcGetOpTmpltReviewDetail', {
                                OperationTemplateID: $scope.minTemplateOps.OpTemplateID
                            })
                            .then(function(result) {
                                $scope.OpTmpltReviewDetail = result[0];
                                configReviewDetail($scope.OpTmpltReviewDetail);
                            })
                            .catch(function(error) {
                                throw error;
                            });

                        // get Internal QA Notes table
                        API.procedure.post('ProcGetOpTmpltIntReviewNotes', {
                                OperationTemplateID: $scope.minTemplateOps.OpTemplateID
                            })
                            .then(function(result) {
                                $scope.OpTmpltIntReviewNotes = result;
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }

                    if (type === 'WorkOrderID') {
                        API.procedure.post('ProcGetWOOpTmpltReviewDetail', {
                                OperationID: $scope.minTemplateOps.OpTemplateID
                            })
                            .then(function(result) {
                                $scope.OpTmpltReviewDetail = result[0];
                                configReviewDetail($scope.OpTmpltReviewDetail);
                            })
                            .catch(function(error) {
                                throw error;
                            });

                        API.procedure.post('ProcGetWOOpTmpltIntReviewNotes', {
                                OperationID: $scope.minTemplateOps.OpTemplateID
                            })
                            .then(function(result) {
                                $scope.OpTmpltIntReviewNotes = result;
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            });
        };

        if (EngineeringTemplateID !== undefined) {
            $scope.getDataReviewTab('EngineeringTemplateID');
        }
        if (WorkOrderID !== undefined) {
            $scope.getDataReviewTab('WorkOrderID');
        }

        function configReviewDetail(ReviewDetail) {
            $scope.OpTmpltReviewDetail = ReviewDetail;

            /**
             * Remove selected exist
             */
            var selectedReview = _.find($scope.listReview, { 'selected': true });
            if (selectedReview !== undefined) {
                delete selectedReview.selected;
            }
            var selectedRisk = _.find($scope.listRisk, { 'selected': true });
            if (selectedRisk !== undefined) {
                delete selectedRisk.selected;
            }

            $scope.listReview.forEach(function(value, key) {
                if (value.value === parseInt(ReviewDetail.ReviewStatus)) {
                    $scope.listReview[key].selected = true;
                }
            });

            $scope.listRisk.forEach(function(value, key) {
                if (value.value === parseInt(ReviewDetail.Risk)) {
                    $scope.listRisk[key].selected = true;
                }
            });
        }

        $scope.UpdateOpReview = function(OpTmpltReviewDetail) {
            if (EngineeringTemplateID !== undefined) {
                API.procedure.post('ProcUpdateOpTmpltReviewDetail', {
                        UserID: $rootScope.user.getId(),
                        OperationTemplateID: $scope.minTemplateOps.OpTemplateID,
                        ReviewStatus: parseInt(OpTmpltReviewDetail.ReviewStatus),
                        Risk: parseInt(OpTmpltReviewDetail.Risk),
                        Tooling: parseInt(OpTmpltReviewDetail.Tooling),
                        QAEquip: parseInt(OpTmpltReviewDetail.QAEquip),
                        CorrectApproval: parseInt(OpTmpltReviewDetail.CorrectApproval)
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            }

            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcUpdateWOOpTmpltReviewDetail', {
                        UserID: $rootScope.user.getId(),
                        OperationID: $scope.minTemplateOps.OpTemplateID,
                        ReviewStatus: parseInt(OpTmpltReviewDetail.ReviewStatus),
                        Risk: parseInt(OpTmpltReviewDetail.Risk),
                        Tooling: parseInt(OpTmpltReviewDetail.Tooling),
                        QAEquip: parseInt(OpTmpltReviewDetail.QAEquip),
                        CorrectApproval: parseInt(OpTmpltReviewDetail.CorrectApproval)
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.setReview = function(Review) {
            $scope.OpTmpltReviewDetail.ReviewStatus = Review;
            $scope.UpdateOpReview($scope.OpTmpltReviewDetail);
        };

        $scope.setRisk = function(Risk) {
            $scope.OpTmpltReviewDetail.Risk = Risk;
            $scope.UpdateOpReview($scope.OpTmpltReviewDetail);
        };

        /**
         * the function working when user enter input Note then press Enter key
         */
        $scope.AddReviewNote = function(ReviewNote) {
            if (ReviewNote === undefined | ReviewNote === '') {
                Notification.show('warning', 'Please enter QA Note');
                return false;
            }

            if (EngineeringTemplateID !== undefined) {
                API.procedure.post('ProcAddOpTmpltIntReviewNote', {
                        UserID: $rootScope.user.getId(),
                        OperationTemplateID: $scope.minTemplateOps.OpTemplateID,
                        Note: ReviewNote
                    })
                    .then(function(result) {
                        var newNote = {
                            Note: ReviewNote
                        };
                        $scope.OpTmpltIntReviewNotes.push(newNote);
                        $scope.ReviewNote = undefined;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }

            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcAddWOOpTmpltIntReviewNote', {
                        UserID: $rootScope.user.getId(),
                        OperationID: $scope.minTemplateOps.OpTemplateID,
                        Note: ReviewNote
                    })
                    .then(function(result) {
                        var newNote = {
                            Note: ReviewNote
                        };
                        $scope.OpTmpltIntReviewNotes.push(newNote);
                        $scope.ReviewNote = undefined;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.QualityToReview = function() {
            if (EngineeringTemplateID !== undefined) {
                API.procedure.post('ProcQualityToReview', {
                        UserID: $rootScope.user.getId(),
                        ETemplateID: EngineeringTemplateID,
                        PartNumber: $scope.TemplateDetail.PartNumber
                    })
                    .then(function(result) {
                        Notification.show('success', 'QA Review success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }

            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcWOQualityToReview', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID,
                        PartNumber: $scope.TemplateDetail.PartNumber
                    })
                    .then(function(result) {
                        Notification.show('success', 'QA Review success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };
    }

})(angular.module('app.components.department.engineering-template.review', []));
