(function(app) {

    app.controller('EngineeringMaterialCtrl', EngineeringMaterialCtrl);
    EngineeringMaterialCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringMaterialCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        var EngineeringTemplateID = $scope.EngineeringTemplateID;
        var WorkOrderID = $scope.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;

        if (!_.isNil(OperationID) && OperationID !== '') {
            hasOperationID = true;
        }
        if (!_.isNil(OperationTemplateID) && OperationTemplateID !== '') {
            hasOperationID = true;
        }

        // Remove to can not add 2 times
        if (!hasOperationID) {
            $rootScope.ApprovalComments = undefined;
            $rootScope.POComments = undefined;
            $rootScope.ConformityNotes = undefined;
            $rootScope.MaterialNotes = undefined;

            $rootScope.SCApprovalCommentItems = undefined;
            $rootScope.OpTmpltIntTreatNotes = undefined;
            $rootScope.OpTmpltIntEngNotes = undefined;
            $rootScope.TemplateProcesses = undefined;
        }


        $scope.$watch('TemplateMaterialInfo', function(TemplateMaterialInfo) {

            // Selected Units drop-down
            if (!_.isNil(TemplateMaterialInfo) && !_.isNil(TemplateMaterialInfo.UnitID)) {
                $scope.Unit = _.find($scope.UnitList, function(item) {
                    return parseInt(item.value) === parseInt(TemplateMaterialInfo.UnitID);
                });
            }

            // Selected Shape drop-down
            if (!_.isNil(TemplateMaterialInfo) && !_.isNil(TemplateMaterialInfo.ShapeID)) {
                $scope.Shape = _.find($scope.MaterialShapes, function(item) {
                    return parseInt(item.pkShapeID) === parseInt(TemplateMaterialInfo.ShapeID);
                });
            }

            // Selected Approval drop-down
            if (!_.isNil(TemplateMaterialInfo) && !_.isNil(TemplateMaterialInfo.ApprovalID)) {
                $scope.Approval = _.find($scope.MaterialLists, function(item) {
                    return parseInt(item.ApprovalID) === parseInt(TemplateMaterialInfo.ApprovalID);
                });
            }

            // Selected SPU drop-down
            if (!_.isNil(TemplateMaterialInfo) && !_.isNil(TemplateMaterialInfo.SPUID)) {
                $scope.SPU = _.find($scope.UOMList, function(item) {
                    return parseInt(item.pkUOMID) === parseInt(TemplateMaterialInfo.SPUID);
                });
            }

            // Selected Supplier drop-down
            if (!_.isNil(TemplateMaterialInfo) && !_.isNil(TemplateMaterialInfo.StdSupplierID)) {
                $scope.Supplier = _.find($scope.StdSuppliers, function(item) {
                    return parseInt(item.StdSupplierID) === parseInt(TemplateMaterialInfo.StdSupplierID);
                });
            }
        });

        API.procedure.post('ProcSearchApprovalComments', {
                CommentSearch: ''
            })
            .then(function(result) {
                $scope.SearchApprovalComments = result;
            })
            .catch(function(error) {
                throw error;
            });

        if (_.isNil($scope.SearchPOComments)) {
            // parent scope
            $scope.getSearchPOComments();
        }

        $scope.$on('clearPOComment', function() {
            $scope.POComment = undefined;
        });

        $scope.addApprovalComment = function(Comment) {
            if (_.isNil($rootScope.ApprovalComments)) {
                $rootScope.ApprovalComments = [];
            }
            var data;
            if (!_.isNil(Comment) && Comment !== "") {
                data = {
                    Comment: Comment
                };
                $rootScope.ApprovalComments.push(data);
                $scope.ApprovalComment = undefined;

                if (!_.isNil(OperationID) && OperationID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationID, 'material');
                }
                if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationTemplateID, 'material');
                }
            }
        };

        $scope.MaterialSpecSelected = function(MaterialSpec) {
            $scope.MaterialSpec = MaterialSpec;
        };

        $scope.addTemplateMaterial = function(TemplateMaterial) {
            if (hasOperationID !== true) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }

            if (_.isNil(TemplateMaterial.Spec)) {
                TemplateMaterial.Spec = '';
            }
            API.procedure.post('ProcAddMaterialSpec', {
                    UserID: $rootScope.user.getId(),
                    MaterialSpec: TemplateMaterial.Spec
                })
                .then(function(response) {
                    if (response[0].Result.toLowerCase().indexOf("error") !== -1) {
                        Notification.show('warning', response[0].Result);
                    } else {
                        if (!_.isUndefined(EngineeringTemplateID)) {
                            API.procedure.post('ProcAddTemplateMaterial', {
                                    UserID: $rootScope.user.getId(),
                                    OperationTemplateID: OperationTemplateID,
                                    MaterialSpecID: response[0].NewSpecID,
                                    Description: TemplateMaterial.getDescription(),
                                    Size: TemplateMaterial.getSize(),
                                    ShapeID: TemplateMaterial.getShapeID(),
                                    PartLength: TemplateMaterial.getPartLength(),
                                    Yield: TemplateMaterial.getYield(),
                                    ApprovalID: TemplateMaterial.getApprovalID(),
                                    IssueInstruction: TemplateMaterial.getIssueInstruction(),
                                    TestPieces: TemplateMaterial.getTestPieces(),
                                    FreeIssue: TemplateMaterial.getFreeIssue(),
                                    UnitID: TemplateMaterial.getUnitID(),
                                    StdDelDays: TemplateMaterial.getStdDeliveryDays(),
                                    SPUID: TemplateMaterial.getSPUID(),
                                    StdSupplierID: TemplateMaterial.getStdSupplierID()
                                })
                                .then(function(result) {
                                    if (parseInt(result[0].Result) === -1) {
                                        Notification.show('warning', result[0].ResultMsg);
                                    } else {
                                        Notification.show('success', 'Add Template Material success');
                                    }
                                })
                                .catch(function(error) {
                                    throw error;
                                    // Notification.show('warning', 'Template Material added');
                                });
                        }

                        if (!_.isUndefined(WorkOrderID)) {
                            API.procedure.post('ProcAddWOTemplateMaterial', {
                                    UserID: $rootScope.user.getId(),
                                    OperationID: OperationID,
                                    MaterialSpecID: response[0].NewSpecID,
                                    Description: TemplateMaterial.getDescription(),
                                    Size: TemplateMaterial.getSize(),
                                    ShapeID: TemplateMaterial.getShapeID(),
                                    PartLength: TemplateMaterial.getPartLength(),
                                    Yield: TemplateMaterial.getYield(),
                                    ApprovalID: TemplateMaterial.getApprovalID(),
                                    IssueInstruction: TemplateMaterial.getIssueInstruction(),
                                    TestPieces: TemplateMaterial.getTestPieces(),
                                    FreeIssue: TemplateMaterial.getFreeIssue(),
                                    UnitID: TemplateMaterial.getUnitID(),
                                    StdDelDays: TemplateMaterial.getStdDeliveryDays(),
                                    SPUID: TemplateMaterial.getSPUID(),
                                    StdSupplierID: TemplateMaterial.getStdSupplierID()
                                })
                                .then(function(result) {
                                    Notification.show('success', 'Add Template Material success');
                                })
                                .catch(function(error) {
                                    throw error;
                                    // Notification.show('warning', 'Template Material added');
                                });
                        }
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.AddConformityNote = function(Note) {
            if (_.isNil($rootScope.ConformityNotes)) {
                $rootScope.ConformityNotes = [];
            }
            var data;
            if (!_.isNil(Note) && Note !== "") {
                data = {
                    Note: Note
                };
                $rootScope.ConformityNotes.push(data);
                $scope.ConformityNote = undefined;

                if (OperationID !== undefined && OperationID !== "") {
                    $rootScope.addRootConformityNote(data, OperationID);
                }
                if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                    $rootScope.addRootConformityNote(data, OperationTemplateID);
                }
            }
        };

        $scope.AddMaterialNote = function(Note) {
            if (_.isNil($rootScope.MaterialNotes)) {
                $rootScope.MaterialNotes = [];
            }
            var data;
            if (!_.isNil(Note) && Note !== "") {
                data = {
                    Note: Note
                };
            }
            $rootScope.MaterialNotes.push(data);
            $scope.MaterialNote = undefined;
            if (OperationID !== undefined && OperationID !== "") {
                $rootScope.addRootMaterialNote(data, OperationID);
            }
            if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                $rootScope.addRootMaterialNote(data, OperationTemplateID);
            }
        };

        $scope.mailTo = function(data) {
            var hasSpec;
            var hasSize;
            var hasUnit;
            var hasShape;
            var hasSPU;
            var hasApproval;
            if (!_.isNil(data.Spec) && data.Spec !== '') {
                hasSpec = true;
            }
            if (!_.isNil(data.Size) && data.Size !== '') {
                hasSize = true;
            }
            if (!_.isNil(data.UnitID)) {
                hasUnit = true;
            }
            if (!_.isNil(data.ShapeID)) {
                hasShape = true;
            }
            if (!_.isNil(data.SPUID)) {
                hasSPU = true;
            }
            if (!_.isNil(data.ApprovalID)) {
                hasApproval = true;
            }
            if (hasSpec && hasSize && hasUnit && hasShape && hasSPU && hasApproval) {
                var Unit = _.find($scope.UnitList, function(item) {
                    return parseInt(item.value) === parseInt(data.UnitID);
                });
                if (!_.isUndefined(Unit)) {
                    Unit = Unit.name;
                }

                var Shape = _.find($scope.MaterialShapes, function(item) {
                    return parseInt(item.pkShapeID) === parseInt(data.ShapeID);
                });
                if (!_.isUndefined(Shape)) {
                    Shape = Shape.Description;
                }

                var Approval = _.find($scope.MaterialLists, function(item) {
                    return parseInt(item.ApprovalID) === parseInt(data.ApprovalID);
                });
                if (!_.isUndefined(Approval)) {
                    Approval = Approval.Description;
                }

                var SPU = _.find($scope.UOMList, function(item) {
                    return parseInt(item.pkUOMID) === parseInt(data.SPUID);
                });
                if (!_.isUndefined(SPU)) {
                    SPU = SPU.Description;
                }
                if (!_.isNil(OperationTemplateID) && OperationTemplateID !== "") {
                    API.procedure.post('ProcGetTemplateMaterial', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            if (!_.isNil(result[0]) && result[0].StdSupplierEmail !== '') {
                                $scope.hasEmail = true;
                                $scope.email = result[0].StdSupplierEmail;
                            } else {
                                $scope.hasEmail = false;
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
                if (!_.isNil(OperationID) && OperationID !== "") {
                    API.procedure.post('ProcGetWOTemplateMaterial', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            if (!_.isNil(result[0]) && result[0].StdSupplierEmail !== '') {
                                $scope.hasEmail = true;
                                $scope.email = result[0].StdSupplierEmail;
                            } else {
                                $scope.hasEmail = false;
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
                $scope.$watch('hasEmail', function(hasEmail) {
                    if (!_.isNil(hasEmail)) {
                        if (!hasEmail) {
                            Notification.show('warning', 'StdSupplierEmail is null. Please add StdSupplierEmail');
                            return false;
                        } else {
                            var firstRow = "Please provide a quotation for the following:";
                            var secondRow = data.Spec + " @ " + data.Size + Unit + " " + Shape + " In " + SPU + ", to " + Approval + " Approval";
                            var thirdRow = "Thank you.";
                            var formattedBody = firstRow + "\n\n" + secondRow + "\n\n" + thirdRow;
                            var mailToLink = "mailto:" + $scope.email + "?body=" + encodeURIComponent(formattedBody);
                            $window.location.href = mailToLink;
                        }
                    }
                });
            } else {
                Notification.show('warning', 'Please select Spec, Size, Unit, Shape, SPU and Approval');
                return false;
            }
        };
    }

})(angular.module('app.components.department.engineering-template.material', []));