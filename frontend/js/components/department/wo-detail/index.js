(function(app) {

    app.controller('WoDetailCtrl', WoDetailCtrl);
    WoDetailCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', '$window', 'Notification'];

    function WoDetailCtrl($rootScope, $scope, API, $state, $stateParams, $window, Notification) {
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;

        $scope.getWODetail = getWODetail;
        $scope.SelectedOperation = SelectedOperation;
        $scope.getOpProcesses = getOpProcesses;
        $scope.getOpOperatorNotes = getOpOperatorNotes;
        $scope.getWOOpsDetail = getWOOpsDetail;
        $scope.getWOFONotes = getWOFONotes;
        $scope.getWODeliveries = getWODeliveries;
        $scope.SwapOp = SwapOp;
        $scope.ViewDrawing = ViewDrawing;
        $scope.UpdateOperation = UpdateOperation;
        $scope.ViewOriginTemplate = ViewOriginTemplate;
        $scope.RepeatOp = RepeatOp;
        $scope.redirectBack = redirectBack;

        /**
         * the function working when select a row of Operation table
         */
        function SelectedOperation(Operation) {
            var OperationID = Operation.OperationID;

            /**
             * get Processes table
             */
            getOpProcesses(OperationID);

            /**
             * get Operator Notes table
             */
            getOpOperatorNotes(OperationID);
        }

        if (!_.isNil(WorkOrderID) && WorkOrderID !== '') {

            /**
             * get data overview of page
             */
            getWODetail(WorkOrderID);

            /**
             * get Operation table
             */
            getWOOpsDetail(WorkOrderID);

            /**
             * get Front Office table
             */
            getWOFONotes(WorkOrderID);

            /**
             * get Delivery table
             */
            getWODeliveries(WorkOrderID);

        } else {
            $window.history.back();
        }

        function redirectBack() {
            $window.history.back();
        }

        function ViewOriginTemplate() {
            var state = 'department.engineering-template.material';
            var params = {
                WorkOrderID: WorkOrderID
            };
            $rootScope.redirectDept(state, params);
        }

        function UpdateOperation(Operation) {
            if (_.isNil(Operation.Bypass)) {
                Operation.Bypass = 0;
            }
            if (_.isNil(Operation.Subcon)) {
                Operation.Subcon = 0;
            }

            API.procedure.post('ProcUpdateOpDetail', {
                    UserID: $rootScope.user.getId(),
                    OperationID: Operation.OperationID,
                    Bypass: Operation.Bypass,
                    Subcon: Operation.Subcon
                })
                .then(function(result) {})
                .catch(function(error) {
                    throw error;
                });
        }

        function ViewDrawing() {
            var WOOpsDetailSelected = _.find($scope.WOOpsDetail, {
                'selected': true
            });
            if (_.isNil(WOOpsDetailSelected)) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
            API.procedure.post('ProcViewDrawing', {
                    OperationID: WOOpsDetailSelected.OperationID
                })
                .then(function(result) {
                    $scope.Drawing = result[0].FileName;
                    if (_.isNil($scope.Drawing) || $scope.Drawing === '') {
                        Notification.show('warning', "File doesn't exist");
                    } else {
                        var FileName = $rootScope.PathImageUpload + $scope.Drawing;
                        $window.open(FileName, 'blank_');
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * the function working when click Swap button
         */
        function SwapOp(WOOpsDetail) {
            var selectedSwap = _.filter(WOOpsDetail, _.matches({
                'Swap': 1
            }));
            if (selectedSwap.length === 2) {

                API.procedure.post('ProcSwapOps', {
                        UserID: $rootScope.user.getId(),
                        OperationID: selectedSwap[0].OperationID,
                        OperationID2: selectedSwap[1].OperationID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Swap Operation success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        }

        function getWODeliveries(WorkOrderID) {
            API.procedure.post('ProcGetWODeliveries', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WODeliveries = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getWOFONotes(WorkOrderID) {
            API.procedure.post('ProcGetWOFONotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOFONotes = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getWOOpsDetail(WorkOrderID) {
            API.procedure.post('ProcGetWOOpsDetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOOpsDetail = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getOpOperatorNotes(OperationID) {
            API.procedure.post('ProcGetOpOperatorNotes', {
                    OperationID: OperationID
                })
                .then(function(result) {
                    $scope.OpOperatorNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getWODetail(WorkOrderID) {
            API.procedure.post('ProcGetWODetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WODetail = result[0];
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getOpProcesses(OperationID) {
            API.procedure.post('ProcGetOpProcesses', {
                    OperationID: OperationID
                })
                .then(function(result) {
                    $scope.OpProcesses = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function RepeatOp() {
            var SelectedOperation = _.find($scope.WOOpsDetail, function(item) {
                return item.selected === true;
            });
            if (_.isNil(SelectedOperation)) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
            API.procedure.post('ProcRepeatOp', {
                    UserID: $rootScope.user.getId(),
                    OperationID: SelectedOperation.OperationID
                })
                .then(function(response) {
                    Notification.show('success', 'Repeat Operation success');
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }

})(angular.module('app.components.department.wo-detail', []));
