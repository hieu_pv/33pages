(function(app) {

    app.controller('PurchaseOrderMaterialSearchCtrl', PurchaseOrderMaterialSearchCtrl);
    PurchaseOrderMaterialSearchCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification'];

    function PurchaseOrderMaterialSearchCtrl($rootScope, $scope, API, $state, $stateParams, Notification) {

        $rootScope.PurchaseOrderDetail = $stateParams;

        if (!_.isNil($rootScope.PurchaseOrderDetail.RFQNo)) {

            /**
             * get ListSpec
             */
            API.procedure.post('ProcSearchMaterialSpec', {
                    MaterialSpec: ''
                })
                .then(function(result) {
                    $scope.SearchMaterialSpec = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.MaterialSelected = function(data) {
                $scope.Spec = data.MaterialSpec;
            };

            /**
             * get Matching SpecPOs
             */
            $scope.SearchMaterial = function() {
                if ($scope.Spec === undefined) {
                    $scope.Spec = '';
                }

                API.procedure.post('ProcGetMatchingSpecPOs', {
                        Spec: $scope.Spec
                    })
                    .then(function(result) {
                        $scope.MatchingSpecPOs = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.material-search', []));
