(function(app) {

    app.controller('PurchaseOrderQualityCtrl', PurchaseOrderQualityCtrl);
    PurchaseOrderQualityCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification'];

    function PurchaseOrderQualityCtrl($rootScope, $scope, API, $state, $stateParams, Notification) {

        $rootScope.PurchaseOrderDetail = $stateParams;
        if ($rootScope.PurchaseOrderDetail.RFQNo !== undefined) {

            API.procedure.post('ProcGetPOOpenGRBList', {
                    RFQOID: $stateParams.RFQNo
                })
                .then(function(result) {
                    $scope.POOpenGRBList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.get('ProcGetNextGRBRejectNo')
                .then(function(result) {
                    $scope.NextRejectNo = result[0].RejectNoteID;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.AddPOReject = function(POReject) {
                if (POReject === undefined) {
                    Notification.show('warning', 'Please select Reject clause!');
                    return false;
                }
                if (POReject.GRBID === undefined || POReject.GRBID === null) {
                    Notification.show('warning', 'Please select Open GRBs!');
                    return false;
                }
                if (POReject.RejectQty === undefined || POReject.RejectQty === null) {
                    Notification.show('warning', 'Please enter Rejected Qty!');
                    return false;
                }
                if (POReject.Deviation === undefined || POReject.Deviation === null) {
                    Notification.show('warning', 'Please enter Deviation!');
                    return false;
                }
                if (POReject.RejectClauseID === undefined || POReject.RejectClauseID === null) {
                    Notification.show('warning', 'Please select Reject clause!');
                    return false;
                }
                if (POReject.Closed === undefined || POReject.Closed === null) {
                    POReject.Closed = false;
                } else {
                    POReject.Closed = true;
                }
                POReject.RejectClauseID = parseInt(POReject.RejectClauseID);
                POReject.RejectQty = parseInt(POReject.RejectQty);

                API.procedure.post('ProcAddPOReject', {
                        UserID: $rootScope.user.getId(),
                        GRBID: POReject.GRBID.GRBID,
                        RejectQty: POReject.RejectQty,
                        Deviation: POReject.Deviation,
                        RejectClauseID: POReject.RejectClauseID,
                        Closed: POReject.Closed
                    })
                    .then(function(result) {
                        Notification.show('success', 'Create Reject success!');
                        if ($scope.NextRejectNo !== undefined) {
                            var params = {
                                RejectNoteID: $scope.NextRejectNo
                            };
                            $rootScope.openTabBirt('InspectRejectAdvice', params);
                        }
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.beforeUpload = function(file) {};

            $scope.uploadSuccess = function(response) {

                API.procedure.post('ProcAddNCRDoc', {
                        UserID: $rootScope.user.getId(),
                        FileName: response.data,
                        RejectNoteID: $scope.NextRejectNo
                    })
                    .then(function(result) {
                        Notification.show('success', 'Drop success');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.quality', []));
