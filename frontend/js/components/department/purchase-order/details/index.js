var PurchaseOrder = require('../../../../models/PurchaseOrder');

(function(app) {

    app.controller('PurchaseOrderDetailsCtrl', PurchaseOrderDetailsCtrl);
    PurchaseOrderDetailsCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', '$window'];

    function PurchaseOrderDetailsCtrl($rootScope, $scope, API, $state, $stateParams, Notification, $window) {
        $rootScope.PurchaseOrderDetail = $stateParams;
        if (!_.isNil($rootScope.PurchaseOrderDetail.RFQNo)) {

            // Show / hide items when PO Type drop-down change
            $scope.POTypeOther1 = false;

            /**
             * get PO type drop-down
             */
            $scope.POTypes = [{
                name: 'Material',
                value: 1
            }, {
                name: 'Sub-con',
                value: 2
            }, {
                name: 'Tooling',
                value: 3
            }, {
                name: 'General',
                value: 4
            }, ];

            if (_.isNil($scope.RFQPODetail)) {
                $scope.getRFQPODetail($stateParams.RFQNo, $stateParams.PONo);
            }

            /**
             * get Unit drop-down
             */
            $scope.UnitList = [{
                name: 'inches',
                value: 1
            }, {
                name: 'mm',
                value: 2
            }, ];

            /**
             * get UOM drop-down other
             */
            $scope.UOMs = [{
                name: 'Off',
                value: 1
            }];
            $scope.UOM = $scope.UOMs[0];

            /**
             * get Price drop-down
             */
            $scope.PriceList = [{
                name: 'Lot',
                value: 1
            }, {
                name: 'each',
                value: 2
            }, ];

            /**
             * Sort Table
             */
            $scope.sortTable = [{
                title: 'WO No.',
                sortBy: 'WONo.WONo'
            }, {
                title: 'Part info',
                sortBy: 'PartNumber'
            }, {
                title: 'Qty',
                sortBy: 'Qty'
            }, ];
            $scope.sortBy = $scope.sortTable[0].sortBy;
            $scope.changeSortBy = function(item) {
                $scope.sortBy = item.sortBy;
            };

            /**
             * get SPU drop-down
             */
            API.procedure.get('ProcGetUOMList')
                .then(function(result) {
                    $scope.UOMList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Shape drop-down
             */
            API.procedure.get('ProcGetShapeList')
                .then(function(result) {
                    $scope.ShapeList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get WorkOrder drop-down in Work Order table
             */
            API.procedure.get('ProcWOList')
                .then(function(result) {
                    $scope.WOList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get data Overview of table under Work Orders table
             * @param mixed ItemDefault
             */
            $scope.getPOItemDetail = function(ItemDefault) {
                if (ItemDefault === true) {
                    $scope.CurrentItem = 1;
                }
                if (ItemDefault === 'prev') {
                    if ($scope.CurrentItem > 1) {
                        $scope.CurrentItem--;
                    }
                }
                if (ItemDefault === 'next') {
                    $scope.CurrentItem++;
                }
                API.procedure.post('ProcGetPOItemDetail', {
                        RFQOID: $stateParams.RFQNo,
                        ItemNo: $scope.CurrentItem
                    })
                    .then(function(result) {
                        $scope.POItemDetail = new PurchaseOrder(result[0]);
                        if (!_.isNil($scope.POItemDetail.Per) && $scope.POItemDetail.Per !== '') {
                            $scope.POItemDetail.Per = parseInt($scope.POItemDetail.Per);
                        }
                        if ($scope.POItemDetail.Per === 2) {
                            $scope.TotalPrice = Math.round(parseFloat(($scope.POItemDetail.Price) * parseFloat($scope.POItemDetail.Qty)) * 100) / 100;
                        } else if ($scope.POItemDetail.Per === 1) {
                            $scope.TotalPrice = parseFloat($scope.POItemDetail.Price);
                        } else {
                            $scope.TotalPrice = 0;
                        }

                        // Selected PO type
                        $scope.POItemDetail.pkOperationTypeID = parseInt($scope.POItemDetail.pkOperationTypeID);
                        $scope.POItemDetail.POType = _.find($scope.POTypes, {
                            'value': $scope.POItemDetail.pkOperationTypeID
                        });
                        if ($scope.POItemDetail.pkOperationTypeID !== 1) {
                            $scope.POTypeOther1 = true;
                        } else {
                            $scope.POTypeOther1 = false;
                        }

                        // Selected SPU drop-down
                        $scope.$watch('UOMList', function(UOMList) {
                            if (UOMList !== undefined) {
                                $scope.SPUDetail = _.find(UOMList, function(item) {
                                    return parseInt(item.pkUOMID) === parseInt($scope.POItemDetail.SPUID);
                                });
                            }
                        });

                        // Selected Unit drop-down
                        $scope.$watch('UnitList', function(UnitList) {
                            if (UnitList !== undefined) {
                                $scope.UnitDetail = _.find(UnitList, function(item) {
                                    return parseInt(item.value) === parseInt($scope.POItemDetail.UnitID);
                                });
                            }
                        });

                        // Selected Shape drop-down
                        $scope.$watch('ShapeList', function(ShapeList) {
                            if (ShapeList !== undefined) {
                                $scope.ShapeDetail = _.find(ShapeList, function(item) {
                                    return parseInt(item.ShapeID) === parseInt($scope.POItemDetail.ShapeID);
                                });
                            }
                        });

                        // Selected Price drop-down
                        $scope.$watch('PriceList', function(PriceList) {
                            if (PriceList !== undefined) {
                                $scope.PriceType = _.find(PriceList, function(item) {
                                    return parseInt(item.value) === parseInt($scope.POItemDetail.Per);
                                });
                            }
                        });

                        /**
                         * get Work Orders table
                         */
                        if ($scope.POItemDetail.ItemID !== undefined && $scope.POItemDetail.ItemID !== null) {
                            API.procedure.post('ProcGetPOLinkedWODetail', {
                                    ItemID: $scope.POItemDetail.ItemID
                                })
                                .then(function(result) {
                                    $scope.getWorkOrderTable(result, false);
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getPOItemDetail(true);

            /**
             * the function working when click Next button
             * when currentItem = totalItem
             */
            $scope.addNewItem = function() {
                if (parseInt($scope.CurrentItem) === parseInt($scope.POItemDetail.TotalItems)) {
                    API.procedure.post('ProcAddItem', {
                            UserID: $rootScope.user.getId(),
                            RFQOID: $stateParams.RFQNo
                        })
                        .then(function(result) {
                            $scope.getPOItemDetail('next');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    $scope.getPOItemDetail('next');
                }
            };

            /**
             * the function working when select Work Order drop-down
             */
            $scope.changeWONo = function(WorkOrder) {

                // Selected Work Order when data of ProcGetSelectedWODetail = null
                $scope.WOList.forEach(function(item) {
                    if (item.WONo === WorkOrder.WONo) {
                        $scope.WorkOrder = item;
                    }
                });

                API.procedure.post('ProcGetSelectedWODetail', {
                        WorkOrderID: WorkOrder.WONo
                    })
                    .then(function(result) {
                        $scope.getWorkOrderTable(result, WorkOrder.WONo);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * the function to get data of Work Order table
             */
            $scope.getWorkOrderTable = function(array, WorkOrderID) {
                $scope.MaxOperation = _.maxBy(array, 'OperationNo');

                // Selected Work Order drop-down
                $scope.WOList.forEach(function(value) {
                    array.forEach(function(item) {
                        if (WorkOrderID !== false) {
                            if (WorkOrderID === value.WONo) {
                                item.WONo = value;
                            }
                        } else {
                            if (item.WONo === value.WONo) {
                                item.WONo = value;
                            }
                        }
                    });
                });
                $scope.SelectedWODetail = [
                    array[0]
                ];
                $scope.WODetail = angular.copy(array[0]);
                $scope.filterWODetail = angular.copy(array);
            };

            /**
             * the function working when click Link button
             */
            $scope.LinkToPOItem = function() {
                if ($scope.WODetail === undefined) {
                    Notification.show('warning', 'Please select Work Order');
                    return false;
                }

                if ($scope.POItemDetail !== undefined && $scope.POItemDetail.ItemID !== undefined) {
                    $scope.WODetail.OperationNo = parseInt($scope.WODetail.OperationNo);
                    $scope.MaxOperation.OperationNo = parseInt($scope.MaxOperation.OperationNo);
                    if ($scope.WODetail.OperationNo <= $scope.MaxOperation.OperationNo) {
                        $scope.WODetail = $scope.filterWODetail[$scope.WODetail.OperationNo - 1];
                    }
                    API.procedure.post('ProcAddWOLinkToPOItem', {
                            UserID: $rootScope.user.getId(),
                            OperationID: $scope.WODetail.OperationID,
                            ItemID: $scope.POItemDetail.ItemID
                        })
                        .then(function(result) {
                            Notification.show('success', 'Link Work Order success');
                        })
                        .catch(function(error) {
                            Notification.show('warning', 'Work Order Linked');
                            throw error;
                        });
                }
            };

            $scope.totalPrice = function(PriceType, POItemDetail) {
                if (!_.isUndefined(PriceType)) {
                    if (PriceType.value === 2) {
                        var NumberRegex = /^\d+$/;
                        var PriceRegex = /(^\d{1,7}$)|(^\d{1,6}\.\d{1}$)|(^\d{1,5}\.\d{1,2}$)/;
                        if (PriceRegex.test(POItemDetail.Price) && NumberRegex.test(POItemDetail.Qty)) {
                            $scope.TotalPrice = Math.round((parseFloat(POItemDetail.Price) * parseFloat(POItemDetail.Qty)) * 100) / 100;
                        } else {
                            $scope.TotalPrice = 0;
                        }
                    } else if (PriceType.value === 1) {
                        $scope.TotalPrice = parseFloat(POItemDetail.Price);
                    } else {
                        $scope.TotalPrice = 0;
                    }
                }
            };

            /**
             * the function working when click Save button
             */
            $scope.UpdateOrder = function(POItemDetail) {
                var POType;
                if (_.isUndefined(POItemDetail.POType)) {
                    POType = null;
                } else {
                    POType = POItemDetail.POType.value;
                }
                API.procedure.post('ProcUpdateOrderItemDetail', {
                        UserID: $rootScope.user.getId(),
                        ItemID: POItemDetail.getItemID(),
                        Qty: POItemDetail.getQty(),
                        SPU: POItemDetail.getSPU(),
                        SPUID: POItemDetail.getSPUID(),
                        Size: POItemDetail.getSize(),
                        UnitID: POItemDetail.getUnitID(),
                        ShapeID: POItemDetail.getShapeID(),
                        PartDescription: POItemDetail.getPartDescription(),
                        Comment: POItemDetail.getComment(),
                        Price: POItemDetail.getPrice(),
                        Per: POItemDetail.getPer(),
                        POType: POType
                    })
                    .then(function(result) {
                        Notification.show('success', 'Update Order Item success');
                        // $scope.getPOItemDetail(true);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * filter Work Order when select PO type drop-down
             */
            $scope.changePOType = function(POType) {
                if (POType.value !== 1) {
                    $scope.POTypeOther1 = true;
                } else {
                    $scope.POTypeOther1 = false;
                }
                // remove filter because now $scope.WODetail always return once value
                // $scope.SelectedWODetail = _.filter($scope.filterWODetail, _.matches({ 'OpTypeID': POType.value }));
                // if ($scope.SelectedWODetail.length > 0) {
                // } else {
                //     $scope.WorkOrder = null;
                // }
            };

            $scope.redirectBack = function() {
                $window.history.back();
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.details', []));
