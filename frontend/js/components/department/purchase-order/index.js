require('./details');
require('./instructions');
require('./invoice');
require('./material-search');
require('./notes');
require('./quality');
require('./raise');
require('./rfq');
require('./supplier');

(function(app) {

    app.controller('PurchaseOrderCtrl', PurchaseOrderCtrl);
    PurchaseOrderCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams'];

    function PurchaseOrderCtrl($rootScope, $scope, API, Notification, $state, $stateParams) {

        $scope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            if (from.name.indexOf('purchase-order') === -1) {
                var isNull = false;
                var params = [
                    'RFQNo',
                    'PONo',
                    'PartNumber',
                    'Issue',
                    'Qty',
                    'Raised',
                    'Description'
                ];
                _.forEach(params, function(param) {
                    if (_.isNil(toParams[param])) {
                        isNull = true;
                    }
                });

                // Remove search element
                if (isNull) {
                    $scope.searchInputModel = null;
                    $scope.PurchaseOrderDetail = null;
                }
            }
        });

        /**
         * the function working when select Part No search
         *
         * after select will redirect to Details tab
         */
        $scope.PartNoSelected = function(PartNo) {
            if (!_.isNil(PartNo.Raised)) {
                PartNo.Raised = $rootScope.convertTime(PartNo.Raised, 'DD/MM/YYYY');
            }
            $scope.PurchaseOrderDetail = PartNo;
            $scope.getRFQPODetail(PartNo.RFQNo, PartNo.PONo);

            var state = 'department.purchase-order.details';
            var params = {
                RFQNo: PartNo.RFQNo,
                PONo: PartNo.PONo,
                PartNumber: PartNo.PartNumber,
                Issue: PartNo.Issue,
                Qty: PartNo.Qty,
                Raised: PartNo.Raised,
                Description: PartNo.Description
            };
            $rootScope.redirectDept(state, params);
        };

        /**
         * the function working when select PO search
         */
        $scope.PONoSelected = function(PO) {
            if (_.isNil($scope.PurchaseOrderDetail)) {
                $scope.PurchaseOrderDetail = {};
            }
            $scope.PurchaseOrderDetail.PONo = PO.PONo;

            API.procedure.post('ProcPOSearchList', {
                    POSearchField: PO.PONo
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.POSearchList = result[0];
                        $scope.PurchaseOrderDetail.RFQNo = $scope.POSearchList.RFQOID;
                        $scope.getRFQPODetail($scope.POSearchList.RFQOID, $scope.POSearchList.PONo);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when select RFQ search
         */
        $scope.RFQNoSelected = function(RFQ) {
            if (_.isNil($scope.PurchaseOrderDetail)) {
                $scope.PurchaseOrderDetail = {};
            }
            $scope.PurchaseOrderDetail.RFQNo = RFQ.RFQNo;
            $scope.getRFQPODetail(RFQ.RFQNo, null);
        };

        $scope.getRFQPODetail = function(RFQNo, PONo) {
            var state = 'department.purchase-order.details';
            var params = {};
            if (_.isUndefined(RFQNo)) {
                RFQNo = null;
            } else {
                params.RFQNo = RFQNo;
            }
            if (_.isUndefined(PONo)) {
                PONo = null;
            } else {
                params.PONo = PONo;
            }
            $rootScope.redirectDept(state, params);
            API.procedure.post('ProcGetRFQPODetail', {
                    RFQNo: RFQNo,
                    PONo: PONo
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.RFQPODetail = result[0];
                        // $scope.RFQPODetail.Raised = $rootScope.convertTime($scope.RFQPODetail.Raised, 'DD/MM/YYYY');
                        if (_.isNil($scope.PurchaseOrderDetail)) {
                            $scope.PurchaseOrderDetail = {};
                        }
                        $scope.PurchaseOrderDetail.RFQNo = RFQNo;
                        $scope.PurchaseOrderDetail.PONo = PONo;
                        $scope.PurchaseOrderDetail.Raised = $rootScope.convertTime($scope.RFQPODetail.Raised, 'DD/MM/YYYY');
                        $scope.PurchaseOrderDetail.Complete = parseInt($scope.RFQPODetail.Complete);

                        params.PartNumber = $scope.RFQPODetail.PartNumber;
                        params.Raised = $scope.RFQPODetail.Raised;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click Complete button radio
         */
        $scope.POComplete = function() {
            if ($scope.PurchaseOrderDetail.PONo !== undefined && $scope.PurchaseOrderDetail.PONo !== null) {

                API.procedure.post('ProcCompletePO', {
                        UserID: $rootScope.user.getId(),
                        PONo: $scope.PurchaseOrderDetail.PONo
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * the function working when click New button
         */
        $scope.AddNewRFQ = function() {
            API.procedure.post('ProcAddNewRFQ', {
                    UserID: $rootScope.user.getId()
                })
                .then(function(result) {
                    var lastInsertID = result[0].Last_Insert_ID;
                    $scope.getRFQPODetail(lastInsertID);
                    var state = 'department.purchase-order.details';
                    var params = {
                        RFQNo: lastInsertID
                    };
                    Notification.show('success', 'Add New RFQ success');
                    AddItem(lastInsertID);
                    $rootScope.redirectDept(state, params);
                })
                .catch(function(error) {
                    throw error;
                });
        };

        function AddItem(RFQNo) {
            API.procedure.post('ProcAddItem', {
                    UserID: $rootScope.user.getId(),
                    RFQOID: RFQNo
                })
                .then(function(result) {
                    console.log(result);
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }

})(angular.module('app.components.department.purchase-order', [
    'app.components.department.purchase-order.details',
    'app.components.department.purchase-order.instructions',
    'app.components.department.purchase-order.invoice',
    'app.components.department.purchase-order.material-search',
    'app.components.department.purchase-order.notes',
    'app.components.department.purchase-order.quality',
    'app.components.department.purchase-order.raise',
    'app.components.department.purchase-order.rfq',
    'app.components.department.purchase-order.supplier',
]));
