(function(app) {

    app.controller('PurchaseOrderInstructionsCtrl', PurchaseOrderInstructionsCtrl);
    PurchaseOrderInstructionsCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', '$window'];

    function PurchaseOrderInstructionsCtrl($rootScope, $scope, API, $state, $stateParams, Notification, $window) {
        $rootScope.PurchaseOrderDetail = $stateParams;

        if (!_.isNil($rootScope.PurchaseOrderDetail.RFQNo)) {

            /**
             * get PO comment table
             */
            API.procedure.post('ProcGetOrderComments', {
                    RFQOID: $stateParams.RFQNo
                })
                .then(function(result) {
                    $scope.OrderComments = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get document table
             */
            API.procedure.post('ProcGetOrderDocuments', {
                    RFQOID: $stateParams.RFQNo
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.OrderDocuments = result;
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.addComment = function(evt, Comment) {
                if (evt.keyCode === 13) {
                    if ($window.confirm('are you want add comment?')) {
                        API.procedure.post('ProcAddCommentToPO', {
                                UserID: $rootScope.user.getId(),
                                RFQOID: $stateParams.RFQNo,
                                Comment: Comment
                            })
                            .then(function(result) {
                                $state.reload();
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            };

            $scope.changeComment = function(Comment) {
                API.procedure.post('ProcCommentSearch', {
                        SearchField: Comment
                    })
                    .then(function(result) {
                        $scope.CommentList = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.UpdateOrderDocument = function(item) {
                API.procedure.post('ProcUpdateOrderDocument', {
                        UserID: $rootScope.user.getId(),
                        DocumentID: item.DocumentID,
                        AutoPrint: item.AutoPrint
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.instructions', []));
