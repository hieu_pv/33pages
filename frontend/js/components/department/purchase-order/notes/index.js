(function(app) {

    app.controller('PurchaseOrderNotesCtrl', PurchaseOrderNotesCtrl);
    PurchaseOrderNotesCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification'];

    function PurchaseOrderNotesCtrl($rootScope, $scope, API, $state, $stateParams, Notification) {

        $rootScope.PurchaseOrderDetail = $stateParams;

        if (!_.isNil($rootScope.PurchaseOrderDetail.RFQNo)) {

            /**
             * get Contact Type
             */
            $scope.ContactTypes = [{
                name: 'Telephone',
                value: 1
            }, {
                name: 'Facsimile',
                value: 2
            }, {
                name: 'email',
                value: 3
            }, {
                name: 'Letter',
                value: 4
            }, ];

            /**
             * get Process table
             */
            API.procedure.post('ProcGetOrderProgress', {
                    RFQOID: $stateParams.RFQNo
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.OrderProgress = result;

                        // Selected ContactType
                        $scope.OrderProgress.forEach(function(progress) {
                            $scope.ContactTypes.forEach(function(contact) {
                                if (progress.ContactTypeID === contact.value) {
                                    progress.ContactTypeID = contact;
                                }
                            });
                        });
                    }

                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when user press Enter key a row of table
             */
            $scope.AddOrderProgress = function(OrderProgressItem) {
                if (_.isNil(OrderProgressItem)) {
                    Notification.show('warning', 'Please enter full information');
                    return false;
                }
                if (_.isNil(OrderProgressItem.ContactType)) {
                    Notification.show('warning', 'Please select Contact Type');
                    return false;
                }
                if (_.isNil(OrderProgressItem.Progress) || OrderProgressItem.Progress === '') {
                    Notification.show('warning', 'Please enter Progress Note');
                    return false;
                }
                if (_.isNil(OrderProgressItem.Chase)) {
                    OrderProgressItem.Chase = 0;
                }

                API.procedure.post('ProcAddOrderProgress', {
                        UserID: $rootScope.user.getId(),
                        RFQOID: $stateParams.RFQNo,
                        ContactType: OrderProgressItem.ContactType.value,
                        Progress: OrderProgressItem.Progress,
                        Chase: OrderProgressItem.Chase
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.notes', []));
