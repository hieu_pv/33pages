(function(app) {

    app.controller('PurchaseOrderSupplierCtrl', PurchaseOrderSupplierCtrl);
    PurchaseOrderSupplierCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification'];

    function PurchaseOrderSupplierCtrl($rootScope, $scope, API, $state, $stateParams, Notification) {
        var regexDecimal5_2 = /^(\d{1,5}|\d{1,4}\.\d{1}|\d{1,3}\.\d{1,2})$/;

        $rootScope.PurchaseOrderDetail = $stateParams;
        if ($rootScope.PurchaseOrderDetail.RFQNo !== undefined) {

            /**
             * get Supplier drop-down
             */
            API.procedure.get('ProcSupplierList')
                .then(function(result) {
                    $scope.SupplierList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Del Comments drop-down
             */
            API.procedure.get('ProcDeliveryCommentsList')
                .then(function(result) {
                    $scope.DeliveryCommentsList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get data Overview
             */
            API.procedure.post('ProcGetPOSupplierDetail', {
                    RFQOID: $stateParams.RFQNo
                })
                .then(function(result) {
                    $scope.POSupplierDetail = result;
                    if (result.length > 0) {
                        result[0].DeliveryDate = $rootScope.convertTime(result[0].DeliveryDate, 'DD/MM/YYYY');
                        if (!_.isNil(result[0].TotalCost) && result[0].TotalCost !== '') {
                            result[0].TotalCost = Math.round(result[0].TotalCost * 100) / 100;
                        }
                        $scope.POSupplierDetail = result[0];
                        if (regexDecimal5_2.test($scope.POSupplierDetail.DeliveryCharge) && regexDecimal5_2.test($scope.POSupplierDetail.CertCharge)) {
                            $scope.POSupplierDetail.TotalNetCost = parseFloat($scope.POSupplierDetail.DeliveryCharge) + parseFloat($scope.POSupplierDetail.CertCharge) + parseFloat($scope.POSupplierDetail.TotalCost);
                            $scope.POSupplierDetail.TotalNetCost = Math.round($scope.POSupplierDetail.TotalNetCost * 100) / 100;
                        } else {
                            $scope.POSupplierDetail.TotalNetCost = 0;
                        }

                        // Selected Supplier drop-down
                        if ($scope.POSupplierDetail.SupplierID !== null && $scope.POSupplierDetail.SupplierID !== "") {
                            $scope.$watch('SupplierList', function(SupplierList) {
                                if (SupplierList !== undefined) {
                                    $scope.POSupplierDetail.SupplierID = _.find(SupplierList, function(value) {
                                        return parseInt(value.SupplierID) === parseInt($scope.POSupplierDetail.SupplierID);
                                    });
                                    $scope.SelectSupplier($scope.POSupplierDetail.SupplierID);
                                    $scope.$watch('SupplierContacts', function(SupplierContacts) {
                                        if (SupplierContacts !== undefined) {

                                            // Select Client Contact drop-down
                                            $scope.POSupplierDetail.ClientContactID = _.find(SupplierContacts, function(value) {
                                                return parseInt(value.ContactID) === parseInt($scope.POSupplierDetail.ClientContactID);
                                            });
                                        }
                                    });
                                }
                            });
                        }

                        // Selected DelComments drop-down
                        if ($scope.POSupplierDetail.DeliveryCommentID !== null && $scope.POSupplierDetail.DeliveryCommentID !== "") {
                            $scope.$watch('DeliveryCommentsList', function(DeliveryCommentsList) {
                                if (DeliveryCommentsList !== undefined) {
                                    $scope.POSupplierDetail.DeliveryCommentID = _.find(DeliveryCommentsList, function(value) {
                                        return parseInt(value.DeliveryCommentID) === parseInt($scope.POSupplierDetail.DeliveryCommentID);
                                    });
                                }
                            });
                        }
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when select Supplier drop-down
             */
            $scope.SelectSupplier = function(Supplier) {
                /**
                 * populate Client Contact drop-down
                 */
                API.procedure.post('ProcGetSupplierContacts', {
                        SupplierID: Supplier.SupplierID
                    })
                    .then(function(result) {
                        $scope.SupplierContacts = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.TotalNetOrder = function(POSupplierDetail) {
                if (regexDecimal5_2.test(POSupplierDetail.DeliveryCharge) && regexDecimal5_2.test(POSupplierDetail.CertCharge)) {
                    POSupplierDetail.TotalNetCost = parseFloat(POSupplierDetail.DeliveryCharge) + parseFloat(POSupplierDetail.CertCharge) + parseFloat(POSupplierDetail.TotalCost);
                    POSupplierDetail.TotalNetCost = Math.round(POSupplierDetail.TotalNetCost * 100) / 100;
                } else {
                    POSupplierDetail.TotalNetCost = 0;
                }
            };

            $scope.UpdatePOSupplier = function(POSupplierDetail) {
                if (POSupplierDetail === undefined) {
                    POSupplierDetail = {};
                }
                var timeFormat = 'DD/MM/YYYY';
                var DeliveryDate;
                var DeliveryCharge;
                var CertCharge;
                var DeliveryCommentID;

                if (_.isNil(POSupplierDetail.SupplierID) || POSupplierDetail.SupplierID === '') {
                    SupplierID = '';
                } else {
                    SupplierID = POSupplierDetail.SupplierID.SupplierID;
                }
                if (_.isNil(POSupplierDetail.SupplierRef)) {
                    POSupplierDetail.SupplierRef = '';
                }
                if (_.isNil(POSupplierDetail.ApprovalComment)) {
                    POSupplierDetail.ApprovalComment = '';
                }
                if (_.isNil(POSupplierDetail.DeliveryDays)) {
                    POSupplierDetail.DeliveryDays = '';
                }
                if (_.isNil(POSupplierDetail.TotalCost)) {
                    POSupplierDetail.TotalCost = null;
                } else {
                    POSupplierDetail.TotalCost = POSupplierDetail.TotalCost;
                }
                if (_.isNil(POSupplierDetail.ClientContactID) || POSupplierDetail.ClientContactID === '') {
                    ClientContactID = '';
                } else {
                    ClientContactID = POSupplierDetail.ClientContactID.ContactID;
                }
                if (!regexDecimal5_2.test(POSupplierDetail.DeliveryCharge) || _.isUndefined(POSupplierDetail.DeliveryCharge) || POSupplierDetail.DeliveryCharge === '' || parseFloat(POSupplierDetail.DeliveryCharge) === 0) {
                    DeliveryCharge = null;
                } else {
                    DeliveryCharge = POSupplierDetail.DeliveryCharge;
                }
                if (!regexDecimal5_2.test(POSupplierDetail.CertCharge) || _.isUndefined(POSupplierDetail.CertCharge) || POSupplierDetail.CertCharge === '' || parseFloat(POSupplierDetail.CertCharge) === 0) {
                    CertCharge = null;
                } else {
                    CertCharge = POSupplierDetail.CertCharge;
                }
                if (!$rootScope.validateDate(POSupplierDetail.DeliveryDate, timeFormat) || _.isUndefined(POSupplierDetail.DeliveryDate) || POSupplierDetail.DeliveryDate === '') {
                    DeliveryDate = null;
                } else {
                    var SplitDate = POSupplierDetail.DeliveryDate.split('/');
                    DeliveryDate = SplitDate[2] + '-' + SplitDate[1] + '-' + SplitDate[0];
                }
                if (_.isNil(POSupplierDetail.DeliveryCommentID) || POSupplierDetail.DeliveryCommentID === '') {
                    DeliveryCommentID = '';
                } else {
                    DeliveryCommentID = POSupplierDetail.DeliveryCommentID.DeliveryCommentID;
                }

                API.procedure.post('ProcUpdatePOSupplierDetail', {
                        UserID: $rootScope.user.getId(),
                        RFQOID: parseInt($stateParams.RFQNo),
                        SupplierRef: POSupplierDetail.SupplierRef,
                        DeliveryCharge: parseFloat(DeliveryCharge),
                        CertCharge: parseFloat(CertCharge),
                        TotalCost: parseFloat(POSupplierDetail.TotalCost),
                        ApprovalComment: POSupplierDetail.ApprovalComment,
                        DeliveryDays: parseInt(POSupplierDetail.DeliveryDays),
                        DeliveryDate: DeliveryDate,
                        DeliveryCommentID: parseInt(DeliveryCommentID),
                        SupplierID: parseInt(SupplierID),
                        ContactID: parseInt(ClientContactID)
                    })
                    .then(function(result) {
                        Notification.show('success', 'update success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.supplier', []));
