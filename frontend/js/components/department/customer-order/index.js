var moment = require('moment');

(function(app) {
    app.controller('CustomerOrderCtrl', CustomerOrderCtrl);

    CustomerOrderCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function CustomerOrderCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        if ($scope.CustomerOrder === undefined) {
            $scope.CustomerOrder = {};
        }
        $scope.CustomerOrder.CustomerOrderRef = $stateParams.CustomerOrderRef;
        var regexDecimal7_2 = /^(\d{1,7}|\d{1,6}\.\d{1}|\d{1,5}\.\d{1,2})$/;
        var regexNumber = /^\d+$/;
        var DateFormat = 'DD/MM/YYYY';

        var hasCustomerId = false;
        var hasCustomerOrderId = false;
        var hasCustomerOrderRef = false;
        if ($stateParams.CustomerId !== undefined) {
            hasCustomerId = true;
        }
        if ($stateParams.CustomerOrderId !== undefined) {
            hasCustomerOrderId = true;
        }
        if ($stateParams.CustomerOrderRef !== undefined) {
            hasCustomerOrderRef = true;
        }

        // Use to show/hide modal when user click Cancel Lineitem
        $scope.showDialog = false;

        /**
         * get Release Type drop-down
         */
        API.procedure.get('ProcGetOurApprovalList')
            .then(function(result) {
                $scope.ReleaseTypes = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Customer drop-down
         */
        API.procedure.get('ProcCustomerList')
            .then(function(result) {
                $scope.customers = result;
                if (hasCustomerId) {
                    $scope.customer = _.find(result, function(customer) {
                        return parseInt(customer.pkCustomerID) === parseInt($stateParams.CustomerId);
                    });
                }
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * the function working when select Customer drop-down
         */
        $scope.changeCustomer = function(customer) {
            $state.go('department.customer-order', {
                CustomerId: customer.pkCustomerID,
                CustomerOrderId: undefined,
                CustomerOrderRef: undefined,
            });
        };

        if (hasCustomerId) {
            var customer_id = $stateParams.CustomerId;

            /**
             * get Delivery point drop-down
             */
            API.procedure.post('ProcGetCustDeliveryPoints', {
                    CustomerID: customer_id
                })
                .then(function(result) {
                    $scope.DeliveryPoints = result;
                    if ($scope.DeliveryPoints.length === 1) {
                        $scope.DeliveryPoint = $scope.DeliveryPoints[0];
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.post('ProcGetCustomerApproval', {
                    CustomerID: customer_id
                })
                .then(function(response) {
                    $scope.$watch('ReleaseTypes', function(ReleaseTypes) {
                        if (!_.isUndefined(ReleaseTypes)) {
                            $scope.ReleaseType = _.find(ReleaseTypes, function(item) {
                                return parseInt(response[0].fkApprovalID) === parseInt(item.pkApprovalID);
                            });
                        }
                    });
                }, function(error) {
                    throw error;
                });

            /**
             * get Customer Order drop-down
             */
            API.procedure.post('ProcCustomerOrderList', {
                    CustomerID: customer_id
                })
                .then(function(result) {
                    $scope.CustomerOrderList = result;
                    $scope.selectCO = _.find(result, function(item) {
                        return parseInt(item.OrderID) === parseInt($stateParams.CustomerOrderId);
                    });
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * the function working when select Customer Order drop-down
         */
        $scope.selectCustomerOrder = function(item) {
            $state.go('department.customer-order', {
                CustomerId: $stateParams.CustomerId,
                CustomerOrderId: item.OrderID,
                CustomerOrderRef: item.CustomerOrderRef,
            });
        };

        if (hasCustomerOrderId) {
            var OrderID = $stateParams.CustomerOrderId;

            /*
             * get Customer Order Notes
             */
            API.procedure.post('ProcGetCustOrderNotes', {
                    OrderID: OrderID
                })
                .then(function(result) {
                    $scope.CustOrderNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /*
             * get Acknowledgement Note table
             */
            API.procedure.post('ProcGetAcknowNotes', {
                    OrderID: OrderID
                })
                .then(function(result) {
                    $scope.AcknowNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get overview data of Customer Order
             */
            API.procedure.post('ProcGetCustomerOrder', {
                    OrderID: OrderID
                })
                .then(function(result) {
                    $scope.CustomerOrder = result[0];
                    $scope.CustomerOrderRef = $scope.CustomerOrder.CustomerOrderRef;

                    /**
                     * selected ReleaseTypes
                     */
                    $scope.$watch('ReleaseTypes', function(ReleaseTypes) {
                        if (!_.isUndefined(ReleaseTypes)) {
                            $scope.ReleaseType = _.find(ReleaseTypes, function(item) {
                                return parseInt($scope.CustomerOrder.fkApprovalID) === parseInt(item.pkApprovalID);
                            });
                        }
                    });

                    /**
                     * selected Delivery Point
                     */
                    $scope.$watch('DeliveryPoints', function(DeliveryPoints) {
                        if (DeliveryPoints !== undefined) {
                            $scope.DeliveryPoint = _.find(DeliveryPoints, function(item) {
                                return parseInt($scope.CustomerOrder.fkDeliveryPointID) === parseInt(item.DPID);
                            });
                        }
                    });
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Re-calculate Total Price when change Each Price
             */
            $scope.changeEachPrice = function(LIWOinfo) {
                totalPrice = 0;
                totalReqd = 0;
                totalBuild = 0;

                LIWOinfo[0].forEach(function(value, key) {
                    if (value.Reqd !== undefined && value.Reqd !== null) {
                        totalReqd += parseInt(value.Reqd);
                    }
                    if (value.Build !== undefined && value.Build !== null) {
                        totalBuild += parseInt(value.Build);
                    }
                    if (value.PriceEach !== undefined && value.PriceEach !== null && value.PriceEach !== "" && regexDecimal7_2.test(value.PriceEach)) {
                        totalPrice += parseFloat(value.PriceEach) * value.Reqd;
                    }
                });

                $scope.totalPrice = Math.round(totalPrice * 100) / 100;
                $scope.totalReqd = totalReqd;
                $scope.totalBuild = totalBuild;
            };

            /**
             * get data of main table
             */
            $scope.getLIFOinfo = function() {

                API.procedure.post('ProcGetLIWOinfo', {
                        OrderID: OrderID
                    })
                    .then(function(result) {
                        var totalPrice = 0,
                            totalReqd = 0,
                            totalBuild = 0;

                        result.forEach(function(value, key) {
                            if (value.DeliveryDate !== null) {
                                if (value.DeliveryDate !== '0000-00-00') {
                                    result[key].DeliveryDate = $rootScope.convertTime(value.DeliveryDate, 'DD/MM/YYYY');
                                } else {
                                    result[key].DeliveryDate = null;
                                }
                            }
                            if (value.Reqd !== undefined && value.Reqd !== null) {
                                totalReqd += parseInt(value.Reqd);
                            }
                            if (value.Build !== undefined && value.Build !== null) {
                                totalBuild += parseInt(value.Build);
                            }
                            if (value.PriceEach !== undefined && value.PriceEach !== null && value.PriceEach !== "" && regexDecimal7_2.test(value.PriceEach)) {
                                totalPrice += parseFloat(value.PriceEach) * value.Reqd;
                            }
                        });

                        $scope.totalPrice = Math.round(totalPrice * 100) / 100;
                        $scope.totalReqd = totalReqd;
                        $scope.totalBuild = totalBuild;

                        var LIWOinfo = _.values(_.groupBy(result, function(item) {
                            return item.CustRef;
                        }));

                        // Selected Template Drop-down
                        $scope.$watch('TemplateLists', function(TemplateLists) {
                            if (TemplateLists !== undefined) {
                                if (LIWOinfo.length > 0) {
                                    LIWOinfo.forEach(function(value) {
                                        value.forEach(function(value2) {
                                            if (!_.isNil(value2.Live)) {
                                                value2.Live = parseInt(value2.Live);
                                            }
                                            value2.FAIR = parseInt(value2.FAIR);
                                            if (!_.isNil(value2.PTemplateID)) {
                                                value2.PTemplateID = _.find(TemplateLists, function(item) {
                                                    return item.PTemplateID === value2.PTemplateID;
                                                });
                                                if (!_.isNil(value2.PTemplateID)) {
                                                    value2.PTemplateID.PlanningComplete = parseFloat(value2.PTemplateID.PlanningComplete);
                                                    value2.PTemplateID.ETemplateID = parseFloat(value2.PTemplateID.ETemplateID);
                                                    value2.PTemplateID.Status = parseFloat(value2.PTemplateID.Status);
                                                }
                                            }
                                        });
                                    });
                                }
                            }
                        });
                        $scope.LIWOinfo = LIWOinfo;
                        console.log('LIWOinfo ', LIWOinfo);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getLIFOinfo();
        }

        if (hasCustomerOrderRef) {
            $scope.CustomerOrderRef = $stateParams.CustomerOrderRef;
        }

        $scope.selectWO = function(WO, LIWOinfo) {
            _(LIWOinfo).forEach(function(LIWO) {
                _(LIWO).forEach(function(item) {
                    item.selected = false;
                });
            });
            WO.selected = true;
        };

        /**
         * the function working when click View Work Order button
         */
        $scope.ViewWO = function(LIWOinfo) {
            if (_.isNil(LIWOinfo)) {
                Notification.show('warning', 'Please select Customer & Customer Order');
                return false;
            }
            var selectedWO;
            _(LIWOinfo).forEach(function(LIWO) {
                selectedWO = _.find(LIWO, {
                    'selected': true
                });
                if (selectedWO !== undefined) {
                    $state.go('department.workorders.main', {
                        WorkOrderID: selectedWO.WONo
                    });
                } else {
                    Notification.show('warning', 'Please select Work Order');
                }
            });
        };

        /**
         * add item for Customer Order Notes table
         *
         * the function working when press Enter key
         * on Customer Order Notes table
         */
        $scope.addCoNotes = function(item) {
            API.procedure.post('ProcAddCustOrderNote', {
                    UserID: $rootScope.user.getId(),
                    OrderID: OrderID,
                    OrderNote: item.Note
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * add item for Acknowledgement Note table
         *
         * the function working when press Enter key
         * on Acknowledgement Note table
         */
        $scope.addACNote = function(item) {
            API.procedure.post('ProcAddAcknowNote', {
                    UserID: $rootScope.user.getId(),
                    OrderID: OrderID,
                    AcknowNote: item.Note
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.validateNewUpateCO = function(CustomerOrder, ReleaseType, DeliveryPoint) {
            if (CustomerOrder === undefined) {
                Notification.show('warning', 'Please enter Customer Order Number');
                return false;
            }
            if (CustomerOrder.CustomerOrderRef === '' | CustomerOrder.CustomerOrderRef === undefined) {
                Notification.show('warning', 'Please enter Customer Order Number');
                return false;
            }
            if (ReleaseType === undefined) {
                Notification.show('warning', 'Please select Release type');
                return false;
            }
            if (DeliveryPoint === undefined) {
                Notification.show('warning', 'Please select Delivery point');
                return false;
            }
            if (CustomerOrder.CommerciallyViable === undefined | CustomerOrder.CommerciallyViable === null) {
                CustomerOrder.CommerciallyViable = 0;
            }
            if (CustomerOrder.AmendmentReviewed === undefined | CustomerOrder.AmendmentReviewed === null) {
                CustomerOrder.AmendmentReviewed = 0;
            }
            return CustomerOrder;
        };

        /**
         * the function working when click New Order button in top screen
         */
        $scope.AddNewCO = function(customer, CustomerOrder, ReleaseType, DeliveryPoint) {
            if (customer === undefined) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var data = $scope.validateNewUpateCO(CustomerOrder, ReleaseType, DeliveryPoint);
            if (data !== false) {

                API.procedure.post('ProcAddNewCO', {
                        UserID: $rootScope.user.getId(),
                        CustomerID: customer.pkCustomerID,
                        OrderRef: data.CustomerOrderRef,
                        ReleaseTypeID: ReleaseType.pkApprovalID,
                        DeliveryPointID: DeliveryPoint.DPID,
                        CommerciallyViable: data.CommerciallyViable,
                        AmendmentReviewed: data.AmendmentReviewed
                    })
                    .then(function(result) {
                        Notification.show('success', 'add Customer Order success');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * the function is trigger with the input:
         *
         * Customer Order Number
         * Commercially viable?
         * Amendment reviewed?
         */
        $scope.UpdateCO = function(selectCO, CustomerOrder, ReleaseType, DeliveryPoint) {
            if (selectCO !== undefined) {
                if (CustomerOrder.CommerciallyViable === undefined | CustomerOrder.CommerciallyViable === null) {
                    CustomerOrder.CommerciallyViable = 0;
                }
                if (CustomerOrder.AmendmentReviewed === undefined | CustomerOrder.AmendmentReviewed === null) {
                    CustomerOrder.AmendmentReviewed = 0;
                }
                if (DeliveryPoint === undefined) {
                    DeliveryPoint = null;
                } else {
                    DeliveryPoint = DeliveryPoint.DPID;
                }
                if (ReleaseType === undefined) {
                    ReleaseType = null;
                } else {
                    ReleaseType = ReleaseType.pkApprovalID;
                }

                API.procedure.post('ProcUpdateCO', {
                        UserID: $rootScope.user.getId(),
                        CustomerOrderNo: selectCO.OrderID,
                        OrderRef: CustomerOrder.CustomerOrderRef,
                        ReleaseTypeID: ReleaseType,
                        DeliveryPointID: DeliveryPoint,
                        CommerciallyViable: CustomerOrder.CommerciallyViable,
                        AmendmentReviewed: CustomerOrder.AmendmentReviewed
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.validateNewUpdateWO = function(NewWO) {

            if (NewWO === undefined) {
                Notification.show('warning', 'Please enter full information');
                return false;
            }
            if (NewWO.PriceEach === undefined) {
                Notification.show('warning', 'Please enter price each');
                return false;
            }
            if (!regexDecimal7_2.test(NewWO.PriceEach)) {
                Notification.show('warning', 'Price each must be decimal(7,2)');
                return false;
            }
            if (NewWO.DeliveryDate === undefined) {
                Notification.show('warning', 'Please enter Delivery date');
                return false;
            }
            if (!$rootScope.validateDate(NewWO.DeliveryDate, DateFormat)) {
                Notification.show('warning', 'Format of Delivery date is DD/MM/YYYY');
                return false;
            }
            if (NewWO.Reqd === undefined) {
                Notification.show('warning', 'Please enter Quantity Reqd');
                return false;
            }
            if (!regexNumber.test(NewWO.Reqd)) {
                Notification.show('warning', 'Quantity Reqd must be number');
                return false;
            }
            if (NewWO.Build === undefined) {
                Notification.show('warning', 'Please enter Quantity Build');
                return false;
            }
            if (!regexNumber.test(NewWO.Build)) {
                Notification.show('warning', 'Quantity Build must be number');
                return false;
            }
            if (NewWO.ChangeReason === undefined) {
                NewWO.ChangeReason = '';
            }
            if (NewWO.FAIR === undefined) {
                NewWO.FAIR = 0;
            }
            return NewWO;
        };

        $scope.AddNewWO = function(NewWO) {
            var selectedLIWO = _.find($scope.LIWOinfo, function(item) {
                return item.selected !== undefined;
            });
            if (selectedLIWO === undefined) {
                Notification.show('warning', 'Please select LineItem');
                return false;
            }
            var data = $scope.validateNewUpdateWO(NewWO);
            if (data !== false) {

                // Convert DD/MM/YYYY to YYYY/MM/DD
                var convertYYYYMMDD = data.DeliveryDate.split('/');
                convertYYYYMMDD = convertYYYYMMDD[2] + '/' + convertYYYYMMDD[1] + '/' + convertYYYYMMDD[0];

                var LineItem = _.find(selectedLIWO, {
                    'WONo': null
                });
                if (LineItem === undefined) {
                    LineItem = selectedLIWO[0];
                }

                API.procedure.post('ProcAddNewWO', {
                        UserID: $rootScope.user.getId(),
                        LineItemID: LineItem.LineItemID,
                        PriceEach: data.PriceEach,
                        DeliveryDate: convertYYYYMMDD,
                        Reqd: data.Reqd,
                        Build: data.Build,
                        ChangeReason: data.ChangeReason,
                        FAIR: data.FAIR,
                        NewWONo: '@NewWONo'
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.UpdateWO = function(WO) {
            var PriceEach;
            var DeliveryDate;
            var convertYYYYMMDD;
            var Reqd;
            var Build;

            if (!regexDecimal7_2.test(WO.PriceEach)) {
                PriceEach = '';
            } else {
                PriceEach = WO.PriceEach;
            }

            if (!$rootScope.validateDate(WO.DeliveryDate, DateFormat)) {
                DeliveryDate = '';
                convertYYYYMMDD = '';
            } else {
                DeliveryDate = WO.DeliveryDate;

                // Convert DD/MM/YYYY to YYYY/MM/DD
                convertYYYYMMDD = WO.DeliveryDate.split('/');
                convertYYYYMMDD = convertYYYYMMDD[2] + '/' + convertYYYYMMDD[1] + '/' + convertYYYYMMDD[0];
            }

            if (!regexNumber.test(WO.Reqd)) {
                Reqd = '';
            } else {
                Reqd = WO.Reqd;
            }

            if (!regexNumber.test(WO.Build)) {
                Build = '';
            } else {
                Build = WO.Build;
            }

            if (WO.Build >= WO.Reqd) {
                API.procedure.post('ProcUpdateWO', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WO.WONo,
                        PriceEach: PriceEach,
                        DeliveryDate: convertYYYYMMDD,
                        Reqd: Reqd,
                        Build: Build,
                        ChangeReason: WO.ChangeReason,
                        FAIR: WO.FAIR
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                        // Notification.show('warning', 'Numeric value out of range');
                    });
            }
        };

        /**
         * get Part Template List
         */
        API.procedure.get('ProcPartTemplateList')
            .then(function(result) {
                result.forEach(function(value, key) {
                    result[key].DisplayData = value.PartNumber + ' // ' + value.Issue + ' // ' + value.COS;
                });
                $scope.TemplateLists = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.LaunchWO = function(item) {
            API.procedure.post('ProcLaunchWO', {
                    UserID: $rootScope.user.getId(),
                    WorkOrderID: item.WONo,
                })
                .then(function(result) {
                    var regexError = /err/;
                    var patternError = new RegExp(regexError, "i");
                    if (result.length > 0) {
                        if (patternError.test(result[0].MsgResult)) {
                            Notification.show('warning', result[0].MsgResult);
                        } else {
                            Notification.show('success', result[0].MsgResult);
                        }
                    }
                    $scope.getLIFOinfo();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.CancelWO = function(LIWO) {
            var item = _.find(LIWO, {
                'highlight': true
            });
            if (item === undefined) {
                Notification.show('warning', 'Please select WO');
                return false;
            }
            if (item.ChangeReason === undefined | item.ChangeReason === null | item.ChangeReason === '') {
                Notification.show('warning', 'Please enter Change/Close Reason');
                return false;
            }
            API.procedure.post('ProcCancelWO', {
                    UserID: $rootScope.user.getId(),
                    WorkOrderID: item.WONo,
                    ChangeReason: item.ChangeReason
                })
                .then(function(result) {
                    if (parseInt(result[0].Status) !== 1) {
                        Notification.show('warning', 'WO cannot be cancelled/closed');
                        return false;
                    } else {
                        Notification.show('success', 'Cancel WO success');
                        $scope.getLIFOinfo();
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * when user select Lineitem then show dialog
         */
        $scope.CancelLineItem = function(LIWOinfo) {
            var selectedLIWO = _.find(LIWOinfo, {
                'chooseLineItem': true
            });
            if (selectedLIWO === undefined) {
                Notification.show('warning', 'Please select LineItem');
                return false;
            }
            $scope.showDialog = true;
        };

        /**
         * Execute Cancel LineItem
         */
        $scope.ExecCancelLineItem = function(LineItem) {
            var selectedLIWO = _.find($scope.LIWOinfo, {
                'chooseLineItem': true
            });
            if (LineItem === undefined) {
                Notification.show('warning', 'Please enter full information');
                return false;
            }
            if (LineItem.IssueType === undefined) {
                Notification.show('warning', 'Please select issue');
                return false;
            }
            if (LineItem.CancelReason === undefined | LineItem.CancelReason === '') {
                Notification.show('warning', 'Please enter cancel reason');
                return false;
            }

            API.procedure.post('ProcCancelLineItem', {
                    UserID: $rootScope.user.getId(),
                    LineItemID: selectedLIWO[0].LineItemID,
                    IssueType: LineItem.IssueType,
                    CancelReason: LineItem.CancelReason
                })
                .then(function(result) {
                    Notification.show('success', 'Cancel LineItem success');
                    $scope.showDialog = false;
                    $scope.getLIFOinfo();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.addNewLineItem = function(NewLineItem) {
            if ($scope.selectCO === undefined) {
                Notification.show('warning', 'Please select Customer Order');
                return false;
            }
            if (NewLineItem === undefined) {
                Notification.show('warning', 'Please enter full information');
                return false;
            }
            if (NewLineItem.CustRef === undefined) {
                Notification.show('warning', 'Please enter Cust Ref');
                return false;
            }
            if (NewLineItem.TemplateList === undefined) {
                Notification.show('warning', 'Please select Part number');
                return false;
            }

            API.procedure.post('ProcAddNewLineItem', {
                    UserID: $rootScope.user.getId(),
                    OrderID: parseInt($scope.selectCO.OrderID),
                    CustRef: NewLineItem.CustRef,
                    Quoteno: null,
                    PTemplateID: NewLineItem.TemplateList.PTemplateID,
                    ETemplateID: NewLineItem.TemplateList.ETemplateID
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.UpdateLineItem = function(LIWO) {
            if (regexNumber.test(LIWO.CustRef) && regexNumber.test(LIWO.QuoteNo) && LIWO.PTemplateID !== undefined) {
                API.procedure.post('ProcUpdateLineItem', {
                        UserID: $rootScope.user.getId(),
                        LineItemID: LIWO.LineItemID,
                        CustRef: LIWO.CustRef,
                        QuoteNo: LIWO.QuoteNo,
                        PTemplateID: LIWO.PTemplateID.PTemplateID,
                        ETemplateID: LIWO.PTemplateID.ETemplateID
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        $scope.redirectPartControl = function(LIWOinfo) {
            if (_.isNil(LIWOinfo) || LIWOinfo.length === 0) {
                Notification.show('warning', 'Please select Customer & Customer Order');
                return false;
            }
            var selectLineItem = _.find(LIWOinfo, {
                'chooseLineItem': true
            });
            if (_.isNil(selectLineItem)) {
                Notification.show('warning', 'Please select Line Item');
                return false;
            }
            $state.go('department.part-control.part-list', {
                PTemplateID: selectLineItem[0].PTemplateID.PTemplateID,
                ETemplateID: selectLineItem[0].PTemplateID.ETemplateID
            });
        };

        $scope.redirectCustomerCenter = function() {
            $state.go('department.customer.main');
        };

        $scope.beforeUpload = function(file) {
            if ($scope.selectCO === undefined) {
                Notification.show('warning', 'Please select Customer Order');
                return false;
            }
        };

        $scope.uploadSuccess = function(response) {
            API.procedure.post('ProcAddOrderDoc', {
                    UserID: $rootScope.user.getId(),
                    OrderID: parseInt($scope.selectCO.OrderID),
                    FileName: response.data
                })
                .then(function(result) {
                    Notification.show('success', 'Drop Order success');
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        function checkSelectedWO() {
            var hasSelectWO = false;
            if ($scope.LIWOinfo !== undefined && $scope.LIWOinfo.length > 0) {
                _($scope.LIWOinfo).forEach(function(LIWO) {
                    _(LIWO).forEach(function(item) {
                        if (item.selected === true) {
                            hasSelectWO = item;
                        }
                    });
                });
            }
            return hasSelectWO;
        }

        $scope.openBIRT = function() {
            if (_.isNil($stateParams.CustomerOrderId) || $stateParams.CustomerOrderId === '') {
                Notification.show('warning', 'Please select Customer Order');
                return false;
            }
            var params = {
                OrderID: $stateParams.CustomerOrderId
            };
            $rootScope.openTabBirt('Acknowledgement', params);
        };

        $scope.deleteAcknowNote = function(AcknowNotes, AcknowNote) {
            if (_.isNil(AcknowNote) || _.isNil(AcknowNote.AcknowNoteID)) {
                Notification.show('warning', "Does not exists AcknowNoteID");
                return false;
            } else {
                API.procedure.post('ProcDeleteAcknowNote', {
                        AcknowNoteID: AcknowNote.AcknowNoteID
                    })
                    .then(function(response) {
                        _.remove(AcknowNotes, AcknowNote);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

    }
})(angular.module('app.components.department.customer-order', []));