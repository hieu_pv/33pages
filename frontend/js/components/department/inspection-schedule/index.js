require('./inspection/');
require('./main/');

(function(app) {

    app.controller('InspectionScheduleCtrl', InspectionScheduleCtrl);
    InspectionScheduleCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function InspectionScheduleCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {

    }

})(angular.module('app.components.department.inspection-schedule', [
    'app.components.department.inspection-schedule.inspection',
    'app.components.department.inspection-schedule.main'
]));
