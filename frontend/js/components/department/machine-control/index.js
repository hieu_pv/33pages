(function(app) {

    app.controller('MachineControlCtrl', MachineControlCtrl);
    MachineControlCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification', '$window'];

    function MachineControlCtrl($rootScope, $scope, $stateParams, API, Notification, $window) {
        var timeFormat = 'DD/MM/YYYY';

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'Mcn ID', sortBy: 'MachineID' },
            { title: 'Manufacturer', sortBy: 'Manufacturer' },
            { title: 'Model', sortBy: 'Model' },
            { title: 'Start', sortBy: 'ServiceStart' },
            { title: 'End', sortBy: 'ServiceEnd' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        API.procedure.get('ProcGetMachineListDetails')
            .then(function(result) {
                if (result.length > 0) {
                    result.forEach(function(value) {
                        if (value.MachineID !== null && value.MachineID !== "") {
                            value.MachineID = parseFloat(value.MachineID);
                        }
                        value.ServiceStart = $rootScope.convertTime(value.ServiceStart, timeFormat);
                        value.ServiceEnd = $rootScope.convertTime(value.ServiceEnd, timeFormat);
                    });
                    $scope.MachineListDetails = result;
                }
            })
            .catch(function(error) {
                throw error;
            });

        $scope.selectMachine = function(item) {

            /**
             * get Overview data and Tooling table
             */
            API.procedure.post('ProcGetMachineStats', {
                    MachineID: item.MachineID
                })
                .then(function(result) {
                    $scope.MachineStats = result[0];
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Maintenance table
             */
            API.procedure.post('ProcGetMaintenanceStats', {
                    MachineID: item.MachineID
                })
                .then(function(result) {
                    $scope.MaintenanceStats = result[0];
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when change Start Date or End Date
         */
        $scope.AddMachine = function(Machine) {
            var StartDate;
            var EndDate;
            if ($rootScope.validateDate(Machine.ServiceStart, timeFormat) && $rootScope.validateDate(Machine.ServiceEnd, timeFormat)) {
                StartDate = Machine.ServiceStart;

                // Convert DD/MM/YYYY to YYYY/MM/DD
                StartDate = StartDate.split('/');
                StartDate = StartDate[2] + '/' + StartDate[1] + '/' + StartDate[0];

                EndDate = Machine.ServiceEnd;
                EndDate = EndDate.split('/');
                EndDate = EndDate[2] + '/' + EndDate[1] + '/' + EndDate[0];

                API.procedure.post('ProcAddMachineService', {
                        UserID: $rootScope.user.getId(),
                        MachineID: Machine.MachineID,
                        StartDate: StartDate,
                        EndDate: EndDate
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.machine-control', []));
