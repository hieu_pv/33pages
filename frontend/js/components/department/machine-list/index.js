var Machine = require('../../../models/Machine');

(function(app) {

    app.controller('MachineListCtrl', MachineListCtrl);
    MachineListCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$window', '$state', '$stateParams'];

    function MachineListCtrl($rootScope, $scope, API, Notification, $window, $state, $stateParams) {
        var MachineID = $stateParams.MachineID;
        var MachineName = $stateParams.MachineName;
        var MachineType = $scope.MachineType = $stateParams.MachineType;

        $scope.MachineParams = new Machine();

        // Use for show/hide icon
        $scope.isIcon = false;

        if (!_.isNil(MachineID) && MachineID !== "") {
            $scope.isIcon = true;
        }

        if (!_.isNil(MachineName) && MachineName !== "") {
            $scope.isIcon = true;
        }

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'MID', sortBy: 'MachineID' },
            { title: 'Manufacturer', sortBy: 'Manufacturer' },
            { title: 'Model', sortBy: 'Model' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        /**
         * get MachineListDetails
         */
        API.procedure.get('ProcGetMachineMasterList')
            .then(function(result) {
                $scope.MachineMasterList = [];
                if (result.length > 0) {
                    var selectedMachine;
                    if (!_.isUndefined(MachineType) && MachineType !== "" && MachineType !== 'ALL') {
                        result.forEach(function(value) {
                            if (parseInt(value[MachineType]) === 1) {
                                $scope.MachineMasterList.push(value);
                            }
                            if (value.MachineID !== null && value.MachineID !== "") {
                                value.MachineID = parseFloat(value.MachineID);
                            }
                        });
                    } else {
                        result.forEach(function(value) {
                            if (value.MachineID !== null && value.MachineID !== "") {
                                value.MachineID = parseFloat(value.MachineID);
                            }
                        });
                        $scope.MachineMasterList = result;
                    }
                    if (MachineID !== undefined) {
                        selectedMachine = _.find($scope.MachineMasterList, function(item) {
                            return parseInt(item.MachineID) === parseInt(MachineID);
                        });
                        if (selectedMachine !== undefined) {
                            selectedMachine.selected = true;
                        }
                    }
                    if (MachineName !== undefined) {
                        selectedMachine = _.find($scope.MachineMasterList, function(item) {
                            return item.Manufacturer + " " + item.Model === MachineName;
                        });
                        if (selectedMachine !== undefined) {
                            selectedMachine.selected = true;
                        }
                    }
                }
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * Highlight Machine
         */
        $scope.selectMachine = function(MachineItem) {
            $scope.MachineID = MachineItem.MachineID;
            var state = 'department.machine-list';
            var params = {
                MachineID: $scope.MachineID,
                MachineName: undefined
            };
            $rootScope.redirectDept(state, params);
        };
        if (MachineID !== undefined) {
            getMachineInfo(MachineID);
        }

        function getMachineInfo(MachineID) {

            /**
             * get Machine Params
             */
            API.procedure.post('ProcGetMachineParams', {
                    MachineID: MachineID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.MachineParams = new Machine(result[0]);
                        var pattern = /^\d+$/;
                        for (var k in $scope.MachineParams) {
                            if (pattern.test($scope.MachineParams[k])) {
                                $scope.MachineParams[k] = parseInt($scope.MachineParams[k]);
                            }
                        }
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * Filter Machine
         */
        $scope.filterMachine = function(MachineType) {
            var state = 'department.machine-list';
            var params = {
                MachineID: undefined,
                MachineName: undefined,
                MachineType: MachineType
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.selectTrn = function() {
            if ($scope.MachineParams === undefined) {
                Notification.show('warning', 'Please select Machine');
                return false;
            }
            $scope.isTrn = !$scope.isTrn;
            if ($scope.isTrn === true) {
                $scope.MachineParams.setTrn(1);
            } else {
                $scope.MachineParams.setTrn(0);
            }
        };

        $scope.selectGrd = function() {
            if ($scope.MachineParams === undefined) {
                Notification.show('warning', 'Please select Machine');
                return false;
            }
            $scope.isGrd = !$scope.isGrd;
            if ($scope.isGrd === true) {
                $scope.MachineParams.setGrd(1);
            } else {
                $scope.MachineParams.setGrd(0);
            }
        };

        $scope.selectMil = function() {
            if ($scope.MachineParams === undefined) {
                Notification.show('warning', 'Please select Machine');
                return false;
            }
            $scope.isMil = !$scope.isMil;
            if ($scope.isMil === true) {
                $scope.MachineParams.setMil(1);
            } else {
                $scope.MachineParams.setMil(0);
            }
        };

        $scope.selectGC = function() {
            if ($scope.MachineParams === undefined) {
                Notification.show('warning', 'Please select Machine');
                return false;
            }
            $scope.isGC = !$scope.isGC;
            if ($scope.isGC === true) {
                $scope.MachineParams.setGC(1);
            } else {
                $scope.MachineParams.setGC(0);
            }
        };

        $scope.selectoth = function() {
            if ($scope.MachineParams === undefined) {
                Notification.show('warning', 'Please select Machine');
                return false;
            }
            $scope.isoth = !$scope.isoth;
            if ($scope.isoth === true) {
                $scope.MachineParams.setoth(1);
            } else {
                $scope.MachineParams.setoth(0);
            }
        };

        $scope.AddNew = function() {
            $scope.MachineParams = new Machine();
            $scope.MachineID = undefined;
            $scope.isIcon = true;
        };

        $scope.AddOrUpdateMachine = function(MachineItem) {
            var hasManufacturer = true;
            var hasModel = true;
            /**
             * Add: MachineID = undefined 
             * Update: MachineID != null
             */
            if (_.isUndefined(MachineID)) {
                if (_.isUndefined(MachineItem.getManufacturer()) || MachineItem.getManufacturer() === '') {
                    hasManufacturer = false;
                }
                if (_.isUndefined(MachineItem.getModel()) || MachineItem.getModel() === '') {
                    hasModel = false;
                }
                if (!hasManufacturer && !hasModel) {
                    Notification.show('warning', 'Please enter Make or Model');
                    return false;
                }
                API.procedure.post('ProcAddNewMachine', {
                        Manufacturer: MachineItem.getManufacturer(),
                        Model: MachineItem.getModel(),
                        Trn: MachineItem.getTrn(),
                        Grd: MachineItem.getGrd(),
                        Mil: MachineItem.getMil(),
                        GC: MachineItem.getGC(),
                        Oth: MachineItem.getoth(),
                        Offset: MachineItem.getOffset(),
                        StationNo: MachineItem.getStationNo(),
                        MinStickOut: MachineItem.getMinStickOut(),
                        Designation: MachineItem.getDesignation(),
                        HolderNo: MachineItem.getHolderNo(),
                        Diameter: MachineItem.getDiameter(),
                        InsertDatumPoint: MachineItem.getInsertDatumPoint(),
                        MinfluteLength: MachineItem.getMinfluteLength(),
                        Spindle: MachineItem.getSpindle(),
                        DifficultyLevel: MachineItem.getDifficultyLevel(),
                        CoolantType: MachineItem.getCoolantType(),
                        TargetConcMin: MachineItem.getTargetConcMin(),
                        TargetConcMax: MachineItem.getTargetConcMax(),
                        SlideOilLevel: MachineItem.getSlideOilLevel(),
                        CoolantLevel: MachineItem.getCoolantLevel(),
                        GreaseLevel: MachineItem.getGreaseLevel(),
                        Fanfilter: MachineItem.getFanfilter(),
                        OilFilter: MachineItem.getOilFilter(),
                        CoolantFilter: MachineItem.getCoolantFilter(),
                        AirSystem: MachineItem.getAirSystem(),
                        IntegralLighting: MachineItem.getIntegralLighting(),
                        InterlocksEngaged: MachineItem.getInterlocksEngaged()
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                API.procedure.post('ProcUpdateMachineParams', {
                        MachineID: MachineID,
                        Manufacturer: MachineItem.getManufacturer(),
                        Model: MachineItem.getModel(),
                        Trn: MachineItem.getTrn(),
                        Grd: MachineItem.getGrd(),
                        Mil: MachineItem.getMil(),
                        GC: MachineItem.getGC(),
                        Oth: MachineItem.getoth(),
                        Offset: MachineItem.getOffset(),
                        StationNo: MachineItem.getStationNo(),
                        MinStickOut: MachineItem.getMinStickOut(),
                        Designation: MachineItem.getDesignation(),
                        HolderNo: MachineItem.getHolderNo(),
                        Diameter: MachineItem.getDiameter(),
                        InsertDatumPoint: MachineItem.getInsertDatumPoint(),
                        MinfluteLength: MachineItem.getMinfluteLength(),
                        Spindle: MachineItem.getSpindle(),
                        DifficultyLevel: MachineItem.getDifficultyLevel(),
                        CoolantType: MachineItem.getCoolantType(),
                        TargetConcMin: MachineItem.getTargetConcMin(),
                        TargetConcMax: MachineItem.getTargetConcMax(),
                        SlideOilLevel: MachineItem.getSlideOilLevel(),
                        CoolantLevel: MachineItem.getCoolantLevel(),
                        GreaseLevel: MachineItem.getGreaseLevel(),
                        Fanfilter: MachineItem.getFanfilter(),
                        OilFilter: MachineItem.getOilFilter(),
                        CoolantFilter: MachineItem.getCoolantFilter(),
                        AirSystem: MachineItem.getAirSystem(),
                        IntegralLighting: MachineItem.getIntegralLighting(),
                        InterlocksEngaged: MachineItem.getInterlocksEngaged()
                    })
                    .then(function(result) {
                        Notification.show('success', 'Update success');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.RemoveMachine = function(MachineListDetails) {
            if (MachineListDetails.length > 0) {
                var selectedMachine = _.filter(MachineListDetails, _.matches({ 'selected': true }));
                if (selectedMachine.length > 0) {
                    selectedMachine.forEach(function(value) {

                        API.procedure.post('ProcRemoveMachine', {
                                MachineID: value.MachineID
                            })
                            .then(function(result) {
                                _.remove(MachineListDetails, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    Notification.show('warning', 'Please select Machine');
                    return false;
                }
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.machine-list', []));
