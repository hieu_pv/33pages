require('./part-list/');
require('./assemblies/');
require('./notes/');
require('./review-tasks/');
require('./docs/');

(function(app) {

    app.controller('PartControlCtrl', PartControlCtrl);
    PartControlCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function PartControlCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {
        var PTemplateID = $scope.PTemplateID = $stateParams.PTemplateID;

        // Use to selected Part Filter
        $scope.PartFilter = 2;

        API.procedure.get('ProcGetAllApprovalList')
            .then(function(result) {
                $scope.AllApprovalList = result;
            })
            .catch(function(error) {
                throw error;
            });

        API.procedure.get('ProcPartTemplateList')
            .then(function(result) {
                result.forEach(function(value, key) {
                    result[key].showData = value.PartNumber + ' // ' + value.Issue + ' // ' + value.COS;
                });
                $scope.PartTemplateList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get COS list
         */
        API.procedure.get('ProcGetSupplyConditionList')
            .then(function(result) {
                $scope.SupplyConditionList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * this function is call after user selected Part number
         */
        $scope.PartNoSelected = function(item, model) {
            console.log('item ', item);
            var ETemplateID = $scope.ETemplateID = item.ETemplateID;
            $scope.getInfoPart(ETemplateID);
            $state.go('department.part-control.part-list', {
                PTemplateID: item.PTemplateID,
                ETemplateID: item.ETemplateID
            });
        };

        $scope.getInfoPart = function(ETemplateID) {
            API.procedure.post('ProcGetPartDetail', {
                    ETemplateID: ETemplateID
                })
                .then(function(result) {

                    // Use to show/hide Alert & Lock button
                    $scope.isAlert = true;
                    $scope.isLocked = true;
                    $scope.PartDetail = result;
                    if (result.length > 0) {
                        result.forEach(function(value) {

                            // Check show/hide Alert & Lock button
                            if (parseInt(result[0].Alert) === 1) {
                                $scope.isAlert = false;
                            } else {
                                $scope.isAlert = true;
                            }
                            if (parseInt(result[0].Locked) === 1) {
                                $scope.isLocked = false;
                            } else {
                                $scope.isLocked = true;
                            }
                            $scope.PartDetail = result;

                            // Selected Release type
                            if ($scope.AllApprovalList.length > 0) {
                                $scope.AllApprovalList.forEach(function(value) {
                                    if (value.pkApprovalID === $scope.PartDetail[0].ReleaseTypeID) {
                                        $scope.PartDetail[0].ReleaseTypeID = value;
                                    }
                                });
                            }

                            // Selected COS
                            if ($scope.SupplyConditionList.length > 0) {
                                $scope.SupplyConditionList.forEach(function(value) {
                                    if (value.SupplyConditionID === $scope.PartDetail[0].COSID) {
                                        $scope.PartDetail[0].COSID = value;
                                    }
                                });
                            }
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

    }

})(angular.module('app.components.department.part-control', [
    'app.components.department.part-control.part-list',
    'app.components.department.part-control.assemblies',
    'app.components.department.part-control.notes',
    'app.components.department.part-control.part-review-tasks',
    'app.components.department.part-control.part-docs'
]));
