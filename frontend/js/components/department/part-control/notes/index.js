(function(app) {

    app.controller('PartNotesCtrl', PartNotesCtrl);
    PartNotesCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function PartNotesCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {
        var PTemplateID = $stateParams.PTemplateID;

        if (!_.isNil(PTemplateID) && PTemplateID !== '') {

            API.procedure.post('ProcGetPartTemplateNotes', {
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    $scope.PartTemplateNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.post('ProcGetPartTemplateRemarks', {
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    $scope.PartTemplateRemarks = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.AddNote = function(Note) {
                if (_.isNil(Note) || Note === '') {
                    Notification.show('warning', 'Please enter Note');
                    return false;
                }

                API.procedure.post('ProcAddTemplateNote', {
                        UserID: $rootScope.user.getId(),
                        PTemplateID: PTemplateID,
                        PartNote: Note
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.AddRemark = function(Remark) {
                if (_.isNil(Remark) || Remark === '') {
                    Notification.show('warning', 'Please enter Note');
                    return false;
                }

                API.procedure.post('ProcAddTemplateRemark', {
                        UserID: $rootScope.user.getId(),
                        PTemplateID: PTemplateID,
                        PartRemark: Remark
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            $state.go('department.part-control.part-list');
        }

    }

})(angular.module('app.components.department.part-control.notes', []));
