var Template = require('../../../../models/Template');

(function(app) {

    app.controller('PartDocsCtrl', PartDocsCtrl);
    PartDocsCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function PartDocsCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var PTemplateID = $stateParams.PTemplateID;
        $scope.TemplateInfo = new Template('');

        $scope.getPartDocs = getPartDocs;
        $scope.getDocumentDropTypes = getDocumentDropTypes;
        $scope.getAllApprovalList = getAllApprovalList;
        $scope.partNumberList = partNumberList;
        $scope.changePartNo = changePartNo;
        $scope.changePartIssue = changePartIssue;
        $scope.AddTemplate = AddTemplate;
        $scope.beforeUpload = beforeUpload;
        $scope.uploadSuccess = uploadSuccess;
        $scope.ViewFileName = ViewFileName;
        $scope.docAutoPrint = docAutoPrint;

        if (!_.isNil(PTemplateID) && PTemplateID !== '') {

            /**
             * get data of table
             */
            getPartDocs(PTemplateID);
        }

        function getPartDocs(PTemplateID) {
            API.procedure.post('ProcGetPartDocs', {
                    PTemplateID: PTemplateID,
                })
                .then(function(result) {
                    $scope.PartDocs = _.map(result, function(item) {
                        if (!_.isNil(item.AutoPrint)) {
                            item.AutoPrint = parseInt(item.AutoPrint);
                        }
                        return item;
                    });
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * get document drop-down
         */
        function getDocumentDropTypes() {
            API.procedure.get('ProcGetDocumentDropTypes')
                .then(function(result) {
                    $scope.DocumentDropTypes = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }
        getDocumentDropTypes();

        /**
         * get Approval list
         */
        function getAllApprovalList() {
            API.procedure.get('ProcGetAllApprovalList')
                .then(function(result) {
                    $scope.ApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }
        getAllApprovalList();

        /**
         * get Master Part No list
         */
        function partNumberList() {
            API.procedure.get('ProcPartNumberList')
                .then(function(result) {
                    $scope.NumberList = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }
        partNumberList();

        function changePartNo(PartNo) {
            $scope.TemplateInfo.DrawingNumber = PartNo;
        }

        function changePartIssue(Issue) {
            $scope.TemplateInfo.Issue = Issue;
        }

        function AddTemplate(Template) {
            API.procedure.post('ProcAddNewPartTemplate', {
                    UserID: $rootScope.user.getId(),
                    PartNumber: Template.getPartNumber(),
                    PartIssue: Template.getPartIssue(),
                    Description: Template.getDescription(),
                    LeadDays: Template.getLeadDays(),
                    Price: Template.getPrice(),
                    DrawingNumber: Template.getDrawingNumber(),
                    Issue: Template.getIssue(),
                    ApprovalID: Template.getApprovalID(),
                    AssemblyQty: Template.getAssemblyQty(),
                    PartTemplateID: Template.getPartTemplateID()
                })
                .then(function(result) {
                    PTemplateID = result[0].Last_Insert_ID;
                    Notification.show('success', 'Add New Part Template success');
                })
                .catch(function(error) {
                    // Notification.show('warning', 'Drawing Number and Issue must match the Reference and IssueNumber fields of a drawing already in the Drawing table');
                });
        }

        function beforeUpload(file) {
            if (_.isNil($scope.AddPartDoc)) {
                Notification.show('warning', 'Please select Document Drop type');
                return false;
            }
            if (_.isNil($scope.AddPartDoc.DocumentDropType)) {
                Notification.show('warning', 'Please select Document Drop type');
                return false;
            } else {
                if (parseInt($scope.AddPartDoc.DocumentDropType.pkDocumentDropTypeID) === 4) {
                    if (_.isNil($scope.AddPartDoc.Reference) || $scope.AddPartDoc.Reference === '') {
                        Notification.show('warning', 'Please enter Reference');
                        return false;
                    }
                    if (_.isNil($scope.AddPartDoc.Issue) || $scope.AddPartDoc.Issue === '') {
                        Notification.show('warning', 'Please enter Issue');
                        return false;
                    }
                } else {
                    $scope.AddPartDoc.Reference = $scope.AddPartDoc.Issue = "";
                }
            }
            return true;
        }

        function uploadSuccess(response) {
            if (_.isNil(PTemplateID) || PTemplateID === '') {
                Notification.show('warning', 'Please select a part');
                return false;
            }
            API.procedure.post('ProcAddPartDoc', {
                    PTemplateID: PTemplateID,
                    UserID: $rootScope.user.getId(),
                    DocTypeID: $scope.AddPartDoc.DocumentDropType.pkDocumentDropTypeID,
                    FileName: response.data,
                    Reference: $scope.AddPartDoc.Reference,
                    Issue: $scope.AddPartDoc.Issue
                })
                .then(function(result) {
                    Notification.show('success', 'Drop file success.');
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function ViewFileName() {
            var selectedPartDocs = _.find($scope.PartDocs, {
                'selected': true
            });
            if (_.isNil(selectedPartDocs)) {
                Notification.show('warning', 'Please select an item to view');
                return false;
            }
            var FileName = $rootScope.PathImageUpload + selectedPartDocs.FileName;
            $window.open(FileName, 'blank_');
        }

        function docAutoPrint() {
            var selectedPartDocs = _.find($scope.PartDocs, function(item) {
                return item.selected === true;
            });
            if (!_.isNil(selectedPartDocs)) {
                API.procedure.post('ProcDocAutoPrint', {
                        FileName: selectedPartDocs.FileName,
                        AutoPrint: selectedPartDocs.AutoPrint
                    })
                    .then(function(response) {
                        if (selectedPartDocs.AutoPrint === 0) {
                            Notification.show('success', 'Disable auto print success');
                        } else {
                            Notification.show('success', 'Enable auto print success');
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                Notification.show('warning', 'Please select PartDoc item');
                return false;
            }
        }
    }

})(angular.module('app.components.department.part-control.part-docs', []));
