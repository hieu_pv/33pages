(function(app) {

    app.controller('PartHistoryCtrl', PartHistoryCtrl);
    PartHistoryCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function PartHistoryCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var PTemplateID = $scope.PTemplateID = $stateParams.PTemplateID;
        var PartNumber = $scope.PartNumber = $stateParams.PartNumber;
        var COS = $scope.COS = $stateParams.COS;
        var Issue = $scope.Issue = $stateParams.Issue;

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'Customer', sortBy: 'CustomerName' },
            { title: 'Work Order', sortBy: 'WONo' },
            { title: 'Order date', sortBy: 'OrderDate' },
            { title: 'Completed', sortBy: 'Completed' },
            { title: 'Qty', sortBy: 'Qty' },
            { title: 'Each', sortBy: 'PriceEach' },
            { title: 'Value', sortBy: 'Value' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        API.procedure.post('ProcGetPartWOList', {
                PTemplateID: PTemplateID
            })
            .then(function(result) {
                if (result.length > 0) {

                    // Show info Times made
                    $scope.TimesMade = result.length;

                    // Show info Grand total
                    $scope.GrandTotal = 0;
                    result.forEach(function(value) {
                        if (!_.isNil(value.Qty) && value.Qty !== "") {
                            value.Qty = parseFloat(value.Qty);
                        }
                        if (!_.isNil(value.PriceEach) && value.PriceEach !== "") {
                            value.PriceEach = parseFloat(value.PriceEach);
                        }
                        if (value.Value !== null && value.Value !== "") {
                            value.Value = parseFloat(value.Value);
                        }

                        if (!_.isNil(value.Value)) {
                            $scope.GrandTotal += parseFloat(value.Value);
                        }
                    });
                    $scope.PartWOList = result;
                }
            })
            .catch(function(error) {
                throw error;
            });

        $scope.PartWOSelected = function(PartWO) {
            $scope.PartWO = PartWO;
        };

        $scope.redirectWorkOrder = function() {
            if ($scope.PartWO === undefined) {
                Notification.show('warning', 'Please select WorkOrder');
                return false;
            }
            var state = 'department.workorders.main';
            var params = {
                WorkOrderID: $scope.PartWO.WONo
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.part-history', []));
