(function(app) {
    app.controller('DeliveryPointCtrl', DeliveryPointCtrl);

    DeliveryPointCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', '$window'];

    function DeliveryPointCtrl($rootScope, $scope, API, $state, $stateParams, Notification, $window) {
        var customer_id = $stateParams.customer_id;
        API.procedure.post('ProcGetCustDeliveryPoints', {
                CustomerID: customer_id,
            })
            .then(function(result) {
                var DeliveryPoints = [];
                result.forEach(function(value, key) {
                    if (DeliveryPoints[value.DPID] === undefined) {
                        value.contact = [{
                            ContactName: value.ContactName,
                            Phone: value.Phone,
                            DCID: value.DCID,
                        }];
                        value = _.omit(value, ['ContactName', 'Phone', 'DCID']);
                        DeliveryPoints[value.DPID] = value;
                    } else {
                        DeliveryPoints[value.DPID].contact.push({
                            ContactName: value.ContactName,
                            Phone: value.Phone,
                            DCID: value.DCID,
                        });
                    }
                });
                var data = [];
                DeliveryPoints.forEach(function(value) {
                    data.push(value);
                });
                $scope.DeliveryPoints = data;
            })
            .catch(function(error) {
                throw (error);
            });

        /*
         * add delivery point contact
         */
        $scope.AddCustomerDeliveryContact = function(DeliveryPointContact, DPID) {
            if (DeliveryPointContact === undefined) {
                Notification.show('warning', 'Please enter contact info');
                return false;
            }
            if (DeliveryPointContact.ContactName === undefined || DeliveryPointContact.ContactName === '') {
                Notification.show('warning', 'Please enter contact name');
                return false;
            }
            if (DeliveryPointContact.Phone === undefined || DeliveryPointContact.Phone === '') {
                Notification.show('warning', 'Please enter contact phone');
                return false;
            }
            var reg = /^[0-9_ ]*$/;
            if (reg.test(DeliveryPointContact.Phone)) {
                DeliveryPointContact.DPID = DPID;
                DeliveryPointContact.UserID = $rootScope.user.getId();
                API.procedure.post('ProcAddDeliveryPointContact', DeliveryPointContact)
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                Notification.show('warning', 'Phone format must be number');
                return false;
            }
        };
        /*
         * delete delivery point contact
         */
        $scope.RmvCustomerDeliveryContact = function(contact) {
            contact.forEach(function(value, key) {
                if (value.selected !== undefined && value.selected === true) {
                    API.procedure.post('ProcRmvDeliveryPointContact', {
                            UserID: $rootScope.user.getId(),
                            DCID: value.DCID
                        })
                        .then(function(result) {
                            _.remove(contact, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };

        /**
         * add Customer Delivery Point
         */
        $scope.AddCustomerDeliveryPoint = function(DeliveryPoint) {
            if (DeliveryPoint === undefined) {
                Notification.show('warning', 'Please enter the address');
                return false;
            }
            if (DeliveryPoint.Address1 === undefined || DeliveryPoint.Address1 === '') {
                Notification.show('warning', 'Please enter line 1');
                return false;
            }
            if (DeliveryPoint.Address2 === undefined || DeliveryPoint.Address2 === '') {
                DeliveryPoint.Address2 = '';
            }
            if (DeliveryPoint.Address3 === undefined || DeliveryPoint.Address3 === '') {
                DeliveryPoint.Address3 = '';
            }
            if (DeliveryPoint.Address4 === undefined || DeliveryPoint.Address4 === '') {
                DeliveryPoint.Address4 = '';
            }
            if (DeliveryPoint.Postcode === undefined || DeliveryPoint.Postcode === '') {
                DeliveryPoint.Postcode = '';
            }
            API.procedure.post('ProcAddCustDeliveryPoint', {
                    UserID: $rootScope.user.getId(),
                    CustomerID: customer_id,
                    Address1: DeliveryPoint.Address1,
                    Address2: DeliveryPoint.Address2,
                    Address3: DeliveryPoint.Address3,
                    Address4: DeliveryPoint.Address4,
                    Postcode: DeliveryPoint.Postcode
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * update Customer Delivery Point
         */
        $scope.UpdateCustDeliveryPoint = function(id, DeliveryPoint) {
            API.procedure.post('ProcUpdateCustDeliveryPoint', {
                    UserID: $rootScope.user.getId(),
                    DPID: id,
                    Address1: DeliveryPoint.Address1,
                    Address2: DeliveryPoint.Address2,
                    Address3: DeliveryPoint.Address3,
                    Address4: DeliveryPoint.Address4,
                    Postcode: DeliveryPoint.Postcode
                })
                .then(function(result) {
                    Notification.show('success', 'Update success!');
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * remove Customer Delivery Point
         */
        $scope.RemoveCustomerDeliveryPoint = function(DeliveryPoints) {
            var hasSelect = false;
            if (Array.isArray(DeliveryPoints)) {
                DeliveryPoints.forEach(function(value, key) {
                    if (value.selected !== undefined && value.selected === true) {
                        hasSelect = true;
                        API.procedure.post('ProcRmvCustDeliveryPoint', {
                                UserID: $rootScope.user.getId(),
                                DeliveryPointID: value.DPID
                            })
                            .then(function(result) {
                                _.remove(DeliveryPoints, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    } 
                });
            }
            if (hasSelect === false) {
                Notification.show('warning', 'Please select address to delete');
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }
})(angular.module('app.components.customer.delivery-points', []));
