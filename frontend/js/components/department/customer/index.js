require('./main/');
require('./info/');
require('./contact/');
require('./order-history/');
require('./delivery-points/');
var Customer = require('../../../models/Customer.js');
(function(app) {

    app.controller('CustomerCtrl', CustomerCtrl);

    CustomerCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification', '$window'];

    function CustomerCtrl($rootScope, $scope, $stateParams, API, $state, Notification, $window) {
        var customer_id = $stateParams.customer_id;

        $scope.resetStatusButton = resetStatusButton;
        $scope.clearInfoCustomer = clearInfoCustomer;
        $scope.getCustomerList = getCustomerList;
        $scope.getAllApprovalList = getAllApprovalList;
        $scope.addCustomer = addCustomer;
        $scope.cancelAddCustomer = cancelAddCustomer;
        $scope.redirectNewCO = redirectNewCO;
        $scope.redirectOrderHistoryPage = redirectOrderHistoryPage;
        $scope.redirectCustomerContactPage = redirectCustomerContactPage;
        $scope.redirectDeliveryPointsPage = redirectDeliveryPointsPage;
        $scope.exportInvoices = exportInvoices;
        $scope.redirectBack = redirectBack;
        $scope.copyAddress = copyAddress;
        $scope.UserSelected = UserSelected;

        function resetStatusButton() {

            /**
             * Use to show/hide Save, Update, Cancel button
             */
            $scope.showUpdateButton = false;
            $scope.showSaveCancelButton = false;
        }

        function clearInfoCustomer() {

            /**
             * Clear information of Customer
             */
            $scope.CustomerInfo = null;
            $scope.PaymentTerm = null;
            $scope.PaymentRunFrequency = null;
            $scope.fkApprovalID = null;
        }
        resetStatusButton();
        clearInfoCustomer();

        /**
         * Use to save old value of Customer when User click Cancel button
         */
        var save_customer;

        $scope.ListPaymentTerms = [{
            value: 1,
            name: '30 days'
        }, {
            value: 2,
            name: '60 days'
        }, {
            value: 3,
            name: '90 days'
        }];


        /**
         * List select Payment Run Frequency
         */
        $scope.ListPaymentRunFrequency = [{
            value: 1,
            name: 'weekly'
        }, {
            value: 2,
            name: 'bi-monthly'
        }, {
            value: 3,
            name: 'monthly'
        }];

        /**
         * get All Customer
         */
        function getCustomerList() {
            API.procedure.get('ProcCustomerList')
                .then(function(result) {
                    $scope.CustomerList = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }
        getCustomerList();

        /**
         * List AllApproval
         */
        function getAllApprovalList() {
            API.procedure.get('ProcGetAllApprovalList')
                .then(function(result) {
                    $scope.AllApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }
        getAllApprovalList();

        /**
         * Add Customer
         */
        function addCustomer() {
            $scope.showUpdateButton = false;
            $scope.showSaveCancelButton = true;
            clearInfoCustomer();
            $scope.CustomerInfo = {};
        }

        /**
         * Cancel Add Customer
         */
        function cancelAddCustomer() {
            if (_.isNil($scope.CustomerInfo)) {
                $scope.showUpdateButton = false;
                $scope.showSaveCancelButton = false;
            } else {
                $scope.showUpdateButton = true;
                $scope.showSaveCancelButton = false;
            }
        }

        /**
         * redirect Customer Order Page
         */
        function redirectNewCO() {
            var state = 'department.customer-order';
            $rootScope.redirectDept(state);
        }

        /**
         * redirect Order History Page
         */
        function redirectOrderHistoryPage() {
            if (_.isNil($scope.CustomerInfo)) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var state = 'department.customer.order-history';
            var params = {
                customer_id: $scope.CustomerInfo.pkCustomerID
            };
            $rootScope.redirectDept(state, params);
        }

        /**
         * redirect Customer Contact Page
         */
        function redirectCustomerContactPage() {
            if (_.isNil($scope.CustomerInfo)) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var state = 'department.customer.contact';
            var params = {
                customer_id: $scope.CustomerInfo.pkCustomerID
            };
            $rootScope.redirectDept(state, params);
        }

        /**
         * redirect Delivery Points Page
         */
        function redirectDeliveryPointsPage() {
            if (_.isNil($scope.CustomerInfo)) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var state = 'department.customer.delivery-points';
            var params = {
                customer_id: $scope.CustomerInfo.pkCustomerID
            };
            $rootScope.redirectDept(state, params);
        }

        function exportInvoices() {
            API.global.post('exportExcel', {
                    userName: $rootScope.user.getUsername(),
                    procedureName: 'ProcSageExport',
                    title: 'Export Customer'
                })
                .then(function(result) {
                    var form = $('<form method="GET" action="/' + result + '">');
                    $('body').append(form);
                    form.submit();
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function redirectBack() {
            $window.history.back();
        }

        /**
         * copy Address
         */
        function copyAddress() {
            $scope.CustomerInfo.InvAddress1 = $scope.CustomerInfo.Address1;
            $scope.CustomerInfo.InvAddress2 = $scope.CustomerInfo.Address2;
            $scope.CustomerInfo.InvAddress3 = $scope.CustomerInfo.Address3;
            $scope.CustomerInfo.InvAddress4 = $scope.CustomerInfo.Address4;
            $scope.CustomerInfo.InvPostcode = $scope.CustomerInfo.Postcode;
        }

        function UserSelected(data) {
            $scope.showUpdateButton = true;
            $scope.showSaveCancelButton = false;

            customer_id = data.pkCustomerID;
            $scope.getCustomerInfo(customer_id);
            $state.go('department.customer.info', {
                customer_id: customer_id
            });
        }

        $scope.getCustomerInfo = function(customer_id) {
            API.procedure.post('ProcGetCustomerDetail', {
                    CustomerID: customer_id
                })
                .then(function(result) {
                    if (result.length === 0) {
                        $state.go('department.home');
                    }
                    $scope.CustomerInfo = new Customer(result[0]);
                    for (var key in $scope.CustomerInfo) {
                        var numberRegex = /^\d+$/;
                        if (numberRegex.test($scope.CustomerInfo[key])) {
                            $scope.CustomerInfo[key] = parseFloat($scope.CustomerInfo[key]);
                        }
                    }
                    save_customer = $scope.CustomerInfo;

                    $scope.PaymentTerm = _.find($scope.ListPaymentTerms, function(item) {
                        return parseInt(item.value) === parseInt($scope.CustomerInfo.getPaymentTerms());
                    });
                    $scope.PaymentRunFrequency = _.find($scope.ListPaymentRunFrequency, function(item) {
                        return parseInt(item.value) === parseInt($scope.CustomerInfo.getPaymentRunFrequency());
                    });

                    var days = [
                        { key: '1', name: 'Monday' },
                        { key: '2', name: 'Tuesday' },
                        { key: '3', name: 'Wednesday' },
                        { key: '4', name: 'Thursday' },
                        { key: '5', name: 'Friday' },
                        { key: '6', name: 'Staturday' },
                        { key: '7', name: 'Sunday' },
                    ];
                    $scope.days = days;

                    $scope.fkApprovalID = _.find($scope.AllApprovalList, function(item) {
                        return parseInt(item.pkApprovalID) === parseInt($scope.CustomerInfo.getFkApprovalID());
                    });

                    API.procedure.post('ProcGetOpeningHours', {
                            CustomerID: customer_id,
                        })
                        .then(function(result) {
                            $scope.CustomerInfo = $scope.CustomerInfo.setOptions(result[0]);
                        })
                        .catch(function(error) {
                            throw (error);
                        });

                    API.procedure.post('ProcGetCustomerDetail', {
                            CustomerID: customer_id
                        })
                        .then(function(result) {
                            $scope.CustomerInfo = $scope.CustomerInfo.setOptions(result[0]);
                        })
                        .catch(function(error) {
                            throw (error);
                        });
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.updateCustomer = function(Customer) {
            var data = {};
            for (var key in Customer) {
                var match = key.match(/^Day\d(Close|Open)$/gi);
                if (match !== null) {
                    if (Customer[key] === 'close' || Customer[key] === undefined) {
                        data[key] = '';
                    }
                    var regex = /^\d{4}$/;
                    if (!regex.test(Customer[key])) {
                        data[key] = '000000';
                    } else {
                        data[key] = Customer[key] + "00";
                    }
                }
            }
            var numberRegex = /^0|[1-9][0-9]*$/;
            var phoneRegex = /^[0-9_ ()]*$/g;
            var facsimileRegex = /^[0-9_ ()]*$/g;
            if (Customer.getFacsimile() !== '') {
                if (!facsimileRegex.test(Customer.getFacsimile())) {
                    Notification.show('warning', 'Facsimile format is invalid');
                    return false;
                }
            }

            if (Customer.getPhone() !== '') {
                if (!phoneRegex.test(Customer.getPhone())) {
                    Notification.show('warning', 'Phone format is invalid');
                    return false;
                }
            }
            if (Customer.getDeliveryCharge() === undefined || !numberRegex.test(Customer.getDeliveryCharge())) {
                Customer.DeliveryCharge = 0;
            }
            if ((Customer.getMarkUpQuote() === undefined) || !numberRegex.test(Customer.getMarkUpQuote())) {
                Customer.MarkUpQuote = 0;
            }
            if (Customer.getMarkUpQuote() > 255) {
                Notification.show('warning', 'Quote markup not be larger 255');
                return false;
            }

            API.procedure.post('ProcUpdateCustomerDetail', {
                    UserID: Customer.getCustomerId(),
                    CustomerID: customer_id,
                    CustomerSageCode: Customer.getFinanceCode(),
                    CustomerName: Customer.getCustomerName(),
                    Alias: Customer.getAlias(),
                    Address1: Customer.getAddress1(),
                    Address2: Customer.getAddress2(),
                    Address3: Customer.getAddress3(),
                    Address4: Customer.getAddress4(),
                    Postcode: Customer.getPostcode(),
                    Phone: Customer.getPhone(),
                    Facsimile: Customer.getFacsimile(),
                    Website: Customer.getWebsite(),
                    Email: Customer.getEmail(),
                    InvAddress1: Customer.getInvAddress1(),
                    InvAddress2: Customer.getInvAddress2(),
                    InvAddress3: Customer.getInvAddress3(),
                    InvAddress4: Customer.getInvAddress4(),
                    InvPostcode: Customer.getInvPostcode(),
                    VATRate: Customer.getVATRate(),
                    FDApproval: Customer.getFDApproval(),
                    MDApproval: Customer.getMDApproval(),
                    GMApproval: Customer.getGMApproval(),
                    CreditLimit: Customer.getCreditLimit(),
                    MarkUpQuote: Customer.getMarkUpQuote(),
                    PaymentTerms: Customer.getPaymentTerms(),
                    PaymentRunFrequency: Customer.getPaymentRunFrequency(),
                    fkApprovalID: Customer.getFkApprovalID(),
                    DeliveryCharge: Customer.getDeliveryCharge(),
                    IrishCurrency: Customer.getIrishCurrency(),
                    USCurrency: Customer.getUSCurrency(),
                    EuroCurrency: Customer.getEuroCurrency(),
                    Day1Open: data.Day1Open,
                    Day1Close: data.Day1Close,
                    Day2Open: data.Day2Open,
                    Day2Close: data.Day2Close,
                    Day3Open: data.Day3Open,
                    Day3Close: data.Day3Close,
                    Day4Open: data.Day4Open,
                    Day4Close: data.Day4Close,
                    Day5Open: data.Day5Open,
                    Day5Close: data.Day5Close,
                    Day6Open: data.Day6Open,
                    Day6Close: data.Day6Close,
                    Day7Open: data.Day7Open,
                    Day7Close: data.Day7Close,
                    Deleted: Customer.getDeleted(),
                    DeliveryGrace: Customer.getDeliveryGrace()
                })
                .then(function(result) {
                    Notification.show('success', 'Update Successful');
                })
                .catch(function(error) {
                    throw (error);
                });
        };

        /**
         * Save Customer
         */
        $scope.saveCustomer = function(CustomerDetail, PaymentTerm, PaymentRunFrequency, fkApprovalID) {
            var CustomerInfo = new Customer(CustomerDetail);

            API.procedure.post('ProcAddNewCustomer', {
                    UserID: $rootScope.user.getId(),
                    CustomerSageCode: CustomerInfo.FinanceCode,
                    CustomerName: CustomerInfo.getCustomerName(),
                    Alias: CustomerInfo.getAlias(),
                    Address1: CustomerInfo.getAddress1(),
                    Address2: CustomerInfo.getAddress2(),
                    Address3: CustomerInfo.getAddress3(),
                    Address4: CustomerInfo.getAddress4(),
                    Postcode: CustomerInfo.getPostcode(),
                    Phone: CustomerInfo.getPhone(),
                    Facsimile: CustomerInfo.getFacsimile(),
                    Website: CustomerInfo.getWebsite(),
                    Email: CustomerInfo.getEmail(),
                    InvAddress1: CustomerInfo.getInvAddress1(),
                    InvAddress2: CustomerInfo.getInvAddress2(),
                    InvAddress3: CustomerInfo.getInvAddress3(),
                    InvAddress4: CustomerInfo.getInvAddress4(),
                    InvPostcode: CustomerInfo.getInvPostcode(),
                    VATRate: CustomerInfo.getVATRate(),
                    FDApproval: CustomerInfo.getFDApproval(),
                    MDApproval: CustomerInfo.getMDApproval(),
                    GMApproval: CustomerInfo.getGMApproval(),
                    CreditLimit: CustomerInfo.getCreditLimit(),
                    MarkUpQuote: CustomerInfo.getMarkUpQuote(),
                    PaymentTerms: PaymentTerm.value,
                    PaymentRunFrequency: PaymentRunFrequency.value,
                    fkApprovalID: fkApprovalID.pkApprovalID,
                    DeliveryCharge: CustomerInfo.getDeliveryCharge(),
                    IrishCurrency: CustomerInfo.getIrishCurrency(),
                    EuroCurrency: CustomerInfo.getEuroCurrency(),
                    USCurrency: CustomerInfo.getUSCurrency(),
                    DeliveryGrace: CustomerInfo.getDeliveryGrace()
                })
                .then(function(result) {
                    Notification.show('success', 'Customer added');
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                    // Notification.show('warning', 'Finance Code has exist');
                    // return false;
                });
        };
    }

})(angular.module('app.components.department.customer', [
    'app.components.customer.main',
    'app.components.customer.info',
    'app.components.customer.contact',
    'app.components.customer.order-history',
    'app.components.customer.delivery-points',
]));
