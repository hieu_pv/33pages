(function(app) {
    app.controller('OrderHistoryCtrl', OrderHistoryCtrl);

    OrderHistoryCtrl.$inject = ['$rootScope', '$scope', '$stateParams', '$window', 'API', '$state', 'Notification'];

    function OrderHistoryCtrl($rootScope, $scope, $stateParams, $window, API, $state, Notification) {
        var customer_id = $stateParams.customer_id;

        /**
         * get Part drop-down
         */
        API.procedure.get('ProcGetPartNos')
            .then(function(result) {
                $scope.PartNos = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * the function working when select an item of Part drop-down
         */
        $scope.changePartNo = function(PartNo) {
            $scope.CustomerOrderHistory = _.filter($scope.filterCustomerOrder, _.matchesProperty('PartID', PartNo.PartID));
        };

        /**
         * get Raise Complete table
         */
        API.procedure.post('ProcGetCustomerOrderHistory', {
                CustomerID: customer_id
            })
            .then(function(result) {
                $scope.CustomerOrderHistory = result;

                // Use to filter when select Part drop-down
                $scope.filterCustomerOrder = angular.copy(result);
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * the function working when select a item on Raised Completed table
         */
        $scope.selectOrder = function(CustomerOrder) {

            /**
             * populate WO No table
             */
            API.procedure.post('ProcGetWorkOrderHistory', {
                    CONo: parseInt(CustomerOrder.CONo)
                })
                .then(function(result) {
                    $scope.WorkOrderHistory = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * populate Order note table
             */
            API.procedure.post('ProcGetOrderNotesHistory', {
                    CONo: parseInt(CustomerOrder.CONo)
                })
                .then(function(result) {
                    $scope.OrderNotesHistory = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when select a item on WorkOrder table
         */
        $scope.WONoSelected = function(WONo) {

            /**
             * get Reject Reason table
             */
            API.procedure.post('ProcGetOrderRejectHistory', {
                    WorkOrderID: WONo.WONo
                })
                .then(function(result) {
                    $scope.OrderRejectHistory = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click Repeat Order button
         */
        $scope.RepeatOrder = function(WorkOrderHistory) {
            var selectedWorkOrder = _.find(WorkOrderHistory, { 'selected': true });
            if (selectedWorkOrder !== undefined) {

                API.procedure.post('ProcRepeatWorkOrder', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: selectedWorkOrder.WONo,
                        Qty: selectedWorkOrder.Qty
                    })
                    .then(function(result) {
                        $scope.RepeatWorkOrder = result[0][1];
                    })
                    .catch(function(error) {
                        throw error;
                    });

            } else {
                Notification.show('warning', 'Please select Work Order');
                return false;
            }
        };

        /**
         * the function working when double click item on WorkOrder table
         */
        $scope.gotoWorkOrder = function(WorkOrder) {
            var state = 'department.workorders.main';
            var params = {
                WorkOrderID: WorkOrder.WONo
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }
})(angular.module('app.components.customer.order-history', ['angular.filter']));
