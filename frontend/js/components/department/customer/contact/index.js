var CustomerOpenHours = require('../../../../models/CustomerOpenHours');
var CustomerContact = require('../../../../models/CustomerContact');

(function(app) {
    app.controller('CustomerContactCtrl', CustomerContactCtrl);

    CustomerContactCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification', '$window'];

    function CustomerContactCtrl($rootScope, $scope, $stateParams, API, $state, Notification, $window) {

        var customer_id = $stateParams.customer_id;
        $scope.customer_id = customer_id;

        $scope.getCustomerAdditional = getCustomerAdditional;
        $scope.addCustomerAdditional = addCustomerAdditional;
        $scope.rmvAdditional = rmvAdditional;

        $scope.getCustomerNotes = getCustomerNotes;
        $scope.addNote = addNote;
        $scope.rmvNote = rmvNote;

        $scope.getCustomerContacts = getCustomerContacts;
        $scope.addContact = addContact;
        $scope.updateContact = updateContact;
        $scope.rmvContact = rmvContact;

        $scope.redirectBack = redirectBack;
        $scope.updateOpeningHours = updateOpeningHours;

        function getCustomerAdditional(customer_id) {
            API.procedure.post('ProcGetCustomerAdditional', {
                    CustomerID: customer_id,
                })
                .then(function(result) {
                    $scope.CustomerAdditional = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getCustomerContacts(customer_id) {
            API.procedure.post('ProcGetCustomerContacts', {
                    CustomerID: customer_id,
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.CustomerContacts = _.map(result, function(item) {
                            if (!_.isNil(item.AcknowContact)) {
                                item.AcknowContact = parseInt(item.AcknowContact);
                            }
                            if (!_.isNil(item.PrimaryContact)) {
                                item.PrimaryContact = parseInt(item.PrimaryContact);
                            }
                            return item;
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getCustomerNotes(customer_id) {
            API.procedure.post('ProcGetCustomerNotes', {
                    CustomerID: customer_id,
                })
                .then(function(result) {
                    $scope.CustomerNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function addNote(Note) {
            if (_.isNil(Note) || Note === '') {
                Notification.show('warning', 'Please enter Freeform Notes');
                return false;
            }
            API.procedure.post('ProcAddCustomerNote', {
                    UserID: $rootScope.user.getId(),
                    CustomerID: customer_id,
                    Note: Note
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        }

        $scope.$watch('CustomerInfo', function(CustomerInfo) {
            if (_.isNil(CustomerInfo) || _.isNil($scope.days)) {
                $scope.getCustomerInfo(customer_id);
            } else {
                getCustomerAdditional(customer_id);
                getCustomerContacts(customer_id);
                getCustomerNotes(customer_id);
            }
        });

        function _validateContact(Customer) {
            var regexExtension = /^\d+$/;
            var regexPhone = /^\d+[ ]?\d*$/;
            var regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            if (Customer.ContactName === '') {
                Notification.show('warning', 'Please enter Name');
                return false;
            }
            if (Customer.Extension !== '') {
                if (!regexExtension.test(Customer.Extension)) {
                    Notification.show('warning', 'Extension must be number');
                    return false;
                }
            }
            if (Customer.Phone === '') {
                Notification.show('warning', 'Please enter Phone');
                return false;
            } else {
                if (!regexPhone.test(Customer.Phone)) {
                    Notification.show('warning', 'Phone must be number');
                    return false;
                }
            }
            if (Customer.Email === '') {
                Notification.show('warning', 'Please enter Email');
                return false;
            } else {
                if (!regexEmail.test(Customer.Email)) {
                    Notification.show('warning', 'Email is not valid');
                    return false;
                }
            }
            return true;
        }

        function addContact(Customer) {
            if (!_.isNil(Customer)) {
                Customer = new CustomerContact(Customer);
                var validate = _validateContact(Customer);
                if (validate === true) {
                    API.procedure.post('ProcAddCustomerContact', {
                            UserID: $rootScope.user.getId(),
                            CustomerID: customer_id,
                            ContactName: Customer.getContactName(),
                            Position: Customer.getPosition(),
                            Phone: Customer.getPhone(),
                            Extension: Customer.getExtension(),
                            Email: Customer.getEmail(),
                            AcknowContact: Customer.getAcknowContact(),
                            PrimaryContact: Customer.getPrimaryContact()
                        }).then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });

                }
            } else {
                Notification.show('warning', 'Please enter Customer Contact');
                return false;
            }
        }

        function updateContact(CustomerContacts) {
            if (CustomerContacts.length > 0) {
                var selectedContacts = _.filter(CustomerContacts, _.matches({ 'selected': true }));
                if (selectedContacts.length > 0) {
                    selectedContacts.forEach(function(value) {
                        value = new CustomerContact(value);
                        var validate = _validateContact(value);
                        if (validate === true) {
                            API.procedure.post('ProcUpdateCustomerContact', {
                                    UserID: $rootScope.user.getId(),
                                    ContactID: value.ContactID,
                                    ContactName: value.getContactName(),
                                    Position: value.getPosition(),
                                    Phone: value.getPhone(),
                                    Extension: value.getExtension(),
                                    Email: value.getEmail(),
                                    AcknowContact: value.getAcknowContact(),
                                    PrimaryContact: value.getPrimaryContact()
                                })
                                .then(function(result) {
                                    Notification.show('success', 'Update success');
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    });
                } else {
                    Notification.show('warning', 'Please select Customer Contact');
                    return false;
                }
            }
        }

        function rmvContact(CustomerContacts) {
            if (CustomerContacts.length > 0) {
                var selectedContacts = _.filter(CustomerContacts, _.matches({ 'selected': true }));
                if (selectedContacts.length > 0) {
                    selectedContacts.forEach(function(value) {

                        API.procedure.post('ProcDeleteCustomerContact', {
                                UserID: $rootScope.user.getId(),
                                ContactID: value.ContactID
                            })
                            .then(function(result) {
                                _.remove(CustomerContacts, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });

                } else {
                    Notification.show('warning', 'Please select Customer Contact');
                    return false;
                }
            }
        }

        function rmvNote(CustomerNotes) {
            if (CustomerNotes.length > 0) {
                var selectedNotes = _.filter(CustomerNotes, _.matches({ 'selected': true }));
                if (selectedNotes.length > 0) {
                    selectedNotes.forEach(function(value) {

                        API.procedure.post('ProcDeleteCustomerNote', {
                                UserID: $rootScope.user.getId(),
                                NoteID: value.NoteID
                            })
                            .then(function(result) {
                                _.remove(CustomerNotes, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });

                } else {
                    Notification.show('warning', 'Please select Customer Note');
                    return false;
                }
            }
        }

        function rmvAdditional(CustomerAdditional) {
            if (CustomerAdditional.length > 0) {
                var selectedAdditional = _.filter(CustomerAdditional, _.matches({ 'selected': true }));
                if (selectedAdditional.length > 0) {
                    selectedAdditional.forEach(function(value) {

                        API.procedure.post('ProcDeleteCustomerAdditional', {
                                UserID: $rootScope.user.getId(),
                                AdditionaID: value.AdditionalID
                            })
                            .then(function(result) {
                                _.remove(CustomerAdditional, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });

                } else {
                    Notification.show('warning', 'Please select Additional');
                    return false;
                }
            }
        }

        function redirectBack() {
            $window.history.back();
        }

        function addCustomerAdditional(Additional) {
            if (_.isNil(Additional)) {
                Notification.show('warning', 'Please select Additional');
                return false;
            }
            if (_.isNil(Additional.StartPoint)) {
                Additional.StartPoint = '';
            }
            if (_.isNil(Additional.EndPoint)) {
                Additional.EndPoint = '';
            }
            API.procedure.post('ProcAddCustomerAdditional', {
                    UserID: $rootScope.user.getId(),
                    CustomerID: customer_id,
                    TimeType: Additional.TimeType,
                    StartPoint: Additional.StartPoint,
                    EndPoint: Additional.EndPoint
                })
                .then(function(response) {
                    $scope.Additional = null;
                    getCustomerAdditional(customer_id);
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function updateOpeningHours(Customer) {
            var data = {};
            for (var key in Customer) {
                var match = key.match(/^Day\d(Close|Open)$/gi);
                if (match !== null) {
                    if (Customer[key] === 'close' || Customer[key] === undefined) {
                        data[key] = '';
                    }
                    var regex = /^\d{4}$/;
                    if (!regex.test(Customer[key])) {
                        data[key] = '000000';
                    } else {
                        data[key] = Customer[key] + "00";
                    }
                }
            }
            API.procedure.post('ProcUpdateOpeningHours', {
                    UserID: $rootScope.user.getId(),
                    CustomerID: customer_id,
                    Day1Open: data.Day1Open,
                    Day1Close: data.Day1Close,
                    Day2Open: data.Day2Open,
                    Day2Close: data.Day2Close,
                    Day3Open: data.Day3Open,
                    Day3Close: data.Day3Close,
                    Day4Open: data.Day4Open,
                    Day4Close: data.Day4Close,
                    Day5Open: data.Day5Open,
                    Day5Close: data.Day5Close,
                    Day6Open: data.Day6Open,
                    Day6Close: data.Day6Close,
                    Day7Open: data.Day7Open,
                    Day7Close: data.Day7Close,
                })
                .then(function(response) {
                    Notification.show('success', 'Update opening hours success');
                    API.procedure.post('ProcGetOpeningHours', {
                            CustomerID: customer_id,
                        })
                        .then(function(result) {
                            $scope.CustomerInfo = $scope.CustomerInfo.setOptions(result[0]);
                        })
                        .catch(function(error) {
                            throw (error);
                        });
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }
})(angular.module('app.components.customer.contact', []));
