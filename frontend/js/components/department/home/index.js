(function(app) {
    app.controller('DeptHomeCtrl', DeptHomeCtrl);
    DeptHomeCtrl.$inject = ['$rootScope', '$scope', '$state', 'Notification', 'API', '$stateParams', '$cookieStore'];

    function DeptHomeCtrl($rootScope, $scope, $state, Notification, API, $stateParams, $cookieStore) {
        var DeptID = $stateParams.DeptID;

        $scope.getTodoList = getTodoList;
        $scope.CompleteTodo = CompleteTodo;
        $scope.openHTML = openHTML;
        $scope.redirectPage = redirectPage;
        $scope.logout = logout;

        getTodoList(DeptID);

        function getTodoList(DeptID) {
            API.procedure.post('ProcGetTodoList', {
                    DeptID: DeptID
                })
                .then(function(result) {
                    $scope.TodoList = result;
                    if ($scope.TodoList.length > 0) {
                        $scope.TodoList.forEach(function(value) {
                            if (!_.isNil(value.Qty) && value.Qty !== "") {
                                value.Qty = parseInt(value.Qty);
                            }
                        });
                    }
                })
                .catch(function(error) {
                    throw (error);
                });
        }

        /**
         * Sort Table
         */
        $scope.sortTable = [{
            title: 'Age',
            sortBy: 'Age',
            class: 'col-xs-3 no-padding'
        }, {
            title: 'Action',
            sortBy: 'ActionType',
            class: 'col-xs-1 no-padding'
        }, {
            title: 'Customer',
            sortBy: 'CustomerName',
            class: 'col-xs-3 no-padding'
        }, {
            title: 'Qty',
            sortBy: 'Qty',
            class: 'col-xs-1 no-padding'
        }, {
            title: 'WO No.',
            sortBy: 'WONo',
            class: 'col-xs-1 no-padding'
        }, {
            title: 'Part Number',
            sortBy: 'PartNO',
            class: 'col-xs-2 no-padding'
        }, {
            title: 'Complete',
            sortBy: 'Age',
            class: 'col-xs-1 no-padding'
        }, ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        function CompleteTodo(Todo) {
            API.procedure.post('ProcCompleteTodo', {
                    TodoID: Todo.TodoID,
                    UserID: $rootScope.user.getId(),
                })
                .then(function(result) {
                    _.remove($scope.TodoList, Todo);
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function openHTML(page) {
            var url = '/' + page;
            window.open(url, '_blank');
        }

        function redirectPage(item) {
            var params = {};
            var action = item.ActionType;
            action = _.camelCase(action);
            action = _.toLower(action);

            switch (action) {
                case "review":
                case "plan":
                case "planchg":
                case "estimate":
                case "nre":
                    /**
                     * Dieu kien chuan cua khach
                     * gio toi fix lai de? chuyen? trang Engineering co' du~ lieu
                     */
                    // if (item.ETemplateID) {
                    //     params.ETemplateID = item.ETemplateID;
                    //     $rootScope.redirectDept('department.engineering-template.material', params);
                    // }
                    var hasETemplateID = false;
                    var hasWONo = false;
                    if (!_.isNil(item.WONo) && item.WONo !== '') {
                        hasWONo = true;
                        params.WorkOrderID = item.WONo;
                    }
                    if (!_.isNil(item.ETemplateID) && item.ETemplateID !== '') {
                        hasETemplateID = true;
                        if (!hasWONo) {
                            params.EngineeringTemplateID = item.ETemplateID;
                        }
                    }
                    if (hasETemplateID || hasWONo) {
                        $rootScope.redirectDept('department.engineering-template.material', params);
                    }
                    break;
                case "grin":
                    if (item.GRBID) {
                        params.GRBID = item.GRBID;
                        $rootScope.redirectDept('department.inspection-schedule.inspection', params);
                    }
                    break;
                case 'issue':
                    if (item.GRBID) {
                        params.WorkOrderID = item.WONo;
                        params.GRBNo = item.GRBID;
                        $rootScope.redirectDept('department.material-issue', params);
                    }
                    break;
                case 'sc':
                    $rootScope.redirectDept('department.delivery-despatch');
                    break;
                case 'invoice':
                case 'outvoice':
                    if (item.WONo) {
                        params.WorkOrderID = item.WONo;
                        $rootScope.redirectDept('department.workorders.invoice', params);
                    }
                    break;
                case 'splitbatch':
                case 'schedule':
                    $rootScope.redirectDept('department.production-schedule.material-issue');
                    break;
                case 'cr':
                    if (item.ETemplateID) {
                        params.ETemplateID = item.ETemplateID;
                        $rootScope.redirectDept('department.engineering-template.review', params);
                    }
                    break;
                case 'purchase':
                case 'quote':
                case 'chasepo':
                    $rootScope.redirectDept('department.purchase-order');
                    break;
                case 'credit':
                    if (item.WONo) {
                        params.WorkOrderID = item.WONo;
                        $rootScope.redirectDept('department.workorders.main', params);
                    }
                    break;
                case 'service':
                    $rootScope.redirectDept('department.machine-control');
                    break;
                case 'dispatch':
                    $rootScope.redirectDept('department.delivery-despatch');
                    break;
                default:
                    console.log("don't exists action: " + action);
            }
        }

        function logout() {
            API.user.logout()
                .then(function(response) {
                    $state.go('login');
                    $cookieStore.remove('token');
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }
})(angular.module('app.components.department.home', []));
