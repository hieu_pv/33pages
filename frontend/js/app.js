var $ = window.$ = require('jquery');
var jQuery = window.jQuery = require('jquery');
var moment = require('moment');
require('angular');
require('bootstrap');
require('lodash');
require('angular-cookies');
// require('typeahead');
require('bloodhound');
require('angular-bootstrap');
require('angular-ui-mask');
require('angular-filter');
require('floatThead');
require('angularFloatThead');
require('angularSanitize');
require('ngStorage');
require('./routes');
require('./templates');
require('./components/');
require('./api/');
require('./common/services/');
require('./common/filters/');
require('./common/directives/');
require('./common/exception');
(function(app) {
    app.run(['$rootScope', '$cookieStore', '$state', '$http', 'API', '$window',
        function($rootScope, $cookieStore, $state, $http, API, $window) {
            var token = $cookieStore.get('token');
            if (token !== undefined) {
                $http.defaults.headers.common.Authorization = 'Bearer ' + token;
                API.user.profile()
                    .then(function(result) {
                        $rootScope.user = result;
                    })
                    .catch(function(error) {
                        $state.go('login');
                    });
            } else {
                $state.go('login');
            }

            /**
             * Path Image Upload
             */
            $rootScope.HostName = $window.location.protocol + "//" + $window.location.hostname;
            $rootScope.PathImageUpload = '/resources/uploads/images/';
            $rootScope.DirImageUpload = $rootScope.PathImageUpload.substring(1);
            $rootScope.PathExcelUpload = '/resources/uploads/excels/';

            var httpBuildQuery = function(params) {
                if (typeof params === 'undefined' || typeof params !== 'object') {
                    params = {};
                    return params;
                }
                var query = '';
                var index = 0;
                for (var i in params) {
                    index++;
                    var param = i;
                    var value = params[i];
                    if (index == 1) {
                        query += param + '=' + value;
                    } else {

                        query += '&' + param + '=' + value;
                    }

                }
                return query;
            };

            $rootScope.openTabBirt = function(name, params) {
                var birt_url;
                if (BIRT_ENV === 'local') {
                    birt_url = 'http://33pages.com:8080/JavaBridge/kythera/';
                } else {
                    birt_url = 'http://104.168.174.40:8080/JavaBridge/kythera/';
                }
                var url = birt_url + name + ".php";
                if (params !== undefined) {
                    var query = httpBuildQuery(params);
                    url = url + "?" + query;
                }
                $window.open(url);
            };

            /**
             * List select Payment Terms
             */
            $rootScope.ListPaymentTerms = [{
                value: '1',
                name: '30 days'
            }, {
                value: '2',
                name: '60 days'
            }, {
                value: '3',
                name: '90 days'
            }];

            /**
             * List Additional Contact
             */
            $rootScope.ListAdditionalContact = [{
                value: 1,
                name: 'Shutdown'
            }, {
                value: 2,
                name: 'Collection'
            }, {
                value: 3,
                name: 'Other'
            }];

            /*
             * global function to flip boolean value, if the attribute was not set then create new attribute with data is {value} or 'true' if {value} was not defined
             * @param item, attribute, value
             */
            $rootScope.selectItem = function(item, attribute, value) {
                if (item !== undefined) {
                    if (typeof item[attribute] === "boolean") {
                        item[attribute] = !item[attribute];
                    } else {
                        if (value === undefined) {
                            item[attribute] = true;
                        } else {
                            item[attribute] = value;
                        }
                    }
                }
            };

            /**
             * Global function to select item in array
             */
            $rootScope.selectOneItem = function(array, item, attribute, callback) {
                attribute = attribute || 'selected';
                if (Array.isArray(array) && item !== undefined) {
                    if (typeof item[attribute] === 'boolean') {
                        item[attribute] = !item[attribute];
                    } else {
                        array.forEach(function(v, k) {
                            delete v[attribute];
                        });
                        item[attribute] = true;
                    }
                    if (callback !== undefined) {
                        callback(item);
                    }
                }
                console.log(item);
            };

            $rootScope.selectNextItem = function(array) {
                var index = _.findIndex(array, {
                    selected: true
                });
                console.log(index);
                if (index === -1) {
                    array[0].selected = true;
                } else {
                    if (array[index + 1] !== undefined) {
                        delete array[index].selected;
                        array[index + 1].selected = true;
                    }
                }
            };

            $rootScope.selectPrevItem = function(array) {
                var index = _.findIndex(array, {
                    selected: true
                });
                console.log(index);
                if (index !== -1) {
                    if (array[index - 1] !== undefined) {
                        delete array[index].selected;
                        array[index - 1].selected = true;
                    }
                }
            };

            $rootScope.changeIndex = function(array) {
                var index = _.findIndex(array, {
                    selected: true
                });
                var totalItem = array.length;
                var newIndex = totalItem - index - 1;
                array[index].selected = false;
                array[newIndex].selected = true;
            };

            $rootScope.convertTime = function(timeStr, format) {
                if (timeStr === null) {
                    return null;
                } else {
                    if (timeStr.match(/^([0-9]*):([0-9]*):([0-9]*)$/)) {
                        timeStr = '2016-01-01 ' + timeStr;
                    }
                    return moment(timeStr).format(format);
                }
            };

            $rootScope.validateDate = function(date, format) {
                if (date === undefined | format === undefined) {
                    return false;
                }
                var listFormat = [
                    'dd/mm/yyyy',
                    'dd-mm-yyyy',
                    'mm/dd/yyyy',
                    'mm-dd-yyyy',
                    'yyyy/mm/dd',
                    'yyyy-mm-dd'
                ];
                format = format.toLowerCase();

                if (listFormat.indexOf(format) == -1) {
                    return false;
                } else {
                    var date_regex;
                    if (format === 'mm/dd/yyyy') {
                        date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
                    }
                    if (format === 'mm-dd-yyyy') {
                        date_regex = /^(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])\-(19|20)\d{2}$/;
                    }
                    if (format === 'dd/mm/yyyy') {
                        date_regex = /^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/;
                    }
                    if (format === 'dd-mm-yyyy') {
                        date_regex = /^(0[1-9]|1\d|2\d|3[01])\-(0[1-9]|1[0-2])\-(19|20)\d{2}$/;
                    }
                    if (format === 'yyyy/mm/dd') {
                        date_regex = /^(19|20)\d{2}\/(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])$/;
                    }
                    if (format === 'yyyy-mm-dd') {
                        date_regex = /^(19|20)\d{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])$/;
                    }
                    return date_regex.test(date);
                }
            };

            function time2second(time) {
                if (!time) {
                    return "";
                }

                /**
                 * Convert time string to second
                 */
                var seconds;
                if (typeof time === 'string') {
                    var array = timeStr.split(":");
                    seconds = (parseInt(array[0], 10) * 60 * 60) + (parseInt(array[1], 10) * 60) + parseInt(array[2], 10);
                } else {
                    seconds = time * 3600;
                }
                return seconds;
            }

            $rootScope.timeFormat = function(timeStr, format, decimal, extension) {
                if (!timeStr) {
                    return "";
                }

                /**
                 * Convert time string to second
                 */
                var regexHHMMSS = /^\d+\:\d+\:\d+$/;
                var seconds;
                if (regexHHMMSS.test(timeStr)) {
                    var array = timeStr.split(":");
                    seconds = (parseInt(array[0], 10) * 60 * 60) + (parseInt(array[1], 10) * 60) + parseInt(array[2], 10);
                } else {
                    seconds = parseFloat(timeStr) * 3600;
                }
                var time = seconds2time(seconds, format, decimal, extension);
                return time;
            };

            $rootScope.convertNumberToHHMMSS = function(number) {

                // Convert number to second
                var totalSecond = parseFloat(number * 3600);

                // get hour
                var hour = Math.floor(totalSecond / 3600);
                totalSecond -= hour * 3600;

                // get minutes
                var minutes = Math.floor(totalSecond / 60);
                totalSecond -= minutes * 60;

                // get second
                var second = Math.floor(totalSecond);
                return hour + ":" + minutes + ":" + second;
            };

            function seconds2time(seconds, format, decimal, extension) {
                var formatArr = [
                    's', 'm', 'h', 'auto'
                ];
                format = format.toLowerCase();
                if (formatArr.indexOf(format) === -1) {
                    return "format Not Support";
                }

                if (!Math.round10) {
                    Math.round10 = function(value, exp) {
                        return decimalAdjust('round', value, exp);
                    };
                }

                var time = "";
                if (format === 's') {
                    time = seconds;
                }

                if (format === 'm') {
                    time = Math.round10(seconds / 60, decimal);
                }

                if (format === 'h') {
                    time = Math.round10(seconds / 3600, decimal);
                }

                if (format === 'auto') {
                    var hours = Math.floor(seconds / 3600);
                    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
                    if (hours !== 0) {
                        time = Math.round10(seconds / 3600, decimal);
                        if (extension === true) {
                            if (hours === 1) {
                                time += " hour";
                            } else {
                                time += " hours";
                            }
                        }
                    } else {
                        time = Math.round10(seconds / 60, decimal);
                        if (minutes !== 0) {
                            if (extension === true) {
                                if (minutes === 1) {
                                    time += " min";
                                } else {
                                    time += " mins";
                                }
                            }
                        } else {
                            if (!minutes | minutes === 0) {
                                time = seconds;
                                time = Math.round(time);
                                if (extension === true) {
                                    if (time === 1) {
                                        time += " sec";
                                    } else {
                                        time += " secs";
                                    }
                                }
                            }
                        }

                    }
                }
                return time;
            }

            function decimalAdjust(type, value, exp) {

                // If the exp is undefined or zero...
                if (typeof exp === 'undefined' || +exp === 0) {
                    return Math[type](value);
                }
                value = +value;
                exp = +exp;

                // If the value is not a number or the exp is not an integer...
                if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                    return NaN;
                }

                // Shift
                value = value.toString().split('e');
                value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));

                // Shift back
                value = value.toString().split('e');
                return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
            }

            $rootScope.getExtensionFile = function(fileName) {
                return fileName.split('.').pop();
            };

            $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
                var windowHistory = $cookieStore.get('windowHistory');
                if (_.isNil(windowHistory)) {
                    windowHistory = [];
                }
                // console.log('from ', from);
                // console.log('to ', to);
            });

            $rootScope.redirectDept = function(deptState, params) {
                var Dept = [{
                    listState: [{
                        state: 'department.admin_reference'
                    }, {
                        state: 'department.customer.main'
                    }, {
                        state: 'department.customer.contact'
                    }, {
                        state: 'department.workorders'
                    }, {
                        state: 'department.workorders.main'
                    }, {
                        state: 'department.workorders.material-issue'
                    }, {
                        state: 'department.workorders.invoice'
                    }, {
                        state: 'department.customer-order'
                    }, {
                        state: 'department.purchase-order'
                    }, {
                        state: 'department.purchase-order.details'
                    }, {
                        state: 'department.purchase-order.instructions'
                    }, {
                        state: 'department.purchase-order.supplier'
                    }, {
                        state: 'department.purchase-order.rfq'
                    }, {
                        state: 'department.purchase-order.raise'
                    }, {
                        state: 'department.purchase-order.notes'
                    }, {
                        state: 'department.purchase-order.material-search'
                    }, {
                        state: 'department.purchase-order.invoice'
                    }, {
                        state: 'department.purchase-order.quality'
                    }, {
                        state: 'department.customer.order-history'
                    }, {
                        state: 'department.customer.delivery-points'
                    }],
                    DeptID: 7
                }, {
                    listState: [{
                        state: 'department.production-schedule.material-issue'
                    }, {
                        state: 'department.machine-control'
                    }, {
                        state: 'department.wo-detail'
                    }],
                    DeptID: 1
                }, {
                    listState: [{
                        state: 'department.goods-in'
                    }, {
                        state: 'department.material-issue'
                    }, {
                        state: 'department.delivery-despatch'
                    }, {
                        state: 'department.store-admin'
                    }],
                    DeptID: 2
                }, {
                    listState: [{
                        state: 'department.machine-list'
                    }],
                    DeptID: 3
                }, {
                    listState: [{
                        state: 'department.purchase-order'
                    }, {
                        state: 'department.supply-chain.main'
                    }, {
                        state: 'department.supply-chain.supply-contact'
                    }, {
                        state: 'department.supply-chain.supply-order-history'
                    }, {
                        state: 'department.supply-chain.supply-performance'
                    }],
                    DeptID: 4
                }, {
                    listState: [{
                        state: 'department.inspection-schedule.main'
                    }, {
                        state: 'department.inspection-schedule.inspection'
                    }, {
                        state: 'department.remarks-comments'
                    }],
                    DeptID: 5
                }, {
                    listState: [{
                        state: 'department.production-schedule'
                    }, {
                        state: 'department.production-schedule.material-issue'
                    }, {
                        state: 'department.operations-treatment-admin'
                    }, {
                        state: 'department.part-control.part-list'
                    }, {
                        state: 'department.engineering-template'
                    }, {
                        state: 'department.engineering-template.material'
                    }, {
                        state: 'department.engineering-template.review'
                    }, {
                        state: 'department.engineering-template.engineering'
                    }, {
                        state: 'department.engineering-template.subcon'
                    }],
                    DeptID: 6
                }, {
                    listState: [{
                        state: 'department.staff-room'
                    }, {
                        state: 'department.finance-admin'
                    }],
                    DeptID: 8
                }];

                var getDept = _.find(Dept, function(item) {
                    return _.find(item.listState, {
                        state: deptState
                    }) !== undefined;
                });
                if (getDept === undefined) {
                    throw ('Not found state. Please additional state');
                }
                $state.go(deptState, params);
            };

        }
    ]);
})(angular.module('app', [
    'app.router',
    'ngCookies',
    'ui.bootstrap',
    'ui.mask',
    // 'floatThead',
    'ngSanitize',
    'ngStorage',
    'angular.filter',
    'app.template',
    'app.components',
    'app.common.directives',
    'app.api',
    'app.common.services',
    'app.common.exceptionHandler',
    'app.common.filters'
]));