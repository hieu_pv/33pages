require('./toString');
require('./hasItem');
require('./timeFormat');
require('./currentIndex');
require('./range');
require('./css');

(function(app) {

})(angular.module('app.common.filters', [
    'app.common.filters.toString',
    'app.common.filters.hasItem',
    'app.common.filters.timeFormat',
    'app.common.filters.currentIndex',
    'app.common.filters.range',
    'app.common.filters.css'
]));
