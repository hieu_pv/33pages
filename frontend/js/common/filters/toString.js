(function(app) {
    app.filter('toString', function() {
        return function(entity) {
            return JSON.stringify(entity);
        };
    });
})(angular.module('app.common.filters.toString', []));
