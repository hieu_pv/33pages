(function(app) {

	app.filter('currentIndex', function() {
        return function(array, key, value) {
            var index = _.findIndex(array, {'selected': true});
            return index + 1;
        };
    });

})(angular.module('app.common.filters.currentIndex', []));