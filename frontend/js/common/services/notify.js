require('angular-notify');

(function(app) {
    app.run(run);

    run.$inject = ['notify'];

    function run(notify) {
        notify.config({
            startTop: 10,
            position: 'right',
            templateUrl: '/frontend/js/common/partials/angular-notify.tpl.html'
        });
    }

    app.factory('Notification', factory);

    factory.$inject = ['notify'];

    function factory(notify) {
        var service = {
            show: show
        };

        function show(type, msg, duration) {
            type = type || 'success';
            var classes = [];
            switch (type) {
                case 'success':
                    classes = ['notify', 'notify-success'];
                    break;
                case 'warning':
                    classes = ['notify', 'notify-warning'];
                    break;
                default:
                    break;
            }
            msg = msg || '';
            duration = duration || 4000;

            notify({
                message: msg,
                duration: duration,
                classes: classes,
            });
        }

        return service;
    }
})(angular.module('app.common.services.notify', ['cgNotify']));
