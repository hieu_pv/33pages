require('jquery');
require('typeahead');
var Bloodhound = require('bloodhound');
(function(app) {
    app.directive('typeahead', function() {
        return {
            strict: 'E',
            scope: {
                source: '=',
                key: '@',
                selectCallback: '=',
            },
            templateUrl: '/frontend/js/common/partials/typeahead.tpl.html',
            link: function(scope, element, attrs) {
                scope.$watch('source', function(source) {
                    if (source !== undefined) {
                        var data = new Bloodhound({
                            datumTokenizer: Bloodhound.tokenizers.obj.whitespace(scope.key),
                            queryTokenizer: Bloodhound.tokenizers.whitespace,
                            local: source
                        });
                        TypeaheadInit(data);
                    }
                });


                function TypeaheadInit(data) {
                    var Input = $(element).find('.typeahead');
                    Input.typeahead({
                        minLength: 1,
                        highlight: true,
                        hint: true,
                    }, {
                        name: scope.key,
                        display: scope.key,
                        source: data,
                    });
                    Input.bind('typeahead:select', function(evt, data) {
                        scope.selectCallback(data);
                    });
                }
            },
        };
    });
})(angular.module('app.common.directives.typeahead', ['app.template']));
