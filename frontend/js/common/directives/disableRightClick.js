(function(app) {

    app.directive('disableRightClick', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                $(element).on("contextmenu", function(e) {
                    return false;
                });

                $(element).bind('cut copy paste', function(e) {
                    e.preventDefault();
                });
            }
        };
    });

})(angular.module('app.common.directives.disableRightClick', []));
