require('./typeahead');
require('./myEnter');
require('./searchInput');
require('./dropzone');
require('./modalShow');
require('./andyFloatThead');
require('./disableRightClick');
// require('./previewFile');
(function(app) {

})(angular.module('app.common.directives', [
    'app.common.directives.typeahead',
    'app.common.directives.myEnter',
    'app.common.directives.searchInput',
    'app.common.directives.dropzone',
    'app.common.directives.modalShow',
    'app.common.directives.andyFloatThead',
    'app.common.directives.disableRightClick'
    // 'app.common.directives.previewFile'
]));
