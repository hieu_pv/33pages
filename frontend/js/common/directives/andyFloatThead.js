(function(app) {

    app.directive('andyFloatThead', function() {
        return {
            restrict: 'AE',
            scope: {
                items: '='
            },
            link: function(scope, elem, attrs) {
                scope.$watch('items', function(items) {
                    if (!_.isNil(items)) {
                        var $floatThead = $(elem);
                        $floatThead.floatThead({
                            scrollContainer: function($table) {
                                return $table.closest('.float-thead-wrapper');
                            }
                        });
                        $floatThead.floatThead('reflow');
                    }
                });
            }
        };
    });

})(angular.module('app.common.directives.andyFloatThead', []));
