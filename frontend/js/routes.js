require('ui-router');

(function(app) {
    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('select-department', {
                url: '/',
                templateUrl: "/frontend/js/components/landing-page/landing-page.tpl.html",
                controller: 'LandingPageCtrl',
            })
            .state('department', {
                url: '/department/:DeptID',
                templateUrl: "/frontend/js/components/department/department.tpl.html",
                controller: 'DepartmentCtrl',
            })
            .state('department.home', {
                url: '/home',
                templateUrl: "/frontend/js/components/department/home/home.tpl.html",
                controller: 'DeptHomeCtrl',
            })
            /*
             * Department - Admin References
             */
            .state('department.admin_reference', {
                url: '/admin-reference',
                templateUrl: "/frontend/js/components/department/admin-reference/admin_reference.tpl.html",
                controller: 'AdminReferenceCtrl',
            })
            /*
             * Department - Customer Center
             */
            .state('department.customer', {
                url: '/customer?customer_id',
                templateUrl: "/frontend/js/components/department/customer/customer.tpl.html",
                controller: 'CustomerCtrl',
            })
            .state('department.customer.main', {
                url: '/main',
                templateUrl: "/frontend/js/components/department/customer/main/main.tpl.html",
                controller: 'CustomerMainCtrl',
            })
            .state('department.customer.info', {
                url: '/info',
                templateUrl: "/frontend/js/components/department/customer/info/info.tpl.html",
                controller: 'CustomerInfoCtrl',
            })
            .state('department.customer.contact', {
                url: '/contact',
                templateUrl: "/frontend/js/components/department/customer/contact/contact.tpl.html",
                controller: 'CustomerContactCtrl',
            })
            .state('department.customer.order-history', {
                url: "/order-history",
                templateUrl: "/frontend/js/components/department/customer/order-history/order-history.tpl.html",
                controller: 'OrderHistoryCtrl',
            })
            .state('department.customer.delivery-points', {
                url: "/delivery-points",
                templateUrl: "/frontend/js/components/department/customer/delivery-points/delivery-points.tpl.html",
                controller: 'DeliveryPointCtrl',
            })
            /*
             * Department - Customer Order
             */
            .state('department.customer-order', {
                url: "/customer-order?CustomerId&CustomerOrderId&CustomerOrderRef",
                templateUrl: "/frontend/js/components/department/customer-order/customer-order.tpl.html",
                controller: 'CustomerOrderCtrl',
            })
            /*
             * Department - WorkOrder
             */
            .state('department.workorders', {
                url: "/work-orders?WorkOrderID",
                templateUrl: "/frontend/js/components/department/workorders/workorders.tpl.html",
                controller: 'WorkordersCtrl',
            })
            .state('department.workorders.main', {
                url: "/main",
                templateUrl: "/frontend/js/components/department/workorders/main/main.tpl.html",
                controller: 'WorkOrderMainCtrl',
            })
            .state('department.workorders.cocs', {
                url: "/cocs",
                templateUrl: "/frontend/js/components/department/workorders/cocs/cocs.tpl.html",
                controller: 'WorkOrderCocsCtrl',
            })
            .state('department.workorders.pos', {
                url: "/pos",
                templateUrl: "/frontend/js/components/department/workorders/pos/pos.tpl.html",
                controller: 'WorkOrderPosCtrl',
            })
            .state('department.workorders.costing', {
                url: "/costing",
                templateUrl: "/frontend/js/components/department/workorders/costing/costing.tpl.html",
                controller: 'WorkOrderCostingCtrl',
            })
            .state('department.workorders.invoice', {
                url: "/invoice",
                templateUrl: "/frontend/js/components/department/workorders/invoice/invoice.tpl.html",
                controller: 'WorkOrderInvoiceCtrl',
            })
            .state('department.workorders.route-card', {
                url: "/route-card",
                templateUrl: "/frontend/js/components/department/workorders/route-card/route-card.tpl.html",
                controller: 'WorkOrderRouteCardCtrl',
            })
            .state('department.workorders.material-issue', {
                url: "/material-issue",
                templateUrl: "/frontend/js/components/department/workorders/material-issue/material-issue.tpl.html",
                controller: 'WorkOrderMaterialIssueCtrl',
            })
            .state('department.workorders.document', {
                url: "/document",
                templateUrl: "/frontend/js/components/department/workorders/document/document.tpl.html",
                controller: 'WorkOrderDocumentCtrl',
            })
            /*
             * Department - Purchasing
             */
            .state('department.purchase-order', {
                url: "/purchase-order?RFQNo&PONo&PartNumber&Issue&Qty&Raised&Description",
                templateUrl: "/frontend/js/components/department/purchase-order/purchase-order.tpl.html",
                controller: 'PurchaseOrderCtrl',
            })
            .state('department.purchase-order.details', {
                url: "/details",
                templateUrl: "/frontend/js/components/department/purchase-order/details/details.tpl.html",
                controller: 'PurchaseOrderDetailsCtrl',
            })
            .state('department.purchase-order.instructions', {
                url: "/instructions",
                templateUrl: "/frontend/js/components/department/purchase-order/instructions/instructions.tpl.html",
                controller: 'PurchaseOrderInstructionsCtrl',
            })
            .state('department.purchase-order.supplier', {
                url: "/supplier",
                templateUrl: "/frontend/js/components/department/purchase-order/supplier/supplier.tpl.html",
                controller: 'PurchaseOrderSupplierCtrl',
            })
            .state('department.purchase-order.rfq', {
                url: "/rfq",
                templateUrl: "/frontend/js/components/department/purchase-order/rfq/rfq.tpl.html",
                controller: 'PurchaseOrderRfqCtrl',
            })
            .state('department.purchase-order.raise', {
                url: "/raise",
                templateUrl: "/frontend/js/components/department/purchase-order/raise/raise.tpl.html",
                controller: 'PurchaseOrderRaiseCtrl',
            })
            .state('department.purchase-order.notes', {
                url: "/notes",
                templateUrl: "/frontend/js/components/department/purchase-order/notes/notes.tpl.html",
                controller: 'PurchaseOrderNotesCtrl',
            })
            .state('department.purchase-order.material-search', {
                url: "/material-search",
                templateUrl: "/frontend/js/components/department/purchase-order/material-search/material-search.tpl.html",
                controller: 'PurchaseOrderMaterialSearchCtrl',
            })
            .state('department.purchase-order.invoice', {
                url: "/invoice",
                templateUrl: "/frontend/js/components/department/purchase-order/invoice/invoice.tpl.html",
                controller: 'PurchaseOrderInvoiceCtrl',
            })
            .state('department.purchase-order.quality', {
                url: "/quality",
                templateUrl: "/frontend/js/components/department/purchase-order/quality/quality.tpl.html",
                controller: 'PurchaseOrderQualityCtrl',
            })
            .state('department.supply-chain', {
                url: "/supply-chain?SupplierID&SupplierName",
                templateUrl: "/frontend/js/components/department/supply-chain/supply-chain.tpl.html",
                controller: 'SupplyChainCtrl',
            })
            .state('department.supply-chain.main', {
                url: "/main",
                templateUrl: "/frontend/js/components/department/supply-chain/main/main.tpl.html",
                controller: 'SupplyMainCtrl',
            })
            .state('department.supply-chain.supply-contact', {
                url: "/contact",
                templateUrl: "/frontend/js/components/department/supply-chain/supply-contact/supply-contact.tpl.html",
                controller: 'SupplyContactCtrl',
            })
            .state('department.supply-chain.supply-order-history', {
                url: "/order-history",
                templateUrl: "/frontend/js/components/department/supply-chain/supply-order-history/supply-order-history.tpl.html",
                controller: 'SupplyOrderHistoryCtrl',
            })
            .state('department.supply-chain.supply-performance', {
                url: "/performance",
                templateUrl: "/frontend/js/components/department/supply-chain/supply-performance/supply-performance.tpl.html",
                controller: 'SupplyPerformanceCtrl',
            })
            /**
             * Department Inspection Schedule
             */
            .state('department.inspection-schedule', {
                url: "/inspection-schedule",
                templateUrl: "/frontend/js/components/department/inspection-schedule/inspection-schedule.tpl.html",
                controller: 'InspectionScheduleCtrl',
            })
            .state('department.inspection-schedule.main', {
                url: "/main",
                templateUrl: "/frontend/js/components/department/inspection-schedule/main/main.tpl.html",
                controller: 'InspectionScheduleMainCtrl',
            })
            .state('department.inspection-schedule.inspection', {
                url: "/inspection?OperationID&WorkOrderID&PartNumber&GRBID",
                templateUrl: "/frontend/js/components/department/inspection-schedule/inspection/inspection.tpl.html",
                controller: 'InspectionCtrl',
            })
            .state('department.remarks-comments', {
                url: "/remarks-comments",
                templateUrl: "/frontend/js/components/department/remarks-comments/remarks-comments.tpl.html",
                controller: 'RemarkCommentCtrl',
            })
            /**
             * Department - Stores
             */
            .state('department.goods-in', {
                url: "/goods-in?WorkOrderID&GRBNo&WOGRBNo",
                templateUrl: "/frontend/js/components/department/goods-in/goods-in.tpl.html",
                controller: 'GoodsInCtrl',
            })
            .state('department.material-issue', {
                url: "/material-issue?WorkOrderID&GRBNo",
                templateUrl: "/frontend/js/components/department/material-issue/material-issue.tpl.html",
                controller: 'MaterialIssueCtrl',
            })
            .state('department.delivery-despatch', {
                url: "/delivery-despatch",
                templateUrl: "/frontend/js/components/department/delivery-despatch/delivery-despatch.tpl.html",
                controller: 'DeliveryDespatchCtrl',
            })
            .state('department.store-admin', {
                url: "/store-admin?StoreAreaID",
                templateUrl: "/frontend/js/components/department/store-admin/store-admin.tpl.html",
                controller: 'StoreAdminCtrl',
            })
            /**
             * Department - Production
             */
            .state('department.production-schedule', {
                url: "/production-schedule",
                templateUrl: "/frontend/js/components/department/production-schedule/production-schedule.tpl.html",
                controller: 'ProductionScheduleCtrl',
            })
            .state('department.production-schedule.material-issue', {
                url: "/material-issue",
                templateUrl: "/frontend/js/components/department/production-schedule/material-issue/material-issue.tpl.html",
                controller: 'ProductionScheduleMaterialIssueCtrl',
            })
            .state('department.production-schedule.subcon', {
                url: "/subcon",
                templateUrl: "/frontend/js/components/department/production-schedule/subcon/subcon.tpl.html",
                controller: 'ProductionScheduleSubconCtrl',
            })
            .state('department.production-schedule.machine', {
                url: "/:machine",
                templateUrl: "/frontend/js/components/department/production-schedule/machine/machine.tpl.html",
                controller: 'ProductionScheduleMachineCtrl',
            })
            .state('department.wo-detail', {
                url: "/wo-detail?WorkOrderID",
                templateUrl: "/frontend/js/components/department/wo-detail/wo-detail.tpl.html",
                controller: 'WoDetailCtrl',
            })
            .state('department.machine-control', {
                url: "/machine-control",
                templateUrl: "/frontend/js/components/department/machine-control/machine-control.tpl.html",
                controller: 'MachineControlCtrl',
            })
            /**
             * Department - Engineering
             */
            .state('department.machine-list', {
                url: "/machine-list?MachineID&MachineName&MachineType",
                templateUrl: "/frontend/js/components/department/machine-list/machine-list.tpl.html",
                controller: 'MachineListCtrl',
            })
            /**
             * Department - Finance / HR
             */
            .state('department.staff-room', {
                url: "/staff-room?StaffUserID",
                templateUrl: "/frontend/js/components/department/staff-room/staff-room.tpl.html",
                controller: 'StaffRoomCtrl',
            })
            .state('department.finance-admin', {
                url: "/finance-admin",
                templateUrl: "/frontend/js/components/department/finance-admin/finance-admin.tpl.html",
                controller: 'FinanceAdminCtrl',
            })
            /**
             * Department - Planning / Estimating
             */
            .state('department.operations-treatment-admin', {
                url: "/operations-treatment-admin",
                templateUrl: "/frontend/js/components/department/operations-treatment-admin/operations-treatment-admin.tpl.html",
                controller: 'OperationsTreatmentAdminCtrl',
            })
            .state('department.part-control', {
                url: "/part-control?PTemplateID&PartNumber&ETemplateID",
                templateUrl: "/frontend/js/components/department/part-control/part-control.tpl.html",
                controller: 'PartControlCtrl',
            })
            .state('department.part-control.part-list', {
                url: "/part-list",
                templateUrl: "/frontend/js/components/department/part-control/part-list/part-list.tpl.html",
                controller: 'PartListCtrl',
            })
            .state('department.part-control.assemblies', {
                url: "/assemblies",
                templateUrl: "/frontend/js/components/department/part-control/assemblies/assemblies.tpl.html",
                controller: 'PartAssemblyCtrl',
            })
            .state('department.part-control.notes', {
                url: "/notes",
                templateUrl: "/frontend/js/components/department/part-control/notes/notes.tpl.html",
                controller: 'PartNotesCtrl',
            })
            .state('department.part-control.review-tasks', {
                url: "/review-tasks",
                templateUrl: "/frontend/js/components/department/part-control/review-tasks/review-tasks.tpl.html",
                controller: 'PartReviewTasksCtrl',
            })
            .state('department.part-control.docs-newpart', {
                url: "/docs",
                templateUrl: "/frontend/js/components/department/part-control/docs/docs.tpl.html",
                controller: 'PartDocsCtrl',
            })
            .state('department.part-history', {
                url: "/part-history?PTemplateID&PartNumber&COS&Issue",
                templateUrl: "/frontend/js/components/department/part-history/part-history.tpl.html",
                controller: 'PartHistoryCtrl',
            })
            .state('department.engineering-template', {
                url: "/engineering-template?EngineeringTemplateID&WorkOrderID&OperationID&OperationTemplateID",
                templateUrl: "/frontend/js/components/department/engineering-template/engineering-template.tpl.html",
                controller: 'EngineeringTemplateCtrl',
            })
            .state('department.engineering-template.material', {
                url: "/material",
                templateUrl: "/frontend/js/components/department/engineering-template/material-requirement/material-requirement.tpl.html",
                controller: 'EngineeringMaterialCtrl',
            })
            .state('department.engineering-template.pering', {
                url: "/pering",
                templateUrl: "/frontend/js/components/department/engineering-template/pering/pering.tpl.html",
                controller: 'EngineeringPeringCtrl',
            })
            .state('department.engineering-template.engineering', {
                url: "/engineering",
                templateUrl: "/frontend/js/components/department/engineering-template/engineering/engineering.tpl.html",
                controller: 'EngineeringEngineeringCtrl',
            })
            .state('department.engineering-template.subcon', {
                url: "/subcon",
                templateUrl: "/frontend/js/components/department/engineering-template/subcon/subcon.tpl.html",
                controller: 'EngineeringSubconCtrl',
            })
            .state('department.engineering-template.review', {
                url: "/review",
                templateUrl: "/frontend/js/components/department/engineering-template/review/review.tpl.html",
                controller: 'EngineeringReviewCtrl',
            })
            .state('department.engineering-template.overview', {
                url: "/overview",
                templateUrl: "/frontend/js/components/department/engineering-template/overview/overview.tpl.html",
                controller: 'EngineeringOverviewCtrl',
            })
            .state('overview', {
                url: "/overview",
                templateUrl: "/frontend/js/components/department/overview/overview.tpl.html",
                // /controller: 'DeliveryDespatchCtrl',
            })
            .state('user', {
                url: '/user',
                templateUrl: "/frontend/js/components/department/customer-order/customer-order.tpl.html",
                controller: 'UserCtrl',
            })
            .state('login', {
                url: '/login',
                templateUrl: "/frontend/js/components/login/index.tpl.html",
                controller: 'LoginCtrl',
            })
            .state('report', {
                url: "/report",
                templateUrl: "/frontend/js/components/report/report.tpl.html",
                //controller: 'InspectionScheduleCtrl',
            })
            .state('treatment-admin', {
                url: "/treatment-admin",
                templateUrl: "/frontend/js/components/treatment-admin/treatment-admin.tpl.html",
                controller: 'TreatmentAdminCtrl',
            })
            // call when click View Route Card button in Engineering - Overview tab & Work Order - Main tab
            .state('route-card', {
                url: '/route-card',
                templateUrl: "/frontend/js/components/route-card/route-card.tpl.html",
                controller: 'RouteCardCtrl',
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);
})(angular.module('app.router', ['ui.router']));
