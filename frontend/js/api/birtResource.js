(function(app) {
	
	app.factory('BirtResource', BirtResource);
    BirtResource.$inject = ['$q', '$resource', 'ApiUrl'];

    function BirtResource($q, $resource, ApiUrl) {
        var url = ApiUrl.get('/api/v1/birt/:name');

        var resource = $resource(url, {}, {
            post: {
                method: 'POST'
            }
        });

        return resource;
    }

})(angular.module('app.api.birtResource', []));