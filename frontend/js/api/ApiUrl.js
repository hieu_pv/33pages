(function(app) {
    app.factory('ApiUrl', function() {
        var service = {
            get: get,
        };

        function get(url) {
            return BASE_URL + url;
        }
        return service;
    });
})(angular.module('app.api.url', []));
