var User = require('../../models/User');
(function(app) {
    app.service('UserService', UserService);
    UserService.$inject = ['$http', '$q', 'ApiUrl'];

    function UserService($http, $q, ApiUrl) {
        this.login = function(user) {
            var url = ApiUrl.get('/api/v1/login');
            var deferred = $q.defer();
            $http.post(url, user)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data.error);
                });
            return deferred.promise;
        };

        this.profile = function() {
            var url = ApiUrl.get('/api/v1/me');
            var deferred = $q.defer();
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data.user));
                }, function(error) {
                    deferred.reject(error.data.error);
                });
            return deferred.promise;
        };

        this.logout = function() {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/v1/logout');
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve();
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }
})(angular.module('app.api.user', []));
