require('./ApiUrl');
require('ngResource');
require('./user/');
require('./procedure/');
require('./global/');
require('./birt/');

(function(app) {
    app.factory('API', theFactory);

    theFactory.$inject = [
        'UserService',
        'ProcedureService',
        'GlobalService',
        'BirtService'
    ];

    function theFactory(
        UserService,
        ProcedureService,
        GlobalService,
        BirtService
    ) {
        var service = {
            user: UserService,
            procedure: ProcedureService,
            global: GlobalService,
            birt: BirtService
        };
        return service;
    }
})(angular.module('app.api', [
    'app.api.url',
    'ngResource',
    'app.api.user',
    'app.api.procedure',
    'app.api.global',
    'app.api.birt'
]));