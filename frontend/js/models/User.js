var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function User(options) {
    this.pkUserID = null;
    this.Address1 = '';
    this.Address2 = '';
    this.Address3 = '';
    this.Address4 = '';
    this.ChargeRate = '';
    this.Email = '';
    this.Employed = null;
    this.Extension = '';
    this.FirstName = '';
    this.LastName = '';
    this.HomePhone = '';
    this.MobilePhone = '';
    this.POLimit = '';
    this.Postcode = '';
    this.ReportsTo = '';
    this.SkillLevel = '';
    this.SpendLimit = '';
    this.UName = '';
    this.department = [];

    BaseModel.call(this, options);
}

inherits(User, BaseModel);

User.prototype.getId = function() {
    return this.pkUserID;
};

User.prototype.getUsername = function() {
    return this.FirstName;
};

User.prototype.getDepartment = function() {
    return this.department;
};

module.exports = User;
