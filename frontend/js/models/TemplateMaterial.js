var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function TemplateMaterial(options) {
    this.Description = '';
    this.Size = null;
    this.ShapeID = null;
    this.PartLength = null;
    this.Yield = null;
    this.ApprovalID = null;
    this.IssueInstruction = '';
    this.TestPieces = null;
    this.FreeIssue = null;
    this.UnitID = null;
    this.StdDeliveryDays = null;
    this.SPUID = null;
    this.StdSupplierID = null;
    this.Spec = '';
    this.StdSupplierEmail = '';

    BaseModel.call(this, options);
}

inherits(TemplateMaterial, BaseModel);

TemplateMaterial.prototype.setShapeID = function(value) {
    this.ShapeID = value;
};

TemplateMaterial.prototype.setApprovalID = function(value) {
    this.ApprovalID = value;
};

TemplateMaterial.prototype.setUnitID = function(value) {
    this.UnitID = value;
};

TemplateMaterial.prototype.setSPUID = function(value) {
    this.SPUID = value;
};

TemplateMaterial.prototype.setStdSupplierID = function(value) {
    this.StdSupplierID = value;
};

TemplateMaterial.prototype.getDescription = function() {
    return this.Description;
};

TemplateMaterial.prototype.getSize = function() {
    return this.Size;
};

TemplateMaterial.prototype.getShapeID = function() {
    return this.ShapeID;
};

TemplateMaterial.prototype.getPartLength = function() {
    return this.PartLength;
};

TemplateMaterial.prototype.getYield = function() {
    return this.Yield;
};

TemplateMaterial.prototype.getApprovalID = function() {
    return this.ApprovalID;
};

TemplateMaterial.prototype.getIssueInstruction = function() {
    return this.IssueInstruction;
};

TemplateMaterial.prototype.getTestPieces = function() {
    return this.TestPieces;
};

TemplateMaterial.prototype.getFreeIssue = function() {
    return this.FreeIssue;
};

TemplateMaterial.prototype.getUnitID = function() {
    return this.UnitID;
};

TemplateMaterial.prototype.getStdDeliveryDays = function() {
    return this.StdDeliveryDays;
};

TemplateMaterial.prototype.getSPUID = function() {
    return this.SPUID;
};

TemplateMaterial.prototype.getStdSupplierID = function() {
    return this.StdSupplierID;
};

TemplateMaterial.prototype.getSpec = function() {
    return this.Spec;
};

TemplateMaterial.prototype.getStdSupplierEmail = function() {
    return this.StdSupplierEmail;
};

module.exports = TemplateMaterial;
