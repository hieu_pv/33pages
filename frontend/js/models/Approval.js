var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Approval(options) {
    this.Description = '';
    this.pkApprovalID = null;

    BaseModel.call(this, options);
}

inherits(Approval, BaseModel);

Approval.prototype.getDescription = function() {
    return this.Description;
};
Approval.prototype.getPkApprovalID = function() {
    return this.pkApprovalID;
};

module.exports = Approval;
