var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function PurchaseOrder(options) {
    this.OperationID = null;
    this.OpNo = null;
    this.TotalItems = null;
    this.pkOperationTypeID = null;
    this.ItemID = null;
    this.ItemNo = null;
    this.Qty = null;
    this.SPU = null;
    this.SPUID = null;
    this.Size = null;
    this.UnitID = null;
    this.ShapeID = null;
    this.PartDescription = '';
    this.Comment = '';
    this.Price = null;
    this.Per = null;

    BaseModel.call(this, options);
}

inherits(PurchaseOrder, BaseModel);

PurchaseOrder.prototype.setOptions = function(options) {
    for (var key in options) {
        this[key] = options[key];
    }
    return this;
};

PurchaseOrder.prototype.setSPUID = function(value) {
    this.SPUID = value;
};

PurchaseOrder.prototype.setUnitID = function(value) {
    this.UnitID = value;
};

PurchaseOrder.prototype.setShapeID = function(value) {
    this.ShapeID = value;
};

PurchaseOrder.prototype.setPer = function(value) {
    this.Per = value;
};

PurchaseOrder.prototype.getOperationID = function() {
    return this.OperationID;
};

PurchaseOrder.prototype.getOpNo = function() {
    return this.OpNo;
};

PurchaseOrder.prototype.getTotalItems = function() {
    return this.TotalItems;
};

PurchaseOrder.prototype.getPkOperationTypeID = function() {
    return this.pkOperationTypeID;
};

PurchaseOrder.prototype.getItemID = function() {
    return this.ItemID;
};

PurchaseOrder.prototype.getItemNo = function() {
    return this.ItemNo;
};

PurchaseOrder.prototype.getQty = function() {
    return this.Qty;
};

PurchaseOrder.prototype.getSPU = function() {
    return this.SPU;
};

PurchaseOrder.prototype.getSPUID = function() {
    return this.SPUID;
};

PurchaseOrder.prototype.getSize = function() {
    return this.Size;
};

PurchaseOrder.prototype.getUnitID = function() {
    return this.UnitID;
};

PurchaseOrder.prototype.getShapeID = function() {
    return this.ShapeID;
};

PurchaseOrder.prototype.getPartDescription = function() {
    return this.PartDescription;
};

PurchaseOrder.prototype.getComment = function() {
    return this.Comment;
};

PurchaseOrder.prototype.getPrice = function() {
    return this.Price;
};

PurchaseOrder.prototype.getPer = function() {
    return this.Per;
};

module.exports = PurchaseOrder;
