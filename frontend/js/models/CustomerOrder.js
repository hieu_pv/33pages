var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function CustomerOrder(options) {
    this.CustomerID = null;
    this.OrderRef = '';
    this.ReleaseTypeID = null;
    this.DeliveryPointID = null;
    this.CommerciallyViable = 0;
    this.AmendmentReviewed = 0;

    BaseModel.call(this, options);
}

inherits(CustomerOrder, BaseModel);

CustomerOrder.prototype.setCustomerID = function(value) {
    this.CustomerID = value;
};

CustomerOrder.prototype.setOrderRef = function(value) {
    this.OrderRef = value;
};

CustomerOrder.prototype.setReleaseTypeID = function(value) {
    this.ReleaseTypeID = value;
};

CustomerOrder.prototype.setDeliveryPointID = function(value) {
    this.DeliveryPointID = value;
};

CustomerOrder.prototype.setCommerciallyViable = function(value) {
    this.CommerciallyViable = value;
};

CustomerOrder.prototype.setAmendmentReviewed = function(value) {
    this.AmendmentReviewed = value;
};

CustomerOrder.prototype.getCustomerID = function() {
    return this.CustomerID;
};

CustomerOrder.prototype.getOrderRef = function() {
    return this.OrderRef;
};

CustomerOrder.prototype.getReleaseTypeID = function() {
    return this.ReleaseTypeID;
};

CustomerOrder.prototype.getDeliveryPointID = function() {
    return this.DeliveryPointID;
};

CustomerOrder.prototype.getCommerciallyViable = function() {
    return this.CommerciallyViable;
};

CustomerOrder.prototype.getAmendmentReviewed = function() {
    return this.AmendmentReviewed;
};

module.exports = CustomerOrder;
