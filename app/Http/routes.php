<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::group(['prefix' => 'api/v1'], function () {
	Route::post('login', 'AuthenticateController@authenticate');
	Route::post('upload', 'FileController@upload');
	Route::post('checkFileExist', 'CheckFileController@checkFile');
	Route::post('exportExcel', 'ExportController@export');
	Route::post('base64Image', 'Base64ImageController@encode');
});

Route::group(['prefix' => 'api/v1', 'middleware' => ['jwt.auth']], function () {
	Route::get('me', 'AuthenticateController@me');
	Route::delete('logout', 'AuthenticateController@logout');
});

Route::group(['prefix' => 'api/v1'], function () {
	Route::get('procedure/{name}', 'ProcedureController@get');
	Route::post('procedure/{name}', 'ProcedureController@post');

	Route::post('birt/{name}', 'BirtController@post');
});

Route::group(['middleware' => ['web']], function () {
	Route::any('{path?}', function () {
		return view("frontend.spa");
	})->where("path", ".+");
});
