<?php

namespace App\Http\Controllers;
use App\Exceptions\ApiException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FileController extends ApiController {
	private $pathUpload = 'resources/uploads/images/';
	private $validMimeType;

	public function upload(Request $request) {
		$data = $request->all();
		if (!isset($data['maxFileName']) || $data['maxFileName'] == "") {
			$data['maxFileName'] = 30;
		}
		if ($request->hasFile('fileUpload')) {
			$validator = Validator::make($request->all(), [
				'fileUpload' => 'required|mimes:jpeg,png,jpg,pdf,tiff,tif',
			]);
			if ($validator->fails()) {
				throw new ApiException("file extension not allow", 1);
			}
			$objFileSystem = new Filesystem;
			$file     = $request->file('fileUpload');
			$time = time();
			$originName = $file->getClientOriginalName();
			$fullName = $time."_".$originName;
			$fileName = pathinfo($originName, PATHINFO_FILENAME);

			// Cut filename if fullName > 30 character
			if (strlen($fullName) > $data['maxFileName'])  {
				$fileName = pathinfo($originName, PATHINFO_FILENAME);
				$extension = ".".pathinfo($originName, PATHINFO_EXTENSION);
				$subFileName = strlen($fullName) - $data['maxFileName'];
				$fileName = substr($fileName, 0, '-'.$subFileName);
				$fullName = $time."_".$fileName.$extension;
			}

			$checkExist = $objFileSystem->exists($fullName);
			while($checkExist) {
				$time = time();
				$fullName = $time."_".$fileName;
				$checkExist = $objFileSystem->exists($fullName);
			}
			if (!file_exists($this->pathUpload)) {
				mkdir($this->pathUpload, 0777);
			}
			$file->move($this->pathUpload, $fullName);
			return $this->response($fullName);
		} 
	}
}
