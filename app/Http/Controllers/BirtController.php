<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BirtController extends ApiController {
	private $birt_url;

	public function __construct() {
		if (getenv('BIRT_ENV') == 'local') {
			$this->birt_url = 'http://33pages.com:8080/JavaBridge/kythera/';
		} else {
			$this->birt_url = 'http://104.168.174.40:8080/JavaBridge/kythera/';
		}
	}

	public function post($name, Request $request) {
		$data = $request->all();
		$url  = $this->birt_url . $name . ".php";
		if (!empty($data)) {
			$query = http_build_query($data);
			$url   = $url . "?" . $query;
		}
		$content = file_get_contents($url);
		return $this->response($content);
	}
}
