<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class Base64ImageController extends ApiController {

	public function encode(Request $request) {
		$data = $request->only(['pathFile', 'extension']);
		if (!empty($data)) {
			$pathFile  = $data['pathFile'];
			$extension = $data['extension'];
		} else {
			throw new ApiException("Missing parameters", 1);
		}
		$extensions = array('jpg', 'jpeg', 'png', 'tif', 'tiff', 'gif');

		if (!in_array($extension, $extensions)) {
			throw new ApiException("Format image not support", 1);
		} else {
			$prefix = "data:image/" . $extension . ";base64,";
		}

		if (file_exists($pathFile)) {
			$file   = file_get_contents($pathFile);
			$encode = $prefix . base64_encode($file);
			return $this->response($encode);
		} else {
			throw new ApiException("File not found", 1);
		}
	}
}
