<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Response;
use JWTAuth;
use Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class ApiController extends Controller
{
    protected function getCurrentUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $GLOBALS['user'] = $user;

        return $user;
    }

    /**
     * Execute query
     *
     * @param string $nameProcedure, array $params
     * @return array
     */
    protected function runQuery($nameProcedure, $params = null)
    {
        if (!$nameProcedure) {
            throw new Exception("executeQuery params invalid", 1);
        }
        if (!empty($params)) {
            $sql = "";
            foreach ($params as $value) {
                $sql .= "'$value',";
            }
            $sql   = rtrim($sql, ",");
            $query = "Call $nameProcedure($sql)";
        } else {
            $query = "Call $nameProcedure";
        }
        return DB::select($query);
    }
    public function response($data)
    {
        $response = new Response(['data' => $data], '200');
        $response->header('Content-Type', 'application/json');
        return $response;
    }
    public function error($e)
    {
        $data = [
            'error'    => $e->getMessage(),
            'error_code' => $e->getCode(),
        ];
        $response = new Response($data, '500');
        $response->header('Content-Type', 'application/json');
        return $response;
    }
}
