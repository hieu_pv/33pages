<?php
namespace App\Transformer;

use App\User;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{

    public function transform(User $user)
    {
        return [
            'pkUserID' => (int) $user->pkUserID,
            'FirstName' => $user->FirstName,
            'LastName' => $user->LastName,
            'Email' => $user->Email,
        ];
    }
}
